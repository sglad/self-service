<!-- Please make sure all sections are filled out

-->
# Ad-Hoc Self-Service Data Request

## Filled out by Requestor
**Date** - 

**Requestor Name** - 

**Question** - 

**Context** (What is the context behind the question? Said otherwise, what is motivating you to ask this question?) -

**Business impact** (How will knowing this answer drive business changes or change your behavior?) -

**What level of priority is knowing the answer to this question for YOU?** 

- [ ] Business Stopping - unable to make time-sensitive nARR/FO impacting decision without this data
- [ ] High - need to know
- [ ] Medium - want to know
- [ ] Low - nice to know

**Could you see yourself wanting to look at this metric/data point on an ongoing basis?** 

- [ ] Yes
- [ ] No

## Filled out by Self-Service Data Team
**Responder** - 

**Answer** -

**Link to resources / snippet** -

/assign @alex_martin
/label ~"Self-Service Data"
/label ~"Self-Service Data Ad Hoc"
/label ~"Self-Service Data Intake"
/confidential
