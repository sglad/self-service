with

/*
We pull all the Pooled cases, using the record type ID. 

We have to manually parse the Subject field to get the Trigger Type, hopefully this will go away in future iterations.

-No spam filter
-Trigger Type logic only valid for FY25 onwards
*/

case_data as

(
select 

case when subject like 'Multiyear Renewal%' then 'Multiyear Renewal'
 when subject like 'EOA Renewal%' then 'EOA Renewal'
  when subject like 'PO Required%' then 'PO Required'
  when subject like 'Auto-Renewal Will Fail%' then 'Auto-Renewal Will Fail'
   when subject like 'Overdue Renewal%' then 'Overdue Renewal'
    when subject like 'Auto-Renew Recently Turned Off%' then 'Auto-Renew Recently Turned Off'
        when subject like 'Failed QSR%' then 'Failed QSR' else subject end as case_trigger,
sfdc_case.*
 ,
 datediff('day',sfdc_case.created_date,sfdc_case.closed_date) as case_days_to_close,
 datediff('day',sfdc_case.created_date,current_date) as case_age_days,
 dim_crm_user.user_name as case_owner_name,
 dim_crm_user.department as case_department,
 dim_crm_user.team,
 dim_crm_user.manager_name,
 dim_crm_user.user_role_name as case_user_role_name,
 dim_crm_user.crm_user_role_type
from workspace_sales.sfdc_case
left join common.dim_crm_user on sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
where
sfdc_case.RECORD_TYPE_ID = '0128X000001pPRkQAM'
and sfdc_case.created_date >= '2023-02-01'
--and sfdc_case.is_closed
-- and (sfdc_case.reason not like '%Spam%' or reason is null)
)
,

task_data as (

-- Returns all completed Outreach tasks
-- Intended to be used mainly for understanding calls, meetings, and emails by AEs and SDRs
-- May need to be updated for new role names

select * from

(select 
    task_id,
task_status,
task.dim_crm_account_id,
task.dim_crm_user_id,
task.dim_crm_person_id,
task_subject,
case when lower(task_subject) like '%email%' then 'Email'
when lower(task_subject) like '%call%' then 'Call'
when lower(task_subject) like '%linkedin%' then 'LinkedIn'
when lower(task_subject) like '%inmail%' then 'LinkedIn'
when lower(task_subject) like '%sales navigator%' then 'LinkedIn'
when lower(task_subject) like '%drift%' then 'Chat'
when lower(task_subject) like '%chat%' then 'Chat'
else
task_type end as type,
case when task_subject like '%Outreach%' and task_subject not like '%Advanced Outreach%' then 'Outreach'
  when task_subject like '%Clari%' then 'Clari'
when task_subject like '%Conversica%' then 'Conversica'
  else 'Other' end as outreach_clari_flag,
TASK_CREATED_DATE,
task_created_by_id,

--This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

case when outreach_clari_flag = 'Outreach' and (task_subject like '%[Out]%' or task_subject like '%utbound%') then 'Outbound'
when outreach_clari_flag = 'Outreach' and (task_subject like '%[In]%' or task_subject like '%nbound%') then 'Inbound'
else 'Other' end as inbound_outbound_flag,
case when (inbound_outbound_flag = 'Outbound' and task_subject like '%Answered%' and task_subject not like '%Not Answer%'
and task_subject not like '%No Answer%') or (lower(task_subject) like '%call%' and task_subject not like '%Outreach%' and task_status = 'Completed' ) then true else false end as outbound_answered_flag,
task_date,
      case when task.TASK_CREATED_BY_ID like '0054M000003Tqub%' then 'Outreach'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%GitLab Transactions%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Was Sent Email%' then 'SFDC Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Your GitLab License%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Advanced Outreach%' then 'Gainsight Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Filled Out Form%' then 'Marketo Form Fill'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Conversation in Drift%' then 'Drift'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Opened Email%' then 'Marketing Email Opened'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Sales Navigator%' then 'Sales Navigator'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Clari - Email%' then 'Clari Email'
      else
      'Other' end as task_type,

      user.user_name as task_user_name,
      case when user.department like '%arketin%' then 'Marketing' else user.department end as department,
      user.is_active,
      user.crm_user_sales_segment,
        user.crm_user_geo,
user.crm_user_region,
user.crm_user_area,
user.crm_user_business_unit,
      user.user_role_name
      from
  prod.common_mart_sales.mart_crm_task task
     inner join common.dim_crm_user user on task.dim_crm_user_id = user.DIM_CRM_USER_ID
   where
      task.dim_crm_user_id is not null
and
      is_deleted = false
and task_date >= '2023-02-01'
and task_status = 'Completed')

where (outreach_clari_flag = 'Outreach' or task_created_by_id = dim_crm_user_id)
and outreach_clari_flag <> 'Other'
and (user_role_name like any ( '%AE%','%SDR%','%BDR%')
or crm_user_sales_segment = 'SMB')     
)
,
case_task_summary_data as
(
select 
case_data.case_id as case_task_summary_id,

listagg(distinct task_data.task_id, ', ')
as case_task_id_list,

count(distinct case when task_data.inbound_outbound_flag = 'Inbound' then task_data.task_id else null end) as inbound_email_count,
count(distinct case when task_data.outbound_answered_flag then task_data.task_id else null end) as completed_call_count,
count(distinct case when task_data.inbound_outbound_flag = 'Outbound' then task_data.task_id else null end) as outbound_email_count,
case when inbound_email_count > 0 then true else false end as task_inbound_flag,
case when completed_call_count > 0 then true else false end as task_completed_call_flag,
case when outbound_email_count > 0 then true else false end as task_outbound_flag,
count(distinct task_data.task_id) as completed_task_count,
min(datediff('day',case_data.created_date,task_data.task_date)) as days_to_first_task,
min(datediff('day',task_data.task_date,current_date)) as days_since_last_task

from case_data
left join task_data
on
task_data.dim_crm_account_id = case_data.account_id
and
task_data.task_date::DATE >= case_data.created_date::DATE
and (task_data.task_date::DATE <= case_data.closed_date::DATE or case_data.closed_date is null)
--where case_data.account_id is not null
group by 1
)

select
case_data.*,
case_task_summary_data.* EXCLUDE (case_task_summary_id)
from
case_data
left join
case_task_summary_data on case_data.case_id = case_task_summary_data.case_task_summary_id

