--Version of the forecasting model specifically for tableau (with parameters)
with opportunity_data as
(
SELECT
mart_crm_opportunity.*,
user.crm_user_role_type,
dim_date.fiscal_year                     AS date_range_year,
dim_date.fiscal_quarter_name_fy          AS date_range_quarter,
DATE_TRUNC(month, dim_date.date_actual)  AS date_range_month,
dim_date.first_day_of_week               AS date_range_week,
dim_date.date_id                         AS date_range_id,
dim_date.fiscal_month_name_fy,
dim_date.fiscal_quarter_name_fy,
dim_date.fiscal_year,
dim_date.first_day_of_fiscal_quarter,
case
    when product_category like '%Self%' or PRODUCT_DETAILS like '%Self%' or product_category like '%Starter%' or PRODUCT_DETAILS like '%Starter%' then 'Self-Managed'
    when product_category like '%SaaS%' or PRODUCT_DETAILS like '%SaaS%' or product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' or product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' or product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'SaaS'
    when PRODUCT_DETAILS not like '%SaaS%' and (PRODUCT_DETAILS like '%Premium%' or PRODUCT_DETAILS like '%Ultimate%') then 'Self-Managed'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
else 'Other' end as delivery,
case
    when order_type like '3%' or order_type like '2%' then 'Growth'
    when order_type like '1%' then 'First Order'
    when order_type like '4%' or order_type like '5%' or order_type like '6%' then 'Churn / Contraction'
end as order_type_clean,
CASE when order_type like '5%' and net_arr = 0 then true else false end as partial_churn_0_narr_flag,
case
    when product_category like '%Premium%'  or PRODUCT_DETAILS like '%Premium%' then 'Premium'
    when product_category like '%Ultimate%'  or PRODUCT_DETAILS like '%Ultimate%' then 'Ultimate'
    when product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' then 'Bronze'
    when product_category like '%Starter%'  or PRODUCT_DETAILS like '%Starter%' then 'Starter'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
    when product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' then 'Silver'
    when product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'Gold'
    when product_category like 'CI%' or PRODUCT_DETAILS like 'CI%' then 'CI'
else product_category end as product_tier,
CASE
    when opportunity_name like '%QSR%' then true else false end as qsr_flag,
CASE
    when order_type like '7%' and qsr_flag = False then 'PS/CI/CD'
    when order_type like '1%' and net_arr >0  then 'First Order'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = False then 'Growth - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr =0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Flat'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr <0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type = 'Renewal' then 'Renewal - Uplift'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type <> 'Renewal' then 'Non-Renewal - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type <> 'Renewal' then 'Non-Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type = 'Renewal' then 'Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type = 'Renewal' then 'Renewal - Contraction'
    when order_type like any ('5.%','6.%') then 'Churn'
else 'Other' end as trx_type,
CASE  
    when opportunity_name like '%Startups Program%' then true else false end as startup_program_flag

FROM common.dim_date
left JOIN restricted_safe_common_mart_sales.mart_crm_opportunity
  ON mart_crm_opportunity.close_date = dim_date.date_actual
  left join common.dim_crm_user user on mart_crm_opportunity.dim_crm_user_id = user.dim_crm_user_id

WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
and partial_churn_0_narr_flag = false
and fiscal_year >= 2022
--and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)

)
,

/*
We pull all the Pooled cases, using the record type ID. 

We have to manually parse the Subject field to get the Trigger Type, hopefully this will go away in future iterations.

-No spam filter
-Trigger Type logic only valid for FY25 onwards
*/

 case_data as

 (
select 

case when subject like 'Multiyear Renewal%' then 'Multiyear Renewal'
 when subject like 'EOA Renewal%' then 'EOA Renewal'
  when subject like 'PO Required%' then 'PO Required'
  when subject like 'Auto-Renewal Will Fail%' then 'Auto-Renewal Will Fail'
   when subject like 'Overdue Renewal%' then 'Overdue Renewal'
    when subject like 'Auto-Renew Recently Turned Off%' then 'Auto-Renew Recently Turned Off'
        when subject like 'Failed QSR%' then 'Failed QSR' else subject end as case_trigger,
sfdc_case.*
 ,
 datediff('day',sfdc_case.created_date,sfdc_case.closed_date) as case_days_to_close,
 datediff('day',sfdc_case.created_date,current_date) as case_age_days,
 dim_crm_user.user_name as case_owner_name,
 dim_crm_user.department as case_department,
 dim_crm_user.team,
 dim_crm_user.manager_name,
 dim_crm_user.user_role_name as case_user_role_name,
 dim_crm_user.crm_user_role_type
from workspace_sales.sfdc_case
left join common.dim_crm_user on sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
where
sfdc_case.RECORD_TYPE_ID = '0128X000001pPRkQAM'
and sfdc_case.created_date >= '2023-02-01'
--and sfdc_case.is_closed
-- and (sfdc_case.reason not like '%Spam%' or reason is null)
)
,

task_data as (

-- Returns all completed Outreach tasks
-- Intended to be used mainly for understanding calls, meetings, and emails by AEs and SDRs
-- May need to be updated for new role names

select * from

(select 
    task_id,
task_status,
task.dim_crm_account_id,
task.dim_crm_user_id,
task.dim_crm_person_id,
task_subject,
case when lower(task_subject) like '%email%' then 'Email'
when lower(task_subject) like '%call%' then 'Call'
when lower(task_subject) like '%linkedin%' then 'LinkedIn'
when lower(task_subject) like '%inmail%' then 'LinkedIn'
when lower(task_subject) like '%sales navigator%' then 'LinkedIn'
when lower(task_subject) like '%drift%' then 'Chat'
when lower(task_subject) like '%chat%' then 'Chat'
else
task_type end as type,
case when task_subject like '%Outreach%' and task_subject not like '%Advanced Outreach%' then 'Outreach'
  when task_subject like '%Clari%' then 'Clari'
when task_subject like '%Conversica%' then 'Conversica'
  else 'Other' end as outreach_clari_flag,
TASK_CREATED_DATE,
task_created_by_id,

--This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

case when outreach_clari_flag = 'Outreach' and (task_subject like '%[Out]%' or task_subject like '%utbound%') then 'Outbound'
when outreach_clari_flag = 'Outreach' and (task_subject like '%[In]%' or task_subject like '%nbound%') then 'Inbound'
else 'Other' end as inbound_outbound_flag,
case when (inbound_outbound_flag = 'Outbound' and task_subject like '%Answered%' and task_subject not like '%Not Answer%'
and task_subject not like '%No Answer%') or (lower(task_subject) like '%call%' and task_subject not like '%Outreach%' and task_status = 'Completed' ) then true else false end as outbound_answered_flag,
task_date,
      case when task.TASK_CREATED_BY_ID like '0054M000003Tqub%' then 'Outreach'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%GitLab Transactions%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Was Sent Email%' then 'SFDC Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Your GitLab License%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Advanced Outreach%' then 'Gainsight Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Filled Out Form%' then 'Marketo Form Fill'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Conversation in Drift%' then 'Drift'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Opened Email%' then 'Marketing Email Opened'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Sales Navigator%' then 'Sales Navigator'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Clari - Email%' then 'Clari Email'
      else
      'Other' end as task_type,

      user.user_name as task_user_name,
      case when user.department like '%arketin%' then 'Marketing' else user.department end as department,
      user.is_active,
      user.crm_user_sales_segment,
        user.crm_user_geo,
user.crm_user_region,
user.crm_user_area,
user.crm_user_business_unit,
      user.user_role_name
      from
  prod.common_mart_sales.mart_crm_task task
     inner join common.dim_crm_user user on task.dim_crm_user_id = user.DIM_CRM_USER_ID
   where
      task.dim_crm_user_id is not null
and
      is_deleted = false
and task_date >= '2023-02-01'
and task_status = 'Completed')

where (outreach_clari_flag = 'Outreach' or task_created_by_id = dim_crm_user_id)
and outreach_clari_flag <> 'Other'
and (user_role_name like any ( '%AE%','%SDR%','%BDR%')
or crm_user_sales_segment = 'SMB')     
)
,

account_base as (
SELECT acct.*, 
CASE WHEN first_high_value_case.case_id IS NOT NULL then 'Tier 1'
     WHEN first_high_value_case.case_id IS NULL THEN
        CASE WHEN acct.carr_this_account >7000 then 'Tier 1' 
            WHEN acct.carr_this_account < 3000 AND acct.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
            ELSE 'Tier 2' end
    ELSE null END AS calculated_tier, 
CASE WHEN acct.parent_crm_account_geo in ('AMER', 'APAC') then 'AMER/APAC' 
     WHEN acct.parent_crm_account_geo in ('EMEA') then 'EMEA' 
     ELSE 'Other' end as Team,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
fo.SQS as fo_sqs,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr, 
case when fo_fiscal_year <= 2024 then False else True end as New_FY25_FO_Flag, 
first_high_value_case.created_date as first_high_value_case_created_date, 
high_value_case.case_owner_name as high_value_account_owner,
high_value_case.team as high_value_account_team,
high_value_case.manager_name as high_value_manager_name, 
start_values.carr_account_family as starting_carr_account_family, 
start_values.carr_this_account as starting_carr_this_account, 
CASE WHEN start_values.carr_this_account >7000 then 'Tier 1' 
     WHEN start_values.carr_this_account < 3000 AND start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
     else 'Tier 2' end as starting_calculated_tier,
start_values.pte_score as starting_pte_score, 
start_values.ptc_score as starting_ptc_score, 
start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as starting_parent_crm_account_lam_dev_count,
CASE WHEN EOA.dim_crm_account_id IS NOT NULL then True else False end as EOA_Flag, 
CASE WHEN free_promo.dim_crm_account_id IS NOT NULL then True else False end as Free_Promo_Flag, 
CASE WHEN price_increase.dim_crm_account_id IS NOT NULL then True else False end as Price_Increase_Promo_Flag,
CASE WHEN ultimate.dim_parent_crm_account_id is not null then true else false end as ultimate_customer_flag
FROM PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT acct
------subquery that gets latest FO data
LEFT JOIN 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year, 
    last_value(sales_qualified_source_name) over (partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as SQS
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
        ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo
    ON fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
---------subquery that gets latest churn data 
LEFT JOIN 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
        ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
     and not(product_category like '%torage%' or PRODUCT_DETAILS like '%torage%')  
    ) churn 
    ON churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
--------subquery to get high tier case owner
LEFT JOIN 
    (SELECT
    account_id,
    owner_id, 
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.crm_user_role_type
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    and subject = 'FY25 High Value Account' -----subject placeholder - this could change
    ) high_value_case
    ON high_value_case.account_id = acct.dim_crm_account_id
--------------subquery to get start of FY25 values
LEFT JOIN 
    (SELECT
    *
    FROM PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
    WHERE snapshot_date = '2024-02-05' -----placeholder date for start of year
    ) start_values
    ON start_values.dim_crm_account_id = acct.dim_crm_account_id
-----subquery to get FIRST high value case 
LEFT JOIN 
    (SELECT 
    *
    FROM (SELECT
    account_id,
    case_id,
    owner_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.crm_user_role_type,
    sfdc_case.created_date,
    MIN(sfdc_case.created_date) over(partition by ACCOUNT_ID) as first_high_value_date
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    and lower(subject) like '%high value account%') -----subject placeholder - this could change
    WHERE created_date = first_high_value_date 
    ) first_high_value_case
    ON first_high_value_case.account_id = acct.dim_crm_account_id

-----EOA cohort accounts
    LEFT JOIN 
    (SELECT
mart_arr.ARR_MONTH
--,ping_created_at
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
--,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
--,product_rate_plan_name
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
)) EOA 
    ON EOA.dim_crm_account_id = acct.dim_crm_account_id 
------free limit promo cohort accounts
LEFT JOIN 
    (select
  distinct
dim_crm_account_id
  from restricted_safe_common_mart_sales.mart_charge charge
  where 
  subscription_start_date >= '2023-02-01'
  and rate_plan_charge_description = 'fo-discount-70-percent') free_promo
    ON free_promo.dim_crm_account_id = acct.dim_crm_account_id 
------price increase promo cohort accounts
LEFT JOIN
    (select distinct dim_crm_account_id from
        (
         select
          charge.*,
          arr / quantity as actual_price,
          prod.annual_billing_list_price as list_price
          from restricted_safe_common_mart_sales.mart_charge charge
          inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
          where 
          subscription_start_date >= '2023-04-01'
          and subscription_start_date <= '2023-07-01'
          and TYPE_OF_ARR_CHANGE = 'New'
          and quantity > 0
          and actual_price > 228
          and actual_price < 290
          and rate_plan_charge_name like '%Premium%'
        )) price_increase
        ON price_increase.dim_crm_account_id = acct.dim_crm_account_id 
LEFT JOIN
(
    select
distinct
dim_parent_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = date_trunc('month',current_date)
and product_tier_name like '%Ultimate%'
) ultimate on acct.dim_parent_crm_account_id = ultimate.dim_parent_crm_account_id


-------filtering to get current account data     
WHERE acct.snapshot_date >= '2021-12-01')



,

upgrades as
(
SELECT
   arr_month,
  type_of_arr_change,
  arr.product_category[0] as upgrade_product,
  previous_month_product_category[0] as prior_product,
  opp.is_web_portal_purchase,
  arr.dim_crm_account_id, 
  opp.dim_crm_opportunity_id,
  SUM(beg_arr)                      AS beg_arr,
  SUM(end_arr)                      AS end_arr,
  SUM(end_arr) - SUM(beg_arr)       AS delta_arr,
  SUM(seat_change_arr)              AS seat_change_arr,
  SUM(price_change_arr)             AS price_change_arr,
  SUM(tier_change_arr)              AS tier_change_arr,
  SUM(beg_quantity)                 AS beg_quantity,
  SUM(end_quantity)                 AS end_quantity,
  SUM(seat_change_quantity)         AS delta_seat_change,
  COUNT(*)                          AS nbr_customers_upgrading
FROM restricted_safe_legacy.mart_delta_arr_subscription_month arr
left join 
(
SELECT
*
FROM restricted_safe_common_mart_sales.mart_crm_opportunity
WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
--and partial_churn_0_narr_flag = false
--and fiscal_year >= 2023
--and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)

)
 opp
  on (arr.DIM_CRM_ACCOUNT_ID = opp.DIM_CRM_ACCOUNT_ID
  and arr.arr_month = date_trunc('month',opp.subscription_start_date)
 -- and opp.order_type = '3. Growth'
  and opp.is_won)
WHERE 
 (ARRAY_CONTAINS('Self-Managed - Starter'::VARIANT, previous_month_product_category)
          OR ARRAY_CONTAINS('SaaS - Bronze'::VARIANT, previous_month_product_category)
       or ARRAY_CONTAINS('SaaS - Premium'::VARIANT, previous_month_product_category)
       or ARRAY_CONTAINS('Self-Managed - Premium'::VARIANT, previous_month_product_category)
       )
   AND tier_change_arr > 0
GROUP BY 1,2,3,4,5,6,7
)
,
price_increase_promo_fo_data as
(--Gets all FO opportunities associated with Price Increase promo

select
distinct dim_crm_opportunity_id,
actual_price
from
(select
mart_charge.*,
dim_subscription.dim_crm_opportunity_id,
max(arr) over(partition by mart_charge.dim_subscription_id) as actual_arr,
max(quantity) over(partition by mart_charge.dim_subscription_id) as actual_quantity,
actual_arr/actual_quantity as actual_price
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
left join PROD.COMMON.DIM_SUBSCRIPTION
on mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
where
-- effective_start_date >= '2023-02-01'
-- and
dim_subscription.subscription_start_date >= '2023-04-01'
  and dim_subscription.subscription_start_date <= '2023-07-01'
  and TYPE_OF_ARR_CHANGE = 'New'
  and quantity > 0
  -- and actual_price > 228
  -- and actual_price < 290
  and rate_plan_charge_name like '%Premium%'
qualify actual_price > 228
  and actual_price < 290
)
)
,
past_eoa_uplift_opportunity_data as
(--Gets all opportunities associated with EoA special pricing uplift
with eoa_accounts_fy24 as
(
--Looks for current month ARR around $15 to account for currency conversion.
--Checks to make sure accounts previously had ARR in Bronze or Starter (to exclude accounts that just have discounts)
select distinct dim_crm_account_id from
(
SELECT
mart_arr.ARR_MONTH
--,ping_created_at
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
--,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
--,product_rate_plan_name
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
) order by dim_crm_account_id asc
)
)

select distinct dim_crm_opportunity_id from
(
select
mart_charge.*,
dim_subscription.dim_crm_opportunity_id,
dim_subscription.dim_crm_opportunity_id_current_open_renewal,
previous_mrr / previous_quantity as previous_price,
mrr / quantity as price_after_renewal
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
left join PROD.COMMON.DIM_SUBSCRIPTION
on mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
inner join eoa_accounts_fy24 on eoa_accounts_fy24.dim_crm_account_id = mart_charge.dim_crm_account_id
-- left join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY on dim_subscription.dim_crm_opportunity_id_current_open_renewal = mart_crm_opportunity.dim_crm_opportunity_id
where
mart_charge.rate_plan_name like '%Premium%'
-- and 
-- mart_charge.term_start_date <= '2024-02-01'
and type_of_arr_change <> 'New'
and mart_charge.term_start_date >= '2022-02-01'
and price_after_renewal > previous_price
and previous_quantity <> 0
and quantity <> 0
and (lower(rate_plan_charge_description) like '%eoa%'
or
((previous_price >= 5 and previous_price <= 7)
or (previous_price >= 8 and previous_price <= 10)
or (previous_price >= 14 and previous_price <= 16)))
)
)
,
free_limit_promo_FO_data as
(--Gets all opportunities associated with Free Limit 70% discount

select
distinct dim_crm_opportunity_id,
actual_price
from
(select
mart_charge.*,
dim_subscription.dim_crm_opportunity_id,
max(arr) over(partition by mart_charge.dim_subscription_id) as actual_arr,
max(quantity) over(partition by mart_charge.dim_subscription_id) as actual_quantity,
actual_arr/actual_quantity as actual_price
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
left join PROD.COMMON.DIM_SUBSCRIPTION
on mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
where
-- effective_start_date >= '2023-02-01'
-- and
rate_plan_charge_description like '%70%'
and type_of_arr_change = 'New'
)
)
,
open_eoa_renewal_data as
(
--Gets all future renewal opportunities where the account currently has EoA special pricing


select
distinct dim_crm_opportunity_id_current_open_renewal,
price_after_renewal,
close_date
from
(select
mart_charge.*,
dim_subscription.dim_crm_opportunity_id,
close_date,
max(mart_charge.arr) over(partition by mart_charge.dim_subscription_id) as actual_arr,
max(mart_charge.quantity) over(partition by mart_charge.dim_subscription_id) as actual_quantity,
actual_arr/actual_quantity as actual_price,
previous_mrr / previous_quantity as previous_price,
mrr / quantity as price_after_renewal,
dim_subscription.dim_crm_opportunity_id_current_open_renewal
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
left join PROD.COMMON.DIM_SUBSCRIPTION
on mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
left join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY on dim_subscription.dim_crm_opportunity_id_current_open_renewal = mart_crm_opportunity.dim_crm_opportunity_id
where
mart_crm_opportunity.close_date >= '2024-02-01'
and mart_crm_opportunity.is_closed = false
and mart_charge.rate_plan_name like '%Premium%'
-- and 
-- mart_charge.term_start_date <= '2024-02-01'
and type_of_arr_change <> 'New'
and mart_charge.term_start_date >= '2022-02-01'
and price_after_renewal > previous_price
and previous_quantity <> 0
and quantity <> 0
and mart_charge.dim_crm_account_id not in
( select
  distinct
dim_crm_account_id
  from restricted_safe_common_mart_sales.mart_charge charge
  where 
  subscription_start_date >= '2023-02-01'
  and rate_plan_charge_description = 'fo-discount-70-percent')
and (lower(rate_plan_charge_description) like '%eoa%'
or
((previous_price >= 5 and previous_price <= 7)
or (previous_price >= 8 and previous_price <= 10)
or (previous_price >= 14 and previous_price <= 16)))
)


)
,
renewal_self_service_data as
(--Uses the Order Action to determine if a Closed Won renewal was Autorenewed, Sales-Assisted, or Manual Portal Renew by the customer

select dim_order_action.DIM_SUBSCRIPTION_ID,
  dim_subscription.subscription_name,
  dim_order_action.contract_effective_date,
    case when ORDER_DESCRIPTION <> 'AutoRenew by CustomersDot' then true else false end as manual_portal_renew_flag,
    mart_crm_opportunity.is_web_portal_purchase,
    mart_crm_opportunity.dim_crm_opportunity_id,
    case when manual_portal_renew_flag and is_web_portal_purchase then 'Manual Portal Renew'
when manual_portal_renew_flag = false and is_web_portal_purchase then 'Autorenew'
else 'Sales-Assisted Renew' end as actual_manual_renew_flag
from common.dim_order_action
    left join common.dim_order on dim_order_action.dim_order_id = dim_order.dim_order_id
    left join common.dim_subscription on dim_order_action.DIM_SUBSCRIPTION_ID = dim_subscription.DIM_SUBSCRIPTION_ID
    left join restricted_safe_common_mart_sales.mart_crm_opportunity on dim_subscription.dim_crm_opportunity_id = mart_crm_opportunity.dim_crm_opportunity_id
where order_action_type = 'RenewSubscription'
)
,
price_increase_promo_renewal_open_data as
(
  select distinct dim_crm_opportunity_id_current_open_renewal from
(
 select
  charge.*,
  arr / quantity as actual_price,
  prod.annual_billing_list_price as list_price,
  dim_subscription.dim_crm_opportunity_id_current_open_renewal
  from restricted_safe_common_mart_sales.mart_charge charge
  inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
  inner join common.dim_subscription on dim_subscription.dim_subscription_id = charge.dim_subscription_id
  where 
  charge.term_start_date >= '2023-04-01'
  and charge.term_start_date <= '2024-05-01'
  and charge.TYPE_OF_ARR_CHANGE <> 'New'
  and charge.subscription_start_date < '2023-04-01'
  and charge.quantity > 0
  and actual_price > 228
  and actual_price < 290
  and charge.rate_plan_charge_name like '%Premium%'
)

)
,
case_task_summary_data as
(
select 
opportunity_data.dim_crm_opportunity_id as case_task_summary_id,

listagg(distinct case_data.case_trigger, ', ') within group (order by case_data.case_trigger)
as oppty_trigger_list,
listagg(distinct case_data.case_id, ', ')
as oppty_case_id_list,

count(distinct case_data.case_id) as closed_case_count,
count(distinct case when case_data.status = 'Closed - Resolved' or case_data.status = 'Closed' then case_data.case_id else null end) as resolved_case_count,

count(distinct case when task_data.inbound_outbound_flag = 'Inbound' then task_data.task_id else null end) as inbound_email_count,
count(distinct case when task_data.outbound_answered_flag then task_data.task_id else null end) as completed_call_count,
count(distinct case when task_data.inbound_outbound_flag = 'Outbound' then task_data.task_id else null end) as outbound_email_count,
case when inbound_email_count > 0 then true else false end as task_inbound_flag,
case when completed_call_count > 0 then true else false end as task_completed_call_flag,
case when outbound_email_count > 0 then true else false end as task_outbound_flag,
count(distinct task_data.task_id) as completed_task_count

from opportunity_data
left join case_data
on
case_data.account_id = opportunity_data.dim_crm_account_id
and
(
(trx_type = 'First Order' and case_data.created_date <= opportunity_data.close_date and case_data.status = 'Closed - Resolved' and case_data.created_date >= '2024-02-01')
or
(trx_type like any ('%Growth%','%QSR%') and case_data.closed_date <= opportunity_data.close_date and case_data.status = 'Closed - Resolved' and case_data.closed_date >= opportunity_data.close_date - 90 and case_data.created_date >= '2024-02-01')
or
(trx_type like any ('Renewal%','Churn') and case_data.status = 'Closed - Resolved' and case_data.closed_date <= opportunity_data.close_date + 30 and case_data.created_date >= '2024-02-01')
)
left join task_data
on
task_data.dim_crm_account_id = opportunity_data.dim_crm_account_id
and
(
(trx_type = 'First Order' and task_data.task_date <= opportunity_data.close_date and task_data.task_date >= '2023-02-01')
or
(trx_type like any ('%Growth%','%QSR%') and task_data.task_date <= opportunity_data.close_date and task_data.task_date >= opportunity_data.close_date - 90 and task_data.task_date >= '2023-02-01')
or
(trx_type like any ('Renewal%','Churn') and task_data.task_date <= opportunity_data.close_date + 30 and task_data.task_date >= '2023-02-01' and task_data.task_date >= opportunity_data.close_date - 365))
--where case_data.account_id is not null
group by 1
)
,
ai_fields as
(
  select
id as dim_crm_opportunity_id,
PTC_PREDICTED_ARR__C,
PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C
from
RAW.SALESFORCE_V2_STITCH.OPPORTUNITY
where PTC_PREDICTED_ARR__C is not null
or PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C is not null
)
,
full_base_data as
(
select
opportunity_data.*,
account_base.calculated_tier,
account_base.team,
account_base.fo_fiscal_year,
account_base.fo_close_date,
account_base.fo_net_arr,
account_base.fo_sqs,
account_base.churn_fiscal_year,
account_base.churn_net_arr,
account_base.churn_close_date,
account_base.New_FY25_FO_Flag,
account_base.high_value_account_owner,
account_base.high_value_account_team,
account_base.high_value_manager_name,
account_base.EOA_Flag as eoa_account_flag,
account_base.Free_Promo_Flag as free_promo_account_flag,
account_base.Price_Increase_Promo_Flag as price_increase_promo_account_flag,
account_base.crm_account_owner,
account_base.owner_role,
account_base.account_tier,
GS_HEALTH_USER_ENGAGEMENT, GS_HEALTH_CD, GS_HEALTH_DEVSECOPS, GS_HEALTH_CI, GS_HEALTH_SCM,
CARR_ACCOUNT_FAMILY, CARR_THIS_ACCOUNT,
pte_score,
ptc_score,
PTC_PREDICTED_ARR__C,
PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C,
upgrades.prior_product,
upgrades.upgrade_product,
case when upgrades.dim_crm_opportunity_id is not null then true else false end as upgrade_flag,
case when price_increase_promo_fo_data.dim_crm_opportunity_id is not null then true else false end as price_increase_promo_fo_flag,
case when free_limit_promo_FO_data.dim_crm_opportunity_id is not null then true else false end as free_limit_promo_fo_flag,
case when past_eoa_uplift_opportunity_data.dim_crm_opportunity_id is not null then true else false end as past_eoa_uplift_opportunity_flag,
case when open_eoa_renewal_data.dim_crm_opportunity_id_current_open_renewal is not null then true else false end as open_eoa_renewal_flag,
case when price_increase_promo_renewal_open_data.dim_crm_opportunity_id_current_open_renewal is not null then true else false end as price_increase_promo_open_renewal_flag,
renewal_self_service_data.actual_manual_renew_flag as actual_manual_renew_flag
,
case_task_summary_data.* EXCLUDE (case_task_summary_id),
dim_subscription.TURN_ON_AUTO_RENEWAL,
ultimate_customer_flag,
case when arr_basis is null or arr_basis = 0 then account_base.carr_this_account else arr_basis end as atr,
case when account_base.team = 'Other' then
  case when account_base.parent_crm_account_geo in ('AMER','APAC','LATAM') then 'AMER'
  when account_base.parent_crm_account_geo in ('EMEA','META') then 'EMEA'
  else account_base.team end
else account_base.team end as team_fixed,
case when
((account_base.carr_account_family <= 30000 and account_base.carr_this_account > 0) or (trx_type = 'First Order' and net_arr <= 30000))
and (account_base.parent_crm_account_max_family_employee <= 100 or account_base.parent_crm_account_max_family_employee is null)
and account_base.ultimate_customer_flag = false
and account_base.parent_crm_account_sales_segment in ('SMB','Mid-Market','Large')
and account_base.parent_crm_account_upa_country <> 'JP'
and account_base.is_jihu_account = false
then true else false end as herbie_oppty_flag,
case when trx_type in ('Renewal - Uplift','Renewal - Flat') then 'Renewal Growth'
when qsr_flag or trx_type = 'Growth - Uplift' then 'Nonrenewal Growth'
when trx_type in ('Churn','Renewal - Contraction') then 'C&C' else 'Other' end as trx_type_grouping
from opportunity_data
left join account_base
on opportunity_data.DIM_CRM_ACCOUNT_ID = account_base.DIM_CRM_ACCOUNT_ID
and ((trx_type <> 'First Order' and opportunity_data.CLOSE_DATE - 7 = account_base.snapshot_date) 
 or (trx_type = 'First Order' and opportunity_data.CLOSE_DATE + 2 = account_base.snapshot_date)
   or (opportunity_data.CLOSE_DATE > CURRENT_DATE and account_base.snapshot_date = CURRENT_DATE - 1)
 )
 left join upgrades on opportunity_data.dim_crm_opportunity_id = upgrades.dim_crm_opportunity_id
 left join price_increase_promo_fo_data on price_increase_promo_fo_data.dim_crm_opportunity_id = opportunity_data.dim_crm_opportunity_id
 left join past_eoa_uplift_opportunity_data on past_eoa_uplift_opportunity_data.dim_crm_opportunity_id = opportunity_data.dim_crm_opportunity_id
 left join free_limit_promo_FO_data on free_limit_promo_FO_data.dim_crm_opportunity_id = opportunity_data.dim_crm_opportunity_id
 left join open_eoa_renewal_data on open_eoa_renewal_data.dim_crm_opportunity_id_current_open_renewal = opportunity_data.dim_crm_opportunity_id
 left join renewal_self_service_data on renewal_self_service_data.dim_crm_opportunity_id = opportunity_data.dim_crm_opportunity_id
left join case_task_summary_data
on
case_task_summary_data.case_task_summary_id = opportunity_data.dim_crm_opportunity_id
left join price_increase_promo_renewal_open_data on price_increase_promo_renewal_open_data.dim_crm_opportunity_id_current_open_renewal = opportunity_data.dim_crm_opportunity_id
left join common.dim_subscription on dim_subscription.dim_crm_opportunity_id_current_open_renewal = opportunity_data.dim_crm_opportunity_id
left join ai_fields on ai_fields.dim_crm_opportunity_id = opportunity_data.dim_crm_opportunity_id

--ADD EOA/PROMO PARAMETER FILTER HERE
--where eoa_account_flag = [PARAMETER]
--and (free_promo_account_flag = [PARAMETER] or price_increase_promo_account_flag = [PARAMETER])
)
,
renewal_forecast_base_data as
(
select
team_fixed,
calculated_tier,
close_month,
sum(atr) as total_atr,
sum(net_arr) as total_net_arr,
sum(case when trx_type_grouping = 'Renewal Growth' and is_won then net_arr else 0 end) / total_atr as renewal_growth_rate,
sum(case when trx_type_grouping = 'C&C' then net_arr else 0 end) / total_atr as c_and_c_rate,
sum(case when PTC_PREDICTED_ARR__C is not null and is_closed = false then PTC_PREDICTED_ARR__C else 0 end) - 
sum(case when PTC_PREDICTED_ARR__C is not null and is_closed = false then atr else 0 end) as ai_atr_net_forecast,
sum(case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Churn (Actionable)'
and is_closed
and trx_type <> 'Churn'
then net_arr else 0 end) as predicted_will_churn_narr_best_case,
sum(case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Churn (Actionable)'
and is_closed
then net_arr else 0 end) as predicted_will_churn_narr_commit,

sum(case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Contract (Actionable)'
and is_closed
and trx_type <> 'Churn'
then net_arr else 0 end) as predicted_will_contract_narr_best_case,
sum(case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Contract (Actionable)'
and is_closed
then net_arr else 0 end) as predicted_will_contract_narr_commit,

sum(case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Renew'
and is_closed
and trx_type <> 'Churn'
then net_arr else 0 end) as predicted_will_renew_narr_best_case,
sum(case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Renew'
and is_closed
then net_arr else 0 end) as predicted_will_renew_narr_commit,


sum(case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Churn (Actionable)'
and is_closed
then atr else 0 end) as predicted_will_churn_atr,
sum(case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Contract (Actionable)'
and is_closed
then atr else 0 end) as predicted_will_contract_atr,
sum(case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Renew'
and is_closed
then atr else 0 end) as predicted_will_renew_atr
from full_base_data
where
close_date >= dateadd('month',-12,date_trunc('month',current_date))
and ((is_closed and close_month < date_trunc('month',current_date)))
and sales_type = 'Renewal'
and team_fixed <> 'Other'
and herbie_oppty_flag
group by 1,2,3
)
,
renewal_forecast_rates as 
(
select *,
percentile_cont(.75) within group (order by renewal_growth_rate asc) over(partition by team_fixed,calculated_tier) as renewal_growth_rate_best_case,
percentile_cont(.2) within group (order by renewal_growth_rate asc) over(partition by team_fixed,calculated_tier) as renewal_growth_rate_commit,
percentile_cont(.4) within group (order by renewal_growth_rate asc) over(partition by team_fixed,calculated_tier) as renewal_growth_rate_most_likely,
percentile_cont(.75) within group (order by c_and_c_rate asc) over(partition by team_fixed,calculated_tier) as c_and_c_rate_best_case,
percentile_cont(.2) within group (order by c_and_c_rate asc) over(partition by team_fixed,calculated_tier) as c_and_c_rate_commit,
percentile_cont(.4) within group (order by c_and_c_rate asc) over(partition by team_fixed,calculated_tier) as c_and_c_rate_most_likely,
sum(predicted_will_churn_narr_best_case)  over (partition by team_fixed,calculated_tier)
 / 
 sum(predicted_will_churn_atr) over (partition by team_fixed,calculated_tier) as predicted_atr_rate_will_churn_best_case,

 sum(predicted_will_contract_narr_best_case)  over (partition by team_fixed,calculated_tier)
 / 
 sum(predicted_will_contract_atr) over (partition by team_fixed,calculated_tier) as predicted_atr_rate_will_contract_best_case,

 sum(predicted_will_renew_narr_best_case)  over (partition by team_fixed,calculated_tier)
 / 
 sum(predicted_will_renew_atr) over (partition by team_fixed,calculated_tier) as predicted_atr_rate_will_renew_best_case,

 sum(predicted_will_churn_narr_commit )  over (partition by team_fixed,calculated_tier)
 / 
 sum(predicted_will_churn_atr) over (partition by team_fixed,calculated_tier) as predicted_atr_rate_will_churn_commit,

 sum(predicted_will_contract_narr_commit)  over (partition by team_fixed,calculated_tier)
 / 
 sum(predicted_will_contract_atr) over (partition by team_fixed,calculated_tier) as predicted_atr_rate_will_contract_commit,

 sum(predicted_will_renew_narr_commit)  over (partition by team_fixed,calculated_tier)
 / 
 sum(predicted_will_renew_atr) over (partition by team_fixed,calculated_tier) as predicted_atr_rate_will_renew_commit
 from renewal_forecast_base_data
)
,
renewal_arr_predictions as
(
select
full_base_data.*,
renewal_forecast_rates.* EXCLUDE (team_fixed,calculated_tier),
--Best Case
case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C is not null then 
  atr * (case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Churn (Actionable)' then predicted_atr_rate_will_churn_best_case
              when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Contract (Actionable)' then predicted_atr_rate_will_contract_best_case
              when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Renew' then predicted_atr_rate_will_renew_best_case
              else 0 end)
  else atr * (renewal_growth_rate_best_case + c_and_c_rate_best_case) end as predicted_renewal_arr_best_case,
atr * (renewal_growth_rate_best_case + c_and_c_rate_best_case) as predicted_renewal_arr_best_case_no_ai,
--Commit
case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C is not null then 
  atr * (case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Churn (Actionable)' then (predicted_atr_rate_will_churn_best_case + predicted_atr_rate_will_churn_commit)/2
              when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Contract (Actionable)' then (predicted_atr_rate_will_contract_best_case + predicted_atr_rate_will_contract_commit)/2
              when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Renew' then (predicted_atr_rate_will_renew_best_case + predicted_atr_rate_will_renew_commit)/2
              else 0 end)
  else atr * (renewal_growth_rate_most_likely + c_and_c_rate_most_likely) end as predicted_renewal_arr_most_likely,
atr * (renewal_growth_rate_most_likely + c_and_c_rate_most_likely) as predicted_renewal_arr_most_likely_no_ai,
--Most Likely
case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C is not null then 
  atr * (case when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Churn (Actionable)' then predicted_atr_rate_will_churn_commit
              when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Contract (Actionable)' then predicted_atr_rate_will_contract_commit
              when PTC_PREDICTED_RENEWAL_RISK_CATEGORY__C = 'Will Renew' then predicted_atr_rate_will_renew_commit
              else 0 end)
  else atr * (renewal_growth_rate_commit + c_and_c_rate_commit) end as predicted_renewal_arr_commit,
atr * (renewal_growth_rate_commit + c_and_c_rate_commit) as predicted_renewal_arr_commit_no_ai,
atr * c_and_c_rate_best_case as predicted_c_and_c_arr_best_case,
atr * c_and_c_rate_commit as predicted_c_and_c_arr_commit,
atr * c_and_c_rate_most_likely as predicted_c_and_c_arr_most_likely
from
full_base_data
left join 
(
select distinct 
team_fixed,
calculated_tier,
renewal_growth_rate_best_case,
renewal_growth_rate_most_likely,
renewal_growth_rate_commit,
c_and_c_rate_best_case,
c_and_c_rate_most_likely,
c_and_c_rate_commit,
predicted_atr_rate_will_churn_best_case,
predicted_atr_rate_will_churn_commit,
predicted_atr_rate_will_contract_best_case,
predicted_atr_rate_will_contract_commit,
predicted_atr_rate_will_renew_best_case,
predicted_atr_rate_will_renew_commit
from
renewal_forecast_rates
) renewal_forecast_rates
on full_base_data.team_fixed = renewal_forecast_rates.team_fixed
and full_base_data.calculated_tier = renewal_forecast_rates.calculated_tier
--and month(full_base_data.close_month = renewal_forecast_rates.close_month
and full_base_data.sales_type = 'Renewal'
and full_base_data.is_closed = false
and full_base_data.fiscal_year = 2025
where sales_type = 'Renewal'
and herbie_oppty_flag
and eoa_account_flag
)
,
renewal_forecast_output as
(
select
'Renewal' as forecast_type,
team_fixed,
calculated_tier,
close_month,
sum(case when is_closed then net_arr else 0 end) as total_actual_narr,
sum(case when is_closed and net_arr < 0 then net_arr else 0 end) as total_actual_c_and_c_narr,
sum(case when is_closed and net_arr > 0 then net_arr else 0 end) as total_actual_renewal_growth_narr,
sum(case when is_closed then atr else 0 end) as total_closed_atr,
sum(atr) as total_atr,
sum(case when is_closed = false then atr else 0 end) as total_open_atr,
sum(case when PTC_PREDICTED_ARR__C is not null and is_closed = false then PTC_PREDICTED_ARR__C else 0 end) - 
sum(case when PTC_PREDICTED_ARR__C is not null and is_closed = false then atr else 0 end) as ai_atr_net_forecast,
sum(predicted_renewal_arr_best_case) as predicted_renewal_arr_best_case,
sum(predicted_renewal_arr_commit) as predicted_renewal_arr_commit,
sum(predicted_renewal_arr_most_likely) as predicted_renewal_arr_most_likely,
sum(predicted_renewal_arr_best_case_no_ai) as predicted_renewal_arr_best_case_no_ai,
sum(predicted_renewal_arr_commit_no_ai) as predicted_renewal_arr_commit_no_ai,
sum(predicted_renewal_arr_most_likely_no_ai) as predicted_renewal_arr_most_likely_no_ai,
sum(predicted_c_and_c_arr_best_case) as predicted_c_and_c_arr_best_case,
sum(predicted_c_and_c_arr_commit) as predicted_c_and_c_arr_commit,
sum(predicted_c_and_c_arr_most_likely) as predicted_c_and_c_arr_most_likely
from renewal_arr_predictions
where fiscal_year = 2025
group by 1,2,3,4
),

fo_forecast_base_data as
(
select
team_fixed,
dateadd('year',1,close_month) as fy25_close_month,
count(distinct dim_crm_opportunity_id) as fo_count,
sum(net_arr) as total_net_arr,
count(distinct case when free_promo_account_flag = false and price_increase_promo_account_flag = false then dim_crm_opportunity_id else null end) as fo_count_without_promos,
sum(case when free_promo_account_flag = false and price_increase_promo_account_flag = false then net_arr else 0 end) as net_arr_without_promos,
total_net_arr / fo_count as total_actual_asp,
net_arr_without_promos / fo_count_without_promos as actual_asp_no_promos
from full_base_data
where
close_date >= dateadd('month',-12,date_trunc('month',current_date))
and ((is_closed and close_month < date_trunc('month',current_date)))
and trx_type = 'First Order'
and team_fixed <> 'Other'
and herbie_oppty_flag
and is_won
group by 1,2
),

date_spine as
(
(select 
date_actual,
'EMEA' as team_fixed
 from common.dim_date
where
date_actual = date_trunc('month',date_actual)
and fiscal_year = 2025)
union all
(select 
date_actual,
'AMER/APAC' as team_fixed
 from common.dim_date
where
date_actual = date_trunc('month',date_actual)
and fiscal_year = 2025)
),

fo_forecast_rates as
(
select 
distinct
team_fixed,
percentile_cont(.75) within group (order by actual_asp_no_promos asc) over(partition by team_fixed) as asp_best_case,
percentile_cont(.2) within group (order by actual_asp_no_promos asc) over(partition by team_fixed) as asp_commit,
percentile_cont(.4) within group (order by actual_asp_no_promos asc) over(partition by team_fixed) as asp_most_likely,
percentile_cont(.75) within group (order by fo_count_without_promos asc) over(partition by team_fixed) as fo_count_best_case,
percentile_cont(.2) within group (order by fo_count_without_promos asc) over(partition by team_fixed) as fo_count_commit,
percentile_cont(.4) within group (order by fo_count_without_promos asc) over(partition by team_fixed) as fo_count_most_likely
from fo_forecast_base_data
)
,
fo_forecast_output as
(
select 
'First Order' as forecast_type,
date_spine.team_fixed,
null as calculated_tier,
date_spine.date_actual as close_month,
fo_forecast_rates.* EXCLUDE (team_fixed),
asp_best_case * fo_count_best_case as predicted_fo_arr_best_case,
asp_commit * fo_count_commit as predicted_fo_arr_commit,
asp_most_likely * fo_count_most_likely as predicted_fo_arr_most_likely
 from date_spine
 left join fo_forecast_rates on date_spine.team_fixed = fo_forecast_rates.team_fixed
)
,
get_monthly_carr as
(
select
dateadd('year',1,snapshot_date) as fy25_target_month,
CASE WHEN parent_crm_account_geo in ('AMER', 'APAC','LATAM') then 'AMER/APAC' 
     WHEN parent_crm_account_geo in ('EMEA','META') then 'EMEA' 
     ELSE 'Other' end as team_fixed,
calculated_tier,
case when
((carr_account_family <= 30000 and carr_this_account > 0))
and (parent_crm_account_max_family_employee <= 100 or parent_crm_account_max_family_employee is null)
and parent_crm_account_sales_segment in ('SMB','Mid-Market','Large')
and parent_crm_account_upa_country <> 'JP'
and is_jihu_account = false
and dim_parent_crm_account_id not in
(
    select
distinct
dim_parent_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = date_trunc('month',current_date)
and product_tier_name like '%Ultimate%'
)
then true else false end as herbie_acct_flag,
sum(carr_this_account) as total_monthly_carr,
sum(case when eoa_flag then carr_this_account else 0 end) as total_eoa_carr
from account_base
where snapshot_date >= dateadd('month',-12,date_trunc('month',current_date))
and snapshot_date < date_trunc('month',current_date)
and snapshot_date = date_trunc('month',snapshot_date)
and herbie_acct_flag
and team_fixed <> 'Other'
group by 1,2,3,4
),

nonrenewal_growth_base_data as
(
select
team_fixed,
calculated_tier,
dateadd('year',1,close_month) as fy25_close_month,
sum(net_arr) as total_net_arr
from full_base_data
where
close_date >= dateadd('month',-12,date_trunc('month',current_date))
and is_closed and close_month < date_trunc('month',current_date)
and trx_type_grouping = 'Nonrenewal Growth'
and team_fixed <> 'Other'
and herbie_oppty_flag
and is_won
group by 1,2,3
),

nonrenewal_growth_rates as 
(
select
nonrenewal_growth_base_data.*,
get_monthly_carr.total_monthly_carr,
total_net_arr / get_monthly_carr.total_monthly_carr as nonrenewal_growth_rate
from nonrenewal_growth_base_data left join get_monthly_carr
 on nonrenewal_growth_base_data.team_fixed = get_monthly_carr.team_fixed
 and nonrenewal_growth_base_data.calculated_tier = get_monthly_carr.calculated_tier
 and nonrenewal_growth_base_data.fy25_close_month = get_monthly_carr.fy25_target_month
)
,
nonrenewal_growth_calcs as
(
select *,
percentile_cont(.75) within group (order by nonrenewal_growth_rate asc) over(partition by team_fixed,calculated_tier) as nonrenewal_growth_best_case,
percentile_cont(.2) within group (order by nonrenewal_growth_rate asc) over(partition by team_fixed,calculated_tier) as nonrenewal_growth_commit,
percentile_cont(.4) within group (order by nonrenewal_growth_rate asc) over(partition by team_fixed,calculated_tier) as nonrenewal_growth_most_likely
 from nonrenewal_growth_rates
)
,
nonrenewal_growth_output as
(
select
distinct
nonrenewal_growth_calcs.* EXCLUDE (fy25_close_month,total_net_arr,total_monthly_carr,nonrenewal_growth_rate),
carr.total_eoa_carr as carr
from nonrenewal_growth_calcs
left join 
(
select team_fixed, calculated_tier,
total_eoa_carr
from
get_monthly_carr
where fy25_target_month = dateadd('month',11,date_trunc('month',current_date))
) carr
on nonrenewal_growth_calcs.team_fixed = carr.team_fixed
 and nonrenewal_growth_calcs.calculated_tier = carr.calculated_tier
)
,
combined_output as
(
select *

 from
(select
forecast_type,
renewal_forecast_output.team_fixed,
renewal_forecast_output.calculated_tier,
close_month,
ai_atr_net_forecast,
predicted_renewal_arr_best_case,
predicted_renewal_arr_commit,
predicted_renewal_arr_most_likely,
predicted_renewal_arr_best_case_no_ai,
predicted_renewal_arr_commit_no_ai,
predicted_renewal_arr_most_likely_no_ai,
predicted_c_and_c_arr_best_case,
predicted_c_and_c_arr_commit,
predicted_c_and_c_arr_most_likely,
0 as asp_best_case,
0 as asp_commit,
0 as asp_most_likely,
0 as fo_count_best_case,
0 as fo_count_commit,
0 as fo_count_most_likely,
0 as predicted_fo_arr_best_case,
0 as predicted_fo_arr_commit,
0 as predicted_fo_arr_most_likely,
nonrenewal_growth_best_case,
nonrenewal_growth_commit,
nonrenewal_growth_most_likely,
carr as current_carr
from renewal_forecast_output
left join nonrenewal_growth_output on renewal_forecast_output.team_fixed = nonrenewal_growth_output.team_fixed
and renewal_forecast_output.calculated_tier = nonrenewal_growth_output.calculated_tier)
-- union all
-- (
-- select
-- forecast_type,
-- team_fixed,
-- calculated_tier,
-- close_month,
-- 0 as ai_atr_net_forecast,
-- 0 as predicted_renewal_arr_best_case,
-- 0 as predicted_renewal_arr_commit,
-- 0 as predicted_renewal_arr_most_likely,
-- 0 as predicted_renewal_arr_best_case_no_ai,
-- 0 as predicted_renewal_arr_commit_no_ai,
-- 0 as predicted_renewal_arr_most_likely_no_ai,
-- 0 as predicted_c_and_c_arr_best_case,
-- 0 as predicted_c_and_c_arr_commit,
-- 0 as predicted_c_and_c_arr_most_likely,
-- asp_best_case,
-- asp_commit,
-- asp_most_likely,
-- fo_count_best_case,
-- fo_count_commit,
-- fo_count_most_likely,
-- predicted_fo_arr_best_case,
-- predicted_fo_arr_commit,
-- predicted_fo_arr_most_likely,
-- 0 as nonrenewal_growth_best_case,
-- 0 as nonrenewal_growth_commit,
-- 0 as nonrenewal_growth_most_likely,
-- 0 as current_carr
-- from fo_forecast_output
-- )
),

actual_data as
(
select
close_month,
team_fixed,
case when trx_type_grouping = 'First Order' then null else calculated_tier end as calculated_tier,
case when trx_type_grouping in ('Renewal Growth','C&C','Nonrenewal Growth') then 'Renewal'
when trx_type_grouping = 'First Order' then 'First Order'
else null end as forecast_type,
sum(net_arr) as total_net_arr,
sum(case when is_won or (is_closed and forecast_type = 'Renewal') then net_arr else 0 end ) as closed_net_arr,
count(distinct dim_crm_opportunity_id) as total_trx,
count(distinct case when is_won then dim_crm_opportunity_id else null end) as won_trx,
sum(atr) as total_atr,
sum(case when is_won and trx_type_grouping = 'Nonrenewal Growth' then net_arr else 0 end) as closed_nonrenewal_arr,
sum(case when is_closed and forecast_type = 'Renewal' and net_arr < 0 then net_arr else 0 end) as closed_c_and_c_arr
from full_base_data
where forecast_type is not null
and herbie_oppty_flag
and fiscal_year = 2025
and eoa_account_flag
group by 1,2,3,4
)
,
add_actual_data as
(
select
combined_output.*,
zeroifnull(actual_data.total_net_arr) as total_net_arr_actual,
zeroifnull(actual_data.total_trx) as total_trx_actual,
zeroifnull(actual_data.total_atr) as total_atr_actual,
zeroifnull(actual_data.closed_net_arr) as closed_net_arr_actual,
zeroifnull(actual_data.won_trx) as won_trx_actual,
zeroifnull(actual_data.closed_c_and_c_arr) as closed_c_and_c_arr_actual,
zeroifnull(actual_data.closed_nonrenewal_arr) as closed_nonrenewal_arr_actual,
(datediff('day',greatest(current_date,combined_output.close_month),dateadd('month',1,combined_output.close_month))-1)
/ (datediff('day',combined_output.close_month,dateadd('month',1,combined_output.close_month))-1) 
as perc_month_remaining,
(zeroifnull(lag(predicted_renewal_arr_best_case * perc_month_remaining + closed_net_arr_actual,1) over(partition by combined_output.forecast_type,combined_output.team_fixed,combined_output.calculated_tier
order by combined_output.close_month asc)) + current_carr) * nonrenewal_growth_best_case * perc_month_remaining + closed_nonrenewal_arr_actual as predicted_nonrenewal_arr_best_case,
(zeroifnull(lag(predicted_renewal_arr_most_likely * perc_month_remaining + closed_net_arr_actual,1) over(partition by combined_output.forecast_type,combined_output.team_fixed,combined_output.calculated_tier
order by combined_output.close_month asc)) + current_carr) * nonrenewal_growth_most_likely * perc_month_remaining + closed_nonrenewal_arr_actual as predicted_nonrenewal_arr_most_likely,
(zeroifnull(lag(predicted_renewal_arr_commit * perc_month_remaining + closed_net_arr_actual,1) over(partition by combined_output.forecast_type,combined_output.team_fixed,combined_output.calculated_tier
order by combined_output.close_month asc)) + current_carr) * nonrenewal_growth_commit * perc_month_remaining + closed_nonrenewal_arr_actual as predicted_nonrenewal_arr_commit
from
combined_output
left join actual_data
on combined_output.close_month = actual_data.close_month
and combined_output.team_fixed = actual_data.team_fixed
and combined_output.calculated_tier = actual_data.calculated_tier
and combined_output.forecast_type = actual_data.forecast_type
where combined_output.team_fixed <> 'Other'
order by combined_output.forecast_type,combined_output.team_fixed,combined_output.calculated_tier,combined_output.close_month
)

select * from add_actual_data