--Gets product, price, quantity information for current customers (current month ARR > 0) that have future subscription end dates

with arr_data as
(
--Get current pricing and quantity info for customers renewing in future months
SELECT
*
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check,
case when monthly_price_per_user < 23 and product_tier_name like '%Premium%' then true else false end as fy25_premium_price_increase_flag,
case when fy25_premium_price_increase_flag then (
    (case when subscription_end_month > '2023-04-01' then 29 else 23.78 end)
     - monthly_price_per_user) * quantity else 0 end as fy25_estimated_price_increase_impact,
case when arr + fy25_estimated_price_increase_impact >= 7000 and arr < 7000 then true else false end as fy25_likely_price_increase_uptier_flag
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
WHERE ARR_MONTH = date_trunc('month',current_date)
and subscription_end_month > date_trunc('month',current_date)
and arr > 0
--and PRODUCT_TIER_NAME like '%Premium%'


)

select
*,
first_value(product_tier_name) over(partition by dim_crm_account_id order by arr desc) as primary_arr_product,
first_value(dim_subscription_id) over(partition by dim_crm_account_id order by arr desc) as primary_arr_sub_id
from arr_data
qualify dim_subscription_id = primary_arr_sub_id
