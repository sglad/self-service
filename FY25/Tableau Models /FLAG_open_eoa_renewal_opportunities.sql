--Gets all future renewal opportunities where the account currently has EoA special pricing

select
distinct dim_crm_opportunity_id_current_open_renewal,
price_after_renewal,
close_date
from
(select
mart_charge.*,
dim_subscription.dim_crm_opportunity_id,
close_date,
max(mart_charge.arr) over(partition by mart_charge.dim_subscription_id) as actual_arr,
max(mart_charge.quantity) over(partition by mart_charge.dim_subscription_id) as actual_quantity,
actual_arr/actual_quantity as actual_price,
previous_mrr / previous_quantity as previous_price,
mrr / quantity as price_after_renewal,
dim_subscription.dim_crm_opportunity_id_current_open_renewal
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
left join PROD.COMMON.DIM_SUBSCRIPTION
on mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
left join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY on dim_subscription.dim_crm_opportunity_id_current_open_renewal = mart_crm_opportunity.dim_crm_opportunity_id
where
mart_crm_opportunity.close_date >= '2024-02-01'
and mart_crm_opportunity.is_closed = false
and mart_charge.rate_plan_name like '%Premium%'
-- and 
-- mart_charge.term_start_date <= '2024-02-01'
and type_of_arr_change <> 'New'
and mart_charge.term_start_date >= '2022-02-01'
and price_after_renewal > previous_price
and previous_quantity <> 0
and quantity <> 0
and mart_charge.dim_crm_account_id not in
( select
  distinct
dim_crm_account_id
  from restricted_safe_common_mart_sales.mart_charge charge
  where 
  subscription_start_date >= '2023-02-01'
  and rate_plan_charge_description = 'fo-discount-70-percent')
and (lower(rate_plan_charge_description) like '%eoa%'
or
((previous_price >= 5 and previous_price <= 7)
or (previous_price >= 8 and previous_price <= 10)
or (previous_price >= 14 and previous_price <= 16)))
)
