with account_base as (
SELECT acct.*, 
CASE WHEN first_high_value_case.case_id IS NOT NULL then 'Tier 1'
     WHEN first_high_value_case.case_id IS NULL THEN
        CASE WHEN acct.carr_this_account >7000 then 'Tier 1' 
            WHEN acct.carr_this_account < 3000 AND acct.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
            ELSE 'Tier 2' end
    ELSE null END AS calculated_tier, 
CASE WHEN acct.parent_crm_account_geo in ('AMER', 'APAC') then 'AMER/APAC' 
     WHEN acct.parent_crm_account_geo in ('EMEA') then 'EMEA' 
     ELSE 'Other' end as Team,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
fo.SQS as fo_sqs,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr, 
case when fo_fiscal_year <= 2024 then False else True end as New_FY25_FO_Flag, 
first_high_value_case.created_date as first_high_value_case_created_date, 
high_value_case.case_owner_name as high_value_case_owner,
high_value_case.team as high_value_case_owner_team,
high_value_case.manager_name as high_value_case_owner_manager, 
start_values.carr_account_family as starting_carr_account_family, 
start_values.carr_this_account as starting_carr_this_account, 
CASE WHEN start_values.carr_this_account >7000 then 'Tier 1' 
     WHEN start_values.carr_this_account < 3000 AND start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
     else 'Tier 2' end as starting_calculated_tier,
start_values.pte_score as starting_pte_score, 
start_values.ptc_score as starting_ptc_score, 
start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as starting_parent_crm_account_lam_dev_count,
CASE WHEN EOA.dim_crm_account_id IS NOT NULL then True else False end as EOA_Flag, 
CASE WHEN free_promo.dim_crm_account_id IS NOT NULL then True else False end as Free_Promo_Flag, 
CASE WHEN price_increase.dim_crm_account_id IS NOT NULL then True else False end as Price_Increase_Promo_Flag,
CASE WHEN ultimate.dim_parent_crm_account_id is not null then true else false end as ultimate_customer_flag
FROM PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT acct
------subquery that gets latest FO data
LEFT JOIN 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year, 
    last_value(sales_qualified_source_name) over (partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as SQS
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
        ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo
    ON fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
---------subquery that gets latest churn data 
LEFT JOIN 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
        ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
     and not(product_category like '%torage%' or PRODUCT_DETAILS like '%torage%')  
    ) churn 
    ON churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
--------subquery to get high tier case owner
LEFT JOIN 
    (SELECT
    account_id,
    owner_id, 
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.crm_user_role_type
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    and subject = 'FY25 High Value Account' -----subject placeholder - this could change
    ) high_value_case
    ON high_value_case.account_id = acct.dim_crm_account_id
--------------subquery to get start of FY25 values
LEFT JOIN 
    (SELECT
    *
    FROM PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
    WHERE snapshot_date = '2024-02-05' -----placeholder date for start of year
    ) start_values
    ON start_values.dim_crm_account_id = acct.dim_crm_account_id
-----subquery to get FIRST high value case 
LEFT JOIN 
    (SELECT 
    *
    FROM (SELECT
    account_id,
    case_id,
    owner_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.crm_user_role_type,
    sfdc_case.created_date,
    MIN(sfdc_case.created_date) over(partition by ACCOUNT_ID) as first_high_value_date
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    and lower(subject) like '%high value account%') -----subject placeholder - this could change
    WHERE created_date = first_high_value_date 
    ) first_high_value_case
    ON first_high_value_case.account_id = acct.dim_crm_account_id

-----EOA cohort accounts
    LEFT JOIN 
    (SELECT
mart_arr.ARR_MONTH
--,ping_created_at
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
--,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
--,product_rate_plan_name
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
)) EOA 
    ON EOA.dim_crm_account_id = acct.dim_crm_account_id 
------free limit promo cohort accounts
LEFT JOIN 
    (select
  distinct
dim_crm_account_id
  from restricted_safe_common_mart_sales.mart_charge charge
  where 
  subscription_start_date >= '2023-02-01'
  and rate_plan_charge_description = 'fo-discount-70-percent') free_promo
    ON free_promo.dim_crm_account_id = acct.dim_crm_account_id 
------price increase promo cohort accounts
LEFT JOIN
    (select distinct dim_crm_account_id from
        (
         select
          charge.*,
          arr / quantity as actual_price,
          prod.annual_billing_list_price as list_price
          from restricted_safe_common_mart_sales.mart_charge charge
          inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
          where 
          subscription_start_date >= '2023-04-01'
          and subscription_start_date <= '2023-07-01'
          and TYPE_OF_ARR_CHANGE = 'New'
          and quantity > 0
          and actual_price > 228
          and actual_price < 290
          and rate_plan_charge_name like '%Premium%'
        )) price_increase
        ON price_increase.dim_crm_account_id = acct.dim_crm_account_id 
LEFT JOIN
(
    select
distinct
dim_parent_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = date_trunc('month',current_date)
and product_tier_name like '%Ultimate%'
) ultimate on acct.dim_parent_crm_account_id = ultimate.dim_parent_crm_account_id
-------filtering to get current account data     
WHERE acct.snapshot_date = current_date), 

account_blended as (
SELECT 
a.dim_crm_account_id as account_id, 
a.account_owner,
a.USER_ROLE_TYPE, 
a.CRM_ACCOUNT_OWNER_GEO,
a.PARENT_CRM_ACCOUNT_SALES_SEGMENT, 
a.PARENT_CRM_ACCOUNT_BUSINESS_UNIT, 
a.NEXT_RENEWAL_DATE, 
a.calculated_tier, 
a.team, 
a.high_value_case_owner,
a.high_value_case_owner_team,
a.high_value_case_owner_manager, 
a.count_active_subscriptions,
DATEDIFF('day', current_date, a.next_renewal_date) as days_till_next_renewal,
-- DATEDIFF('day', last_upload_date, a.next_renewal_date) as days_from_last_upload_to_next_renewal,
a.CARR_ACCOUNT_FAMILY as CARR_ACCOUNT_FAMILY,
a.six_sense_account_buying_stage, 
a.SIX_SENSE_ACCOUNT_PROFILE_FIT, 
a.SIX_SENSE_ACCOUNT_INTENT_SCORE,
a.SIX_SENSE_ACCOUNT_UPDATE_DATE,
a.GS_HEALTH_USER_ENGAGEMENT,
a.GS_HEALTH_CD,
a.GS_HEALTH_DEVSECOPS,
a.GS_HEALTH_CI, 
a.GS_HEALTH_SCM, 
a.GS_FIRST_VALUE_DATE,
a.pte_score, 
a.ptc_score,
a.GS_LAST_CSM_ACTIVITY_DATE,
DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, current_date) as days_since_6sense_account_update,
-- DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, last_upload_date) as days_since_6sense_account_update_last_upload,
o.dim_crm_opportunity_id as opportunity_id,
o.owner_id as opportunity_owner_id,
CASE WHEN o.sales_type = 'Renewal' and o.close_date >= '2024-02-01' and o.close_date <= '2025-01-31' THEN True ELSE False end as FY25_Renewal,
o.is_closed, 
o.is_won,
o.sales_type, 
o.close_date, 
o.opportunity_term, 
o.opportunity_name,
o.QSR_status,
o.stage_name,
s.dim_subscription_id as sub_subscription_id, 
s.dim_subscription_id_original as sub_subscription_id_original,
s.subscription_status, 
s.term_start_date as current_subscription_start_date,
s.term_end_date as current_subscription_end_date,
DATEDIFF('day', s.term_start_date, current_date) as days_into_current_subscription,
s.turn_on_auto_renewal,
COUNT(distinct DIM_SUBSCRIPTION_ID) OVER (PARTITION BY s.dim_crm_account_id) AS count_subscriptions,
DATEDIFF('day', s.term_end_date, current_date) as days_since_current_sub_end_date,
-- DATEDIFF('day', s.term_end_date, last_upload_date) as days_since_current_sub_end_date_last_upload,
s.dim_subscription_id,
CASE WHEN sales_type = 'Renewal' then DATEDIFF('day', current_date, o.close_date) else null end as days_till_close,
-- CASE WHEN sales_type = 'Renewal' then DATEDIFF('day', current_date, last_upload_date) else null end as days_till_close_last_upload,
CASE WHEN o.opportunity_name LIKE '%QSR%' THEN TRUE ELSE FALSE END AS QSR_Flag,
CASE WHEN qsr_flag = false and sales_type != 'Renewal' then TRUE else FALSE END AS non_qsr_non_renewal_oppty_flag,
CASE WHEN o.sales_type = 'Renewal' THEN TRUE ELSE FALSE END AS Renewal_Flag,
a.EOA_flag,
o.auto_renewal_status,
CASE WHEN (CONTAINS(opportunity_name, '#ultimateupgrade') or CONTAINS(opportunity_name, 'Ultimate Upgrade') or CONTAINS(opportunity_name, 'Upgrade to Ultimate') or CONTAINS(opportunity_name, 'ultimate upgrade')) THEN TRUE ELSE FALSE END AS Ultimate_Upgrade_Oppty_Flag
FROM account_base a
LEFT JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o
    ON a.dim_crm_account_id = o.dim_crm_account_id
    --AND o.is_closed = False
    AND DATEDIFF('day', current_date, o.close_date) <= 365
    AND sales_type = 'Renewal'
    AND (close_date >= '2024-02-01' and close_date <= '2025-01-31')
LEFT JOIN PROD.COMMON.DIM_SUBSCRIPTION s
    ON o.dim_crm_opportunity_id = s.dim_crm_opportunity_id_current_open_renewal
    and subscription_status = 'Active'
-- LEFT JOIN (SELECT distinct dim_crm_account_id, PO_required FROM PROD.common.dim_billing_account WHERE PO_REQUIRED = 'YES') b 
--     ON b.dim_crm_account_id = a.dim_crm_account_id
-- -- LEFT JOIN PROD.common.dim_billing_account b
-- --     ON a.dim_crm_account_id = b.dim_crm_account_id
-- LEFT JOIN (SELECT *, max(score_date) over(partition by CRM_ACCOUNT_ID) as latest_score_date FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTC_SCORES"  QUALIFY score_date = latest_score_date) ptc 
--     ON ptc.crm_account_id = a.dim_crm_account_id
-- LEFT JOIN (SELECT *, max(score_date) over(partition by CRM_ACCOUNT_ID) as latest_score_date FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTE_SCORES"  QUALIFY score_date = latest_score_date) pte 
--     ON pte.crm_account_id = a.dim_crm_account_id
-- LEFT JOIN last_case_data cd 
--     ON cd.account_id = a.dim_crm_account_id
-- LEFT JOIN last_renewal_case_data lr 
--     ON lr.account_id = a.dim_crm_account_id
-- LEFT JOIN open_cases oc
--     ON oc.account_id = a.dim_crm_account_id
-- LEFT JOIN open_renewal_opps oo
--     ON oo.dim_crm_account_id = a.dim_crm_account_id
-- LEFT JOIN low_adoption_last_180 
--     ON low_adoption_last_180.account_id = a.dim_crm_account_id
-- LEFT JOIN high_adoption_last_180
--     ON high_adoption_last_180.account_id = a.dim_crm_account_id
-- LEFT JOIN qsr_failed_last_75 qsr
--     ON qsr.account_id = a.dim_crm_account_id
--     AND qsr.opportunity_id = o.dim_crm_opportunity_id
-- LEFT JOIN latest_ptc_score lptc
-- LEFT JOIN latest_pte_score lpte
-- LEFT JOIN high_value_case_last_90
--     ON high_value_case_last_90.account_id = a.dim_crm_account_id
-- JOIN last_created_date
---------------------currently a placeholder and will need to be substituted in for all SMB sements--------
WHERE a.PARENT_CRM_ACCOUNT_SALES_SEGMENT = 'SMB'
AND carr_account_family < 30000),





utilization as (
SELECT 
mart_arr.ARR_MONTH
,monthly_mart.snapshot_month 
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
,monthly_mart.max_BILLABLE_USER_COUNT
,monthly_mart.LICENSE_USER_COUNT
,monthly_mart.subscription_start_date
,monthly_mart.subscription_end_date
,monthly_mart.term_end_date
,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)*(monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as overage_amount
,max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
LEFT JOIN 
  (select
  DISTINCT
    snapshot_month,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    INSTANCE_TYPE,
    subscription_status,
    subscription_start_date,
    subscription_end_date,
    term_end_date, 
    LICENSE_USER_COUNT,
    MAX(BILLABLE_USER_COUNT) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as max_billable_user_count
    FROM COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'  
    and snapshot_month = DATE_TRUNC('month',CURRENT_DATE)
  ) monthly_mart
      ON mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL = monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL
WHERE ARR_MONTH = snapshot_month
and PRODUCT_TIER_NAME not like '%Storage%'
), 

----------------------------get the date on a subscription when a customer switches from auto-renew on to auto-renew off
autorenew_switch as (
select
dim_crm_account_id, 
dim_subscription_id,
active_autorenew_status,
prior_auto_renewal,
MAX(snapshot_date) as latest_switch_date
from
(
  select 
date_actual as snapshot_date,
PREP_BILLING_ACCOUNT_USER.user_name as update_user,
PREP_BILLING_ACCOUNT_USER.IS_INTEGRATION_USER,
TURN_ON_AUTO_RENEWAL as active_autorenew_status,
lag(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as prior_auto_renewal,
lead(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as future_auto_renewal,
sub.*
from common.dim_subscription_snapshot_bottom_up sub
inner join common.dim_date on sub.snapshot_id = dim_date.date_id
inner join restricted_safe_common_prep.PREP_BILLING_ACCOUNT_USER on sub.UPDATED_BY_ID = PREP_BILLING_ACCOUNT_USER.ZUORA_USER_ID
where snapshot_date >= '2023-02-01'
  and snapshot_date < CURRENT_DATE
  and subscription_status = 'Active'
 -- and update_user = 'svc_zuora_fulfillment_int@gitlab.com'
--and (TURN_ON_AUTO_RENEWAL = 'Yes' or TURN_ON_AUTO_RENEWAL is null)
order by snapshot_date asc
)
where
((active_autorenew_status = 'No' and update_user = 'svc_zuora_fulfillment_int@gitlab.com') and prior_auto_renewal = 'Yes')
GROUP BY 1,2,3,4
 ),




-----------brings together account blended date, utilization data, and autorenewal switch data 
all_data as(
SELECT a.*, 
u.overage_count,
u.overage_amount,
u.latest_overage_month,
u.qsr_enabled_flag,
u.max_billable_user_count,
u.LICENSE_USER_COUNT,
u.ARR,
s.latest_switch_date, 
DATEDIFF('day', latest_switch_date, current_date) as days_since_autorenewal_switch 
FROM account_blended a
LEFT JOIN utilization u
    ON u.dim_crm_account_id = a.account_id
    --and term_end_date = current_subscription_end_date
    and u.dim_subscription_id_original = a.sub_subscription_id_original
LEFT JOIN autorenew_switch s
    ON s.dim_crm_account_id = a.account_id
    and s.dim_subscription_id = sub_subscription_id), 

    
tier1 as (
SELECT *, 
'FY25 High Value Account ' as case_trigger
FROM all_data
WHERE calculated_tier = 'Tier 1'
and opportunity_id IS NOT NULL) 

SELECT 
t.case_trigger as case_trigger,
t.account_id,
t.calculated_tier as account_tier,
cd.case_trigger_id,  
cd.status, 
cd.case_origin, 
cd.type, 
case_subject, 
'TO BE CARVED' as owner_id, --------placeholder since these cases will be assigned to advocates
cd.case_reason,
cd.record_type_id,
cd.priority,
t.opportunity_id as case_opportunity_id, 
cd.case_cta, 
CONCAT('Renewal Date: ', close_date, ';', ' PTC Score ', ' ', IFNULL(ptc_score, 0000), ';', ' PTE Score ', ' ', IFNULL(pte_score, 0000), ';', ' GS_HEALTH_USER_ENGAGEMENT ', ' ', IFNULL(GS_HEALTH_USER_ENGAGEMENT, 'NOT AVAILABLE'), ';', ' GS_HEALTH_CD ', ' ', IFNULL(GS_HEALTH_CD,'NOT AVAILABLE'), ';', ' GS_HEALTH_DEVSECOPS ', ' ', IFNULL(GS_HEALTH_DEVSECOPS, 'NOT AVAILABLE'), ';', ' GS_HEALTH_CI ', ' ', IFNULL(GS_HEALTH_CI, 'NOT AVAILABLE'), ';', ' GS_HEALTH_SCM ', ' ', IFNULL(GS_HEALTH_SCM, 'NOT AVAILABLE'), ';', ' GS_FIRST_VALUE_DATE ', ' ', IFNULL(CAST(GS_FIRST_VALUE_DATE AS DATE), '3000-02-01'), ';', ' Billable Users:', ' ', IFNULL(max_BILLABLE_USER_COUNT, 0000), ';', ' Licensed Users:', ' ', IFNULL(LICENSE_USER_COUNT, 0000), ';', ' Overage Amount:', ' ', IFNULL(overage_amount, 0000), ';', 'ARR:', ' ', IFNULL(ARR, 0000), ';' ) as context
FROM tier1 t
LEFT JOIN boneyard.case_data cd
    ON t.case_trigger = cd.case_trigger




 










