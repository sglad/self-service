with high_adoption_Last_180 as 
(
SELECT distinct
account_id, 
CASE WHEN (contains(lower(subject), 'pte') OR contains(lower(subject), 'high adoption') OR contains(lower(subject), 'overage') OR contains(lower(subject), '6sense')) then True else False END AS HA_Last_180
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE  DATEDIFF('day', created_date, current_date) <= 180
and record_type_id in ('0128X000001pPRkQAM')
),

low_adoption_Last_180 as
(SELECT distinct
account_id, 
CASE WHEN (contains(lower(subject), 'ptc') OR contains(lower(subject), 'low adoption') OR contains(lower(subject), 'underutilization')) then True else False END AS LA_Last_180
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE  DATEDIFF('day', created_date, current_date) <= 180
and record_type_id in ('0128X000001pPRkQAM') 
),

qsr_failed_last_75 as 
(
SELECT distinct
account_id,
RIGHT(description, 18) as opportunity_id, -----placeholder to get this until opportunity ID is it's own field
CASE WHEN contains(lower(subject), 'failed qsr') then True else False end as QSR_Failed_last_75
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE  DATEDIFF('day', created_date, current_date) <= 75
and record_type_id in ('0128X000001pPRkQAM') 
and contains(lower(subject), 'failed qsr')
),


last_case_data as (
SELECT *
FROM (
SELECT *,
ROW_NUMBER() OVER (PARTITION BY account_ID ORDER BY created_date desc) as rn
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE record_type_id in ('0128X000001pPRkQAM')
and status = 'Open'
)
WHERE rn = 1
),

last_renewal_case_data as (
SELECT *
FROM (
SELECT *,
ROW_NUMBER() OVER (PARTITION BY account_ID ORDER BY created_date desc) as rn
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE record_type_id in ('0128X000001pPRkQAM')
and contains(subject, 'Renewal') ----currently a placeholder until we can determine what we need to be inclusive of all renewal cases
)
WHERE rn = 1
),

open_cases as (
SELECT
account_id, 
COUNT(distinct case_id) as count_cases
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE record_type_id in ('0128X000001pPRkQAM') 
AND status = 'Open'
GROUP BY 1
),

open_renewal_opps as (
SELECT
dim_crm_account_id, 
count(distinct dim_crm_opportunity_id) as count_open_opps
FROM PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
WHERE sales_type = 'Renewal'
AND IS_Closed = False
GROUP BY 1
),

latest_pte_score as (
SELECT 
MAX(score_date) as most_recent_pte
FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTE_SCORES"
), 

latest_ptc_score as (
SELECT 
MAX(score_date) as most_recent_ptc
FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTC_SCORES"
),

--------------this table pulls in all of the data for the accounts, opportunities, subscription, and billing account (PO required field only) to act as the base table for the query 
account_base as(
SELECT 
a.dim_crm_account_id as account_id, 
a.account_owner,
a.USER_ROLE_TYPE, 
a.CRM_ACCOUNT_OWNER_GEO,
a.PARENT_CRM_ACCOUNT_SALES_SEGMENT, 
a.PARENT_CRM_ACCOUNT_BUSINESS_UNIT, 
a.NEXT_RENEWAL_DATE, 
a.count_active_subscriptions,
DATEDIFF('day', current_date, a.next_renewal_date) as days_till_next_renewal,
a.CARR_ACCOUNT_FAMILY as CARR_ACCOUNT_FAMILY,
a.six_sense_account_buying_stage, 
a.SIX_SENSE_ACCOUNT_PROFILE_FIT, 
a.SIX_SENSE_ACCOUNT_INTENT_SCORE,
a.SIX_SENSE_ACCOUNT_UPDATE_DATE,
a.GS_HEALTH_USER_ENGAGEMENT,
a.GS_HEALTH_CD,
a.GS_HEALTH_DEVSECOPS,
a.GS_HEALTH_CI, 
a.GS_HEALTH_SCM, 
a.GS_FIRST_VALUE_DATE,
a.GS_LAST_CSM_ACTIVITY_DATE,
DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, current_date) as days_since_6sense_account_update,
o.dim_crm_opportunity_id as opportunity_id,
o.is_closed, 
o.sales_type, 
o.close_date, 
o.opportunity_term, 
o.opportunity_name,
o.QSR_status,
qsr.qsr_failed_last_75,
s.dim_subscription_id as sub_subscription_id, 
s.dim_subscription_id_original as sub_subscription_id_original,
s.subscription_status, 
s.term_end_date as current_subscription_end_date,
s.turn_on_auto_renewal,
ptc.score_date as ptc_score_date, 
DATEDIFF('day', ptc.score_date, current_date) as days_since_last_ptc_score, 
ptc.score as ptc_score, 
ptc.score_group as ptc_score_group,
ptc.latest_score_date as ptc_latest_score_date,
ptc.insights as ptc_insights,
pte.score_date as pte_score_date, 
DATEDIFF('day', pte.score_date, current_date) as days_since_last_pte_score,
pte.score as pte_score, 
pte.score_group as pte_score_group,
pte.latest_score_date as pte_latest_score_date,
pte.insights as pte_insights,
lpte.most_recent_pte, 
lptc.most_recent_ptc,
cd.created_date as last_case_created_date, 
DATEDIFF('day', CAST(cd.created_date as date), current_date) as days_since_last_case_created,
cd.case_id as last_case_id, 
cd.subject as last_case_subject, 
cd.status as last_case_status, 
cd.owner_id as last_case_owner_id, 
DATEDIFF('day', CAST(lr.created_date as date), current_date) as days_since_last_renewal_case_created,
lr.case_id as last_renewal_case_id, 
lr.subject as last_renewal_case_subject, 
lr.status as last_renewal_case_status, 
lr.owner_id as last_renewal_case_owner_id,
oc.count_cases as current_open_cases,
oo.count_open_opps,
COUNT(distinct DIM_SUBSCRIPTION_ID) OVER (PARTITION BY s.dim_crm_account_id) AS count_subscriptions,
CASE WHEN HA_last_180 = True then TRUE else FALSE end as HA_LAST_180, 
CASE WHEN LA_last_180 = TRUE then TRUE else FALSE end as LA_last_180,
DATEDIFF('day', s.term_end_date, current_date) as days_since_current_sub_end_date,
s.dim_subscription_id,
CASE WHEN sales_type = 'Renewal' then DATEDIFF('day', current_date, o.close_date) else null end as days_till_close,
CASE WHEN o.opportunity_name LIKE '%QSR%' THEN TRUE ELSE FALSE END AS QSR_Flag,
CASE WHEN qsr_flag = false and sales_type != 'Renewal' then TRUE else FALSE END AS non_qsr_non_renewal_oppty_flag,
CASE WHEN o.sales_type = 'Renewal' THEN TRUE ELSE FALSE END AS Renewal_Flag,
b.PO_required,  
o.auto_renewal_status,
CASE WHEN (CONTAINS(opportunity_name, '#ultimateupgrade') or CONTAINS(opportunity_name, 'Ultimate Upgrade') or CONTAINS(opportunity_name, 'Upgrade to Ultimate') or CONTAINS(opportunity_name, 'ultimate upgrade')) THEN TRUE ELSE FALSE END AS Ultimate_Upgrade_Oppty_Flag
FROM PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT a
LEFT JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o
    ON a.dim_crm_account_id = o.dim_crm_account_id
    AND o.is_closed = False
    AND DATEDIFF('day', current_date, o.close_date) <= 365
LEFT JOIN PROD.COMMON.DIM_SUBSCRIPTION s
    ON o.dim_crm_opportunity_id = s.dim_crm_opportunity_id_current_open_renewal
    and subscription_status = 'Active'
LEFT JOIN (SELECT distinct dim_crm_account_id, PO_required FROM PROD.common.dim_billing_account WHERE PO_REQUIRED = 'YES') b 
    ON b.dim_crm_account_id = a.dim_crm_account_id
-- LEFT JOIN PROD.common.dim_billing_account b
--     ON a.dim_crm_account_id = b.dim_crm_account_id
LEFT JOIN (SELECT *, max(score_date) over(partition by CRM_ACCOUNT_ID) as latest_score_date FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTC_SCORES"  QUALIFY score_date = latest_score_date) ptc 
    ON ptc.crm_account_id = a.dim_crm_account_id
LEFT JOIN (SELECT *, max(score_date) over(partition by CRM_ACCOUNT_ID) as latest_score_date FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTE_SCORES"  QUALIFY score_date = latest_score_date) pte 
    ON pte.crm_account_id = a.dim_crm_account_id
LEFT JOIN last_case_data cd 
    ON cd.account_id = a.dim_crm_account_id
LEFT JOIN last_renewal_case_data lr 
    ON lr.account_id = a.dim_crm_account_id
LEFT JOIN open_cases oc
    ON oc.account_id = a.dim_crm_account_id
LEFT JOIN open_renewal_opps oo
    ON oo.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN low_adoption_last_180 
    ON low_adoption_last_180.account_id = a.dim_crm_account_id
LEFT JOIN high_adoption_last_180
    ON high_adoption_last_180.account_id = a.dim_crm_account_id
LEFT JOIN qsr_failed_last_75 qsr
    ON qsr.account_id = a.dim_crm_account_id
    AND qsr.opportunity_id = o.dim_crm_opportunity_id
LEFT JOIN latest_ptc_score lptc
LEFT JOIN latest_pte_score lpte
WHERE user_role_type = 'POOL' 
AND crm_account_owner_geo in ('AMER', 'EMEA') 
AND a.PARENT_CRM_ACCOUNT_SALES_SEGMENT = 'SMB'),




utilization as (
SELECT 
mart_arr.ARR_MONTH
,monthly_mart.snapshot_month 
--,ping_created_at 
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
--,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
,monthly_mart.max_BILLABLE_USER_COUNT
,monthly_mart.LICENSE_USER_COUNT
,monthly_mart.subscription_start_date
,monthly_mart.subscription_end_date
,monthly_mart.term_end_date
,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)*(monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as overage_amount
,max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT 
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
LEFT JOIN 
  (select
  DISTINCT
    snapshot_month,
    --ping_created_at,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    INSTANCE_TYPE,
    subscription_status,
    subscription_start_date,
    subscription_end_date,
    term_end_date, 
    LICENSE_USER_COUNT,
    MAX(BILLABLE_USER_COUNT) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as max_billable_user_count
    FROM COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
    --from PROD.pumps.pump_gainsight_metrics_monthly_paid 
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'  
    and snapshot_month = DATE_TRUNC('month',CURRENT_DATE)
    --and dim_subscription_id_original = '8a1280098799981d0187a2a977d65030'
  ) monthly_mart
      ON mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL = monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL
WHERE ARR_MONTH = snapshot_month
and PRODUCT_TIER_NAME not like '%Storage%'
  --and monthly_mart.billable_user_count >= 1
  --and overage_count < 0
), 

---------------------------------------------------
autorenew_switch as (
select
dim_crm_account_id, 
dim_subscription_id,
active_autorenew_status,
prior_auto_renewal,
MAX(snapshot_date) as latest_switch_date
from
(
  select 
date_actual as snapshot_date,
PREP_BILLING_ACCOUNT_USER.user_name as update_user,
PREP_BILLING_ACCOUNT_USER.IS_INTEGRATION_USER,
TURN_ON_AUTO_RENEWAL as active_autorenew_status,
lag(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as prior_auto_renewal,
lead(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as future_auto_renewal,
sub.*
from common.dim_subscription_snapshot_bottom_up sub
inner join common.dim_date on sub.snapshot_id = dim_date.date_id
inner join restricted_safe_common_prep.PREP_BILLING_ACCOUNT_USER on sub.UPDATED_BY_ID = PREP_BILLING_ACCOUNT_USER.ZUORA_USER_ID
where snapshot_date >= '2023-02-01'
  and snapshot_date < CURRENT_DATE
  and subscription_status = 'Active'
 -- and update_user = 'svc_zuora_fulfillment_int@gitlab.com'
--and (TURN_ON_AUTO_RENEWAL = 'Yes' or TURN_ON_AUTO_RENEWAL is null)
order by snapshot_date asc
)
where
((active_autorenew_status = 'No' and update_user = 'svc_zuora_fulfillment_int@gitlab.com') and prior_auto_renewal = 'Yes')
GROUP BY 1,2,3,4
 ),




all_data as(
SELECT a.*, 
u.overage_count,
u.overage_amount,
u.latest_overage_month,
u.qsr_enabled_flag,
s.latest_switch_date, 
DATEDIFF('day', latest_switch_date, current_date) as days_since_autorenewal_switch 
FROM account_base a
LEFT JOIN utilization u
    ON u.dim_crm_account_id = a.account_id
    --and term_end_date = current_subscription_end_date
    and u.dim_subscription_id_original = a.sub_subscription_id_original
LEFT JOIN autorenew_switch s
    ON s.dim_crm_account_id = a.account_id
    and s.dim_subscription_id = sub_subscription_id),

 


----updating to have no timeframe filters in case statments; can add these in next CTE
case_flags as (
SELECT *, 
CASE WHEN sales_type = 'Renewal' AND PO_REQUIRED = 'YES' then TRUE ELSE FALSE END AS PO_REQUIRED_Flag, 
CASE WHEN opportunity_term > 12 OR (CONTAINS(opportunity_name, '2 year') or CONTAINS(opportunity_name, '3 year')) then TRUE else FALSE end as multiyear_renewal_flag,
CASE WHEN (auto_renewal_status like '%EoA%' or auto_renewal_status like 'EoA%') THEN TRUE ELSE FALSE end as EOA_Auto_Renewal_Will_Fail_Flag,
CASE WHEN ((auto_renewal_status != NULL or auto_renewal_status not in ('On', 'Off')) AND EOA_Auto_Renewal_Will_Fail_Flag = False) THEN TRUE ELSE FALSE END AS Auto_Renewal_Will_Fail_Flag, 
CASE WHEN QSR_status = 'Failed' and is_closed = False then TRUE ELSE FALSE END AS QSR_Failed_Flag, 
CASE WHEN sales_type = 'Renewal' and days_since_current_sub_end_date > 0 and is_closed = False then TRUE ELSE FALSE END as Overdue_Renewal_Flag,
--CASE WHEN days_since_autorenewal_switch = 2 then TRUE ELSE FALSE END AS Auto_Renew_Recently_Turned_Off_Flag, ----- commenting out to remove timeframe filter (add this in another query
CASE WHEN latest_switch_date IS NOT NULL THEN TRUE ELSE FALSE END AS Auto_Renew_Recently_Turned_Off_Flag,
-- CASE WHEN overage_count < 0 and overage_amount < 0 and latest_overage_month = DATE_TRUNC('month', current_date) and DATEDIFF('day', current_date, term_end_date) > 120 then TRUE ELSE FALSE END AS Underutiliation_Flag, 
CASE WHEN overage_count < 0 and overage_amount < 0 and latest_overage_month = DATE_TRUNC('month', current_date) then TRUE ELSE FALSE END AS Underutilization_Flag, 
--CASE WHEN ptc_score > .5 = false and ptc_last_90 = False and days_since_last_ptc_score = 2 then TRUE ELSE FALSE END AS High_PTC_Score_Flag, ----placeholder, what timeframe after scoring do we want to create cases
CASE WHEN ptc_score > .5 and ptc_latest_score_date = most_recent_ptc and sales_type = 'Renewal' then TRUE ELSE FALSE END AS High_PTC_Score_Flag,
--CASE WHEN pte_score > .5 = false and pte_last_90 = False and days_since_last_pte_score = 2 then TRUE ELSE FALSE END AS High_PTE_Score_Flag, ----placeholder, what timeframe after scoring do we want to create cases
CASE WHEN pte_score > .5 and pte_latest_score_date = most_recent_pte and sales_type = 'Renewal' then TRUE ELSE FALSE END AS High_PTE_Score_Flag,
CASE WHEN overage_count > 0 and overage_amount > 0 and latest_overage_month = DATE_TRUNC('month', current_date) and qsr_enabled_flag = false then TRUE ELSE FALSE END AS Overage_QSR_Off_Flag,
--CASE WHEN six_sense_account_buying_stage in ('Decision', 'Purchase') and six_sense_account_profile_fit = 'Strong' and SIX_SENSE_ACCOUNT_INTENT_SCORE > 74 and DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, current_date) <= 7 then TRUE ELSE FALSE END AS SixSense_Growth_Signal_Flag
CASE WHEN six_sense_account_buying_stage in ('Decision', 'Purchase') and six_sense_account_profile_fit = 'Strong' and SIX_SENSE_ACCOUNT_INTENT_SCORE > 74 then TRUE ELSE FALSE END AS SixSense_Growth_Signal_Flag
FROM all_data
),




cases as (
SELECT *,
CASE WHEN PO_REQUIRED_Flag = True and days_till_close = 30 then 'PO Required'
     WHEN multiyear_renewal_flag = True and days_till_close = 30 then 'Multiyear Renewal'
     WHEN EOA_Auto_Renewal_Will_Fail_Flag = True and days_till_close = 30 then 'EOA Renewal'
     WHEN Auto_Renewal_Will_Fail_Flag = True and days_till_close = 30 then 'Auto-Renewal Will Fail'
     WHEN Overdue_Renewal_Flag = True and days_since_current_sub_end_date = 2 then 'Overdue Renewal'
     WHEN Auto_Renew_Recently_Turned_Off_Flag = TRUE and days_since_autorenewal_switch = 2 then 'Auto-Renew Recently Turned Off'
     WHEN QSR_Failed_Flag = True and qsr_failed_last_75 != True then 'Failed QSR' ELSE NULL END AS Renewal_Trigger_Name, ----90 days is a placeholder (waiting for MIke to confirm timeframe)
CASE WHEN (Underutilization_Flag = True and (days_till_close < 270 and days_till_close > 180)) and LA_last_180 = False then 'Underutilization' 
     WHEN (High_PTC_Score_Flag = True and (days_till_close < 180 and days_till_close > 30)) and LA_last_180 = False then 'High PTC Score' ELSE NULL END AS Low_Adoption_Case_Trigger_Name, 
CASE WHEN Overage_QSR_Off_Flag = True and (days_till_close < 270 and days_till_close > 30) and HA_last_180 = False then 'Overage and QSR Off'
     WHEN High_PTE_Score_Flag = True and (days_till_close < 270 and days_till_close > 30) and HA_last_180 = False and low_adoption_case_trigger_Name IS NULL then 'High PTE Score'
     WHEN SixSense_Growth_Signal_Flag = True and days_since_6sense_account_update = 2 and (days_till_close < 270 and days_till_close > 30) and low_adoption_case_trigger_name IS NULL and HA_last_180 = False then '6sense Growth Signal' ELSE NULL END AS High_Adoption_Case_Trigger_Name
FROM case_flags
),

--------------prioritizes renewals first and then low adoption over high adoption
final as (
SELECT * 
FROM (
SELECT *,
ROW_NUMBER() OVER(PARTITION BY CASE_TRIGGER order by overage_amount) as underutilization_top, 
ROW_NUMBER() OVER(PARTITION BY CASE_TRIGGER ORDER BY OVERAGE_AMOUNT desc) AS overage_top,
ROW_NUMBER() OVER(PARTITION BY CASE_TRIGGER ORDER BY CARR_ACCOUNT_FAMILY desc) as PTC_top,
ROW_NUMBER() OVER(PARTITION BY CASE_TRIGGER ORDER BY CARR_ACCOUNT_FAMILY desc) as PTE_top
FROM(
SELECT *, 
CASE WHEN renewal_trigger_name IS NOT NULL then renewal_trigger_name
     WHEN (renewal_trigger_name IS NULL and low_adoption_case_trigger_name IS NOT NULL) then low_adoption_case_trigger_name
     WHEN (renewal_trigger_name IS NULL and low_adoption_case_trigger_name IS NULL and high_adoption_case_trigger_name IS NOT NULL) then high_adoption_case_trigger_name
     ELSE NULL end as case_trigger, 
CASE WHEN renewal_trigger_name IS NOT NULL then True else False end as renewal_case
FROM cases
WHERE case_trigger is not null))
WHERE (case_trigger not in ('High PTE Score', 'High PTC Score', 'Underutilization', 'Overage and QSR Off') OR (case_trigger = 'High PTE Score' and PTE_Top = 1) OR (case_trigger = 'High PTC Score' and PTC_Top = 1) OR (case_trigger = 'Underutilization' and underutilization_top = 1) OR (case_trigger = 'Overage and QSR Off' and overage_top <= 10))
)


-- SELECT 
-- *
-- FROM FINAL 

SELECT 
f.case_trigger as case_trigger,
f.account_id,
cd.case_trigger_id,  
cd.status, 
cd.case_origin, 
cd.type, 
CASE WHEN renewal_case = true then CONCAT(cd.case_subject, ' ', f.close_date) ELSE cd.case_subject end as case_subject, 
CASE WHEN Ultimate_Upgrade_Oppty_Flag = True then account_owner 
     WHEN current_open_cases > 0 then last_case_owner_id 
     ELSE cd.owner_id end as owner_id,
cd.case_reason,
cd.record_type_id,
cd.priority,
cd.case_cta, 
CASE WHEN cd.case_trigger_id = 9 then CONCAT(cd.CASE_CONTEXT, ' ', ptc_insights)
     WHEN cd.case_trigger_id = 11 then CONCAT(cd.CASE_CONTEXT, ' ', pte_insights)
     WHEN cd.case_trigger_id = 6 then CONCAT(cd.CASE_CONTEXT, ' ', latest_switch_date)
     WHEN cd.case_trigger_id = 5 then CONCAT(cd.case_context, ' ', current_subscription_end_date)
     WHEN cd.case_trigger_id = 7 then CONCAT(cd.CASE_CONTEXT, ' ', opportunity_id)
     WHEN cd.case_trigger_id = 4 then CONCAT(cd.CASE_CONTEXT, ' ', auto_renewal_status)
     WHEN cd.case_trigger_id = 12 then CONCAT(cd.CASE_CONTEXT, ' ', SIX_SENSE_ACCOUNT_INTENT_SCORE) 
     ELSE cd.CASE_CONTEXT END AS context 
FROM FINAL f
LEFT JOIN boneyard.case_data cd
    ON f.case_trigger = cd.case_trigger



