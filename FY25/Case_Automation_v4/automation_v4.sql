----exclusions for case creation: any account with a renewal case within the last 90 days (opened, closed, resolved, etc - any status), any account that has had a *non-storage* puchase in the last 60 days OR a closed lost renewal within the last 90 days

with exclude as (
SELECT 
account_id
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE record_type_id in ('0128X000001pPRkQAM')
and contains(subject, 'Renewal') ----currently a placeholder until we can determine what we need to be inclusive of all renewal cases for last 90 days
AND datediff('day', created_date, current_date) <= 90

UNION 
 
SELECT 
  dim_crm_account_id as account_id
FROM (SELECT 
a.dim_crm_account_id,
case
    when product_category like '%Premium%'  or PRODUCT_DETAILS like '%Premium%' then 'Premium'
    when product_category like '%Ultimate%'  or PRODUCT_DETAILS like '%Ultimate%' then 'Ultimate'
    when product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' then 'Bronze'
    when product_category like '%Starter%'  or PRODUCT_DETAILS like '%Starter%' then 'Starter'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
    when product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' then 'Silver'
    when product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'Gold'
    when product_category like 'CI%' or PRODUCT_DETAILS like 'CI%' or PRODUCT_DETAILS like '%Compute%' then 'CI'
else product_category end as product_tier
  FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" a
  JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o
  ON a.dim_crm_account_id = o.dim_crm_account_id
WHERE  product_tier != 'Storage'
AND ((o.is_closed_won = True AND datediff('day', o.close_date, current_date) <= 60) OR (o.stage_name = '8-Closed Lost' and sales_type = 'Renewal' and datediff('day', o.close_date, current_date) <=90))
AND a.account_owner = 'Pooled Sales User [ DO NOT CHATTER ]')
),

PTE_Last_90 as 
(
SELECT distinct
account_id, 
CASE WHEN subject = 'High PTE Score' then True else False END AS PTE_Last_90
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE  DATEDIFF('day', created_date, current_date) <= 90
and record_type_id in ('0128X000001pPRkQAM') and contains(subject, 'PTE') ---placeholder to get all PTE in last 90 days
),

PTC_Last_90 as
(SELECT distinct
account_id, 
CASE WHEN subject = 'High PTC Score' then True else False END AS PTC_Last_90
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE  DATEDIFF('day', created_date, current_date) <= 90
and record_type_id in ('0128X000001pPRkQAM') and contains(subject, 'PTC')---placeholder to get all PTC in last 90 days 
),


--------------this table pulls in all of the data for the accounts, opportunities, subscription, and billing account (PO required field only) to act as the base table for the query 
account_base  as(
SELECT 
a.dim_crm_account_id as account_id, 
a.account_owner,
a.USER_ROLE_TYPE, 
a.CRM_ACCOUNT_OWNER_GEO,
a.PARENT_CRM_ACCOUNT_SALES_SEGMENT, 
a.PARENT_CRM_ACCOUNT_BUSINESS_UNIT, 
a.NEXT_RENEWAL_DATE, 
a.carr_this_account as cARR_this_account,
a.six_sense_account_buying_stage, 
a.SIX_SENSE_ACCOUNT_PROFILE_FIT, 
a.SIX_SENSE_ACCOUNT_INTENT_SCORE,
a.SIX_SENSE_ACCOUNT_UPDATE_DATE,
o.dim_crm_opportunity_id as opportunity_id,
o.is_closed, 
o.sales_type, 
o.close_date, 
o.opportunity_term, 
o.opportunity_name,
o.QSR_status,
s.dim_subscription_id as sub_subscription_id, 
s.dim_subscription_id_original as sub_subscription_id_original,
s.subscription_status, 
s.term_end_date as current_subscription_end_date,
ptc.score_date as ptc_score_date, 
ptc.score as ptc_score, 
ptc.score_group as ptc_score_group,
ptc.latest_score_date as ptc_latest_score_date,
pte.score_date as pte_score_date, 
pte.score as pte_score, 
pte.score_group as pte_score_group,
pte.latest_score_date as pte_latest_score_date, 
CASE WHEN PTE_last_90 = True then TRUE else FALSE end as PTE_LAST_90, 
CASE WHEN PTC_last_90 = TRUE then TRUE else FALSE end as PTC_last_90,
DATEDIFF('day', s.term_end_date, current_date) as days_since_current_sub_end_date,
s.dim_subscription_id,
DATEDIFF('day', current_date, o.close_date) as days_till_close,
CASE WHEN o.opportunity_name LIKE '%QSR%' THEN TRUE ELSE FALSE END AS QSR_Flag,
CASE WHEN qsr_flag = false and sales_type != 'Renewal' then TRUE else FALSE END AS non_qsr_non_renewal_oppty_flag,
CASE WHEN o.sales_type = 'Renewal' THEN TRUE ELSE FALSE END AS Renewal_Flag,
b.PO_required,  
o.auto_renewal_status,
CASE WHEN (CONTAINS(opportunity_name, '#ultimateupgrade') or CONTAINS(opportunity_name, 'Ultimate Upgrade') or CONTAINS(opportunity_name, 'Upgrade to Ultimate') or CONTAINS(opportunity_name, 'ultimate upgrade')) THEN TRUE ELSE FALSE END AS Ulitimate_Upgrade_Oppty_Flag, 
CASE WHEN sales_type = 'Renewal' AND PO_REQUIRED = 'YES' then TRUE ELSE FALSE END AS PO_REQUIRED_Flag, 
CASE WHEN o.opportunity_term > 12 and (CONTAINS(o.opportunity_name, '2 year') or CONTAINS(o.opportunity_name, '3 year')) then TRUE else FALSE end as multiyear_renewal_flag,
CASE WHEN (auto_renewal_status like '%EoA%' or auto_renewal_status like 'EoA%') THEN TRUE ELSE FALSE end as EOA_Auto_Renewal_Will_Fail_Flag,
CASE WHEN ((auto_renewal_status != NULL or auto_renewal_status not in ('On', 'Off')) AND EOA_Auto_Renewal_Will_Fail_Flag = False) THEN TRUE ELSE FALSE END AS Auto_Renewal_Will_Fail_Flag, 
CASE WHEN QSR_status = 'Failed' and is_closed = False then TRUE ELSE FALSE END AS QSR_Failed_Flag, 
CASE WHEN sales_type = 'Renewal' and days_since_current_sub_end_date = 2 then TRUE ELSE FALSE END as Overdue_Renewal_Flag,
FROM PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT a
LEFT JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o
    ON a.dim_crm_account_id = o.dim_crm_account_id
    AND o.is_closed = False
    AND DATEDIFF('day', current_date, o.close_date) <= 365
LEFT JOIN PROD.COMMON.DIM_SUBSCRIPTION s
    ON o.dim_crm_opportunity_id = s.dim_crm_opportunity_id_current_open_renewal
    and subscription_status = 'Active'
LEFT JOIN (SELECT distinct dim_crm_account_id, PO_required FROM PROD.common.dim_billing_account WHERE PO_REQUIRED = 'YES') b 
    ON b.dim_crm_account_id = a.dim_crm_account_id
-- LEFT JOIN PROD.common.dim_billing_account b
--     ON a.dim_crm_account_id = b.dim_crm_account_id
LEFT JOIN (SELECT *, max(score_date) over(partition by CRM_ACCOUNT_ID) as latest_score_date FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTC_SCORES"  QUALIFY last_day(score_date) = last_day(latest_score_date) AND last_day(latest_score_date) = last_day(CURRENT_DATE)) ptc 
    ON ptc.crm_account_id = a.dim_crm_account_id
LEFT JOIN (SELECT *, max(score_date) over(partition by CRM_ACCOUNT_ID) as latest_score_date FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTE_SCORES"  QUALIFY last_day(score_date) = last_day(latest_score_date) AND last_day(latest_score_date) = last_day(current_date)) pte 
    ON pte.crm_account_id = a.dim_crm_account_id
LEFT JOIN PTC_last_90 
    ON PTC_last_90.account_id = a.dim_crm_account_id
LEFT JOIN PTE_last_90
    ON PTE_last_90.account_id = a.dim_crm_account_id
WHERE user_role_type = 'POOL' 
AND crm_account_owner_geo in ('AMER', 'EMEA') 
AND a.PARENT_CRM_ACCOUNT_SALES_SEGMENT = 'SMB'),




utilization as (
SELECT 
mart_arr.ARR_MONTH
,monthly_mart.snapshot_month 
--,ping_created_at 
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
--,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
,monthly_mart.max_BILLABLE_USER_COUNT
,monthly_mart.LICENSE_USER_COUNT
,monthly_mart.subscription_start_date
,monthly_mart.subscription_end_date
,monthly_mart.term_end_date
,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)*(monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as overage_amount
,max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT 
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
LEFT JOIN 
  (select
  DISTINCT
    snapshot_month,
    --ping_created_at,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    INSTANCE_TYPE,
    subscription_status,
    subscription_start_date,
    subscription_end_date,
    term_end_date, 
    LICENSE_USER_COUNT,
    MAX(BILLABLE_USER_COUNT) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as max_billable_user_count
    FROM COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
    --from PROD.pumps.pump_gainsight_metrics_monthly_paid 
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'  
    and snapshot_month = DATE_TRUNC('month',CURRENT_DATE)
    --and dim_subscription_id_original = '8a1280098799981d0187a2a977d65030'
  ) monthly_mart
      ON mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL = monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL
WHERE ARR_MONTH = snapshot_month
and PRODUCT_TIER_NAME not like '%Storage%'
  --and monthly_mart.billable_user_count >= 1
  --and overage_count < 0
), 

---------------------------------------------------
autorenew_switch as (
select
dim_crm_account_id, 
dim_subscription_id,
active_autorenew_status,
prior_auto_renewal,
MAX(snapshot_date) as latest_switch_date
from
(
  select 
date_actual as snapshot_date,
PREP_BILLING_ACCOUNT_USER.user_name as update_user,
PREP_BILLING_ACCOUNT_USER.IS_INTEGRATION_USER,
TURN_ON_AUTO_RENEWAL as active_autorenew_status,
lag(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as prior_auto_renewal,
lead(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as future_auto_renewal,
sub.*
from common.dim_subscription_snapshot_bottom_up sub
inner join common.dim_date on sub.snapshot_id = dim_date.date_id
inner join restricted_safe_common_prep.PREP_BILLING_ACCOUNT_USER on sub.UPDATED_BY_ID = PREP_BILLING_ACCOUNT_USER.ZUORA_USER_ID
where snapshot_date >= '2023-02-01'
  and snapshot_date < CURRENT_DATE
  and subscription_status = 'Active'
 -- and update_user = 'svc_zuora_fulfillment_int@gitlab.com'
--and (TURN_ON_AUTO_RENEWAL = 'Yes' or TURN_ON_AUTO_RENEWAL is null)
order by snapshot_date asc
)
where
((active_autorenew_status = 'No' and update_user = 'svc_zuora_fulfillment_int@gitlab.com') and prior_auto_renewal = 'Yes')
GROUP BY 1,2,3,4
 ), 


all_data as(
SELECT *, 
DATEDIFF('day', latest_switch_date, current_date) as days_since_autorenewal_switch
FROM account_base a
LEFT JOIN utilization u
    ON u.dim_crm_account_id = a.account_id
    --and term_end_date = current_subscription_end_date
    and u.dim_subscription_id_original = a.sub_subscription_id_original
LEFT JOIN autorenew_switch s
    ON s.dim_crm_account_id = a.account_id
    and s.dim_subscription_id = sub_subscription_id),



-----current renewal timeline is 30 days out. Autorenewal turned off and overdue renewal will have a 2 day lag. This can be adjusted or eventually changable via csv 
renewals as (
SELECT 
*,
CASE WHEN sales_type = 'Renewal' and PO_required_flag = True and days_till_close = 30 then 'PO Renewal'
     WHEN sales_type = 'Renewal' and multiyear_renewal_flag = True and days_till_close = 30 then 'Multiyear Renewal'
     WHEN sales_type = 'Renewal' and Auto_Renewal_Will_Fail_Flag = 'EoA - Autorenewal will Fail' and days_till_close = 30 then 'EOA Renewal'
     WHEN sales_type = 'Renewal' and Auto_Renewal_Will_Fail_Flag = 'Autorenewal Will Fail' and days_till_close = 30 then 'Auto-Renewal Will Fail'
     WHEN sales_type = 'Renewal' and days_since_current_sub_end_date = 2 then 'Overdue Renewal'
     WHEN days_since_autorenewal_switch = 2 then 'Auto-Renew Recently Turned Off'
ELSE NULL END as case_trigger
FROM 
all_data
),


----ptc and pte score need to be determine if we are doing one monthly pull and if so write logic to target when the scoes are updated
churns as (
SELECT *
FROM(
SELECT 
*, 
ROW_NUMBER() OVER(PARTITION BY CASE_TRIGGER order by overage_amount) as underutilization_top, 
ROW_NUMBER() OVER(PARTITION BY CASE_TRIGGER ORDER BY carr_this_account desc) as PTC_top
FROM(
SELECT * ,
CASE WHEN QSR_status = 'Failed' and is_closed = False then 'Failed QSR'
     WHEN overage_count < 0 and overage_amount < 0 and latest_overage_month = DATE_TRUNC('month', current_date) and DATEDIFF('day', current_date, term_end_date) > 120 then 'Underutilization'
     WHEN ptc_score > .5 = false and ptc_last_90 = False then 'High PTC Score'
ELSE NULL END as case_trigger
FROM 
all_data
))
WHERE ((case_trigger = 'Underutilization' and underutilization_top <= 30) OR (case_trigger = 'High PTC Score' and PTC_top <= 30) OR case_trigger = 'Failed QSR')
), 

expansions as (
SELECT * 
FROM 
(
SELECT  
*, 
ROW_NUMBER() OVER(PARTITION BY CASE_TRIGGER ORDER BY carr_this_account) as PTE_top
FROM(
SELECT *, 
CASE WHEN overage_count > 0 and overage_amount > 0 and latest_overage_month = DATE_TRUNC('month', current_date) and qsr_enabled_flag = false then 'Overage with QSR Turned Off'
    WHEN pte_score > .5 and pte_last_90 = false and ptc_last_90 = false then 'High PTE Score'
    WHEN six_sense_account_buying_stage in ('Decision', 'Purchase') and six_sense_account_profile_fit = 'Strong' and SIX_SENSE_ACCOUNT_INTENT_SCORE > 74 and DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, current_date) <= 7 then '6Sense Growth Signal'
ELSE NULL END as case_trigger
FROM all_data
))
WHERE ((case_trigger = 'High PTE Score' and PTE_top <= 30) OR case_trigger in  ('Overage with QSR Turned Off', '6Sense Growth Signal'))
), 

final as (
SELECT 
account_id,
opportunity_id,
close_date,
case_trigger, 
Ulitimate_Upgrade_Oppty_Flag, 
overage_count, 
overage_amount,
latest_overage_month, 
max_BILLABLE_USER_COUNT,
LICENSE_USER_COUNT
from renewals

UNION ALL 

SELECT 
account_id, 
opportunity_id,
close_date,
case_trigger,
Ulitimate_Upgrade_Oppty_Flag, 
overage_count, 
overage_amount,
latest_overage_month, 
max_BILLABLE_USER_COUNT,
LICENSE_USER_COUNT
from churns

UNION ALL

SELECT 
account_id,
opportunity_id,
close_date,
case_trigger, 
Ulitimate_Upgrade_Oppty_Flag, 
overage_count, 
overage_amount,
latest_overage_month,
max_BILLABLE_USER_COUNT,
LICENSE_USER_COUNT
from expansions) 


SELECT f.*, 
CASE WHEN e.account_id IS NULL then FALSE else TRUE end as exclude_flag
FROM final f
LEFT JOIN exclude e
    ON e.account_id = f.account_id 
where case_trigger IS NOT NULL

