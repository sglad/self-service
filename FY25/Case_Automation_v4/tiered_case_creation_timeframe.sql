SET LAST_UPLOAD_DATE = CAST(current_date - 5 as date);

------getting last case creation date for time frame query
-- with last_created_date as 
-- (SELECT 
-- MAX(CAST(created_date as date)) as last_upload_date
-- FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
-- WHERE record_type_id in ('0128X000001pPRkQAM')
-- AND case_type in ('Expansion Opportunity', 'Urgent Renewal & TRX Support', 'Churn & Contraction Mitigation') ---any additional case types that need to be added her? now or in the future? 
-- ), 

------90 days since last case Closed - Resolved OR 90 days since last case created if Closed - Unresponsive
with high_value_case_last_90 as (
SELECT distinct
account_id,
CASE WHEN lower(subject) = 'high value account check in' OR case_type = 'Inbound Request' OR (lower(subject) = 'high value account' AND status in ('Open', 'In Progress') AND DATEDIFF('day', created_date, current_date) <= 90) then True else False END AS High_Value_Last_90 ------- placeplacer until confirmation of case name 
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE ((DATEDIFF('day', created_date, current_date) <= 90 AND status = 'Closed - Unresponsive') 
OR (DATEDIFF ('day', closed_date, current_date) <= 90 AND status = 'Closed - Resolved')
OR status in ('Open', 'In Progress'))
and record_type_id in ('0128X000001pPRkQAM')
),


last_high_value_case as (
SELECT *
FROM(
SELECT
    account_id,
    --opportunity_id,
    case_id,
    owner_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.crm_user_role_type,
    sfdc_case.created_date,
    MAX(sfdc_case.created_date) over(partition by ACCOUNT_ID) as last_high_value_date
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    and lower(subject) like '%high value account%') -----subject placeholder - this could change
    WHERE created_date = last_high_value_date
), 

high_adoption_Last_180 as 
(
SELECT distinct
account_id, 
CASE WHEN (contains(lower(subject), 'pte') OR contains(lower(subject), 'high adoption') OR contains(lower(subject), 'overage') OR contains(lower(subject), '6sense')) then True else False END AS HA_Last_180
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE  DATEDIFF('day', created_date, current_date) <= 180
and record_type_id in ('0128X000001pPRkQAM')
),

low_adoption_Last_180 as
(SELECT distinct
account_id, 
CASE WHEN (contains(lower(subject), 'ptc') OR contains(lower(subject), 'low adoption') OR contains(lower(subject), 'underutilization')) then True else False END AS LA_Last_180
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE  DATEDIFF('day', created_date, current_date) <= 180
and record_type_id in ('0128X000001pPRkQAM') 
),

qsr_failed_last_75 as 
(
SELECT distinct
account_id,
RIGHT(description, 18) as opportunity_id, -----placeholder to get this until opportunity ID is it's own field
CASE WHEN contains(lower(subject), 'failed qsr') then True else False end as QSR_Failed_last_75
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE  DATEDIFF('day', created_date, current_date) <= 75
and record_type_id in ('0128X000001pPRkQAM') 
and contains(lower(subject), 'failed qsr')
),


last_case_data as (
SELECT *
FROM (
SELECT *,
ROW_NUMBER() OVER (PARTITION BY account_ID ORDER BY created_date desc) as rn
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE record_type_id in ('0128X000001pPRkQAM')
and status in ('Open', 'In Progress')
)
WHERE rn = 1
),

last_renewal_case_data as (
SELECT *
FROM (
SELECT *,
ROW_NUMBER() OVER (PARTITION BY account_ID ORDER BY created_date desc) as rn
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE record_type_id in ('0128X000001pPRkQAM')
and contains(subject, 'Renewal') ----currently a placeholder until we can determine what we need to be inclusive of all renewal cases
)
WHERE rn = 1
),

open_cases as (
SELECT
account_id, 
COUNT(distinct case_id) as count_cases
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE"
WHERE record_type_id in ('0128X000001pPRkQAM') 
AND status = 'Open'
GROUP BY 1
),

open_renewal_opps as (
SELECT
dim_crm_account_id, 
count(distinct dim_crm_opportunity_id) as count_open_opps
FROM PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
WHERE sales_type = 'Renewal'
AND IS_Closed = False
GROUP BY 1
),

latest_pte_score as (
SELECT 
MAX(score_date) as most_recent_pte
FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTE_SCORES"
), 

latest_ptc_score as (
SELECT 
MAX(score_date) as most_recent_ptc
FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTC_SCORES"
),


--------------using the account base CTE to get tiering and beginning of FY25 info joined to the account id
account_base as (
SELECT acct.*, 
CASE WHEN first_high_value_case.case_id IS NOT NULL then 'Tier 1'
     WHEN first_high_value_case.case_id IS NULL THEN
        CASE WHEN acct.carr_this_account >7000 then 'Tier 1' 
            WHEN acct.carr_this_account < 3000 AND acct.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
            ELSE 'Tier 2' end
    ELSE null END AS calculated_tier, 
CASE WHEN acct.parent_crm_account_geo in ('AMER', 'APAC') then 'AMER/APAC' 
     WHEN acct.parent_crm_account_geo in ('EMEA') then 'EMEA' 
     ELSE 'Other' end as Team,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
fo.SQS as fo_sqs,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr, 
case when fo_fiscal_year <= 2024 then False else True end as New_FY25_FO_Flag, 
first_high_value_case.created_date as first_high_value_case_created_date, 
high_value_case.case_owner_name as high_value_case_owner,
high_value_case.team as high_value_case_owner_team,
high_value_case.manager_name as high_value_case_owner_manager, 
start_values.carr_account_family as starting_carr_account_family, 
start_values.carr_this_account as starting_carr_this_account, 
CASE WHEN start_values.carr_this_account >7000 then 'Tier 1' 
     WHEN start_values.carr_this_account < 3000 AND start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT < 10 then 'Tier 3'
     else 'Tier 2' end as starting_calculated_tier,
start_values.pte_score as starting_pte_score, 
start_values.ptc_score as starting_ptc_score, 
start_values.PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as starting_parent_crm_account_lam_dev_count,
CASE WHEN EOA.dim_crm_account_id IS NOT NULL then True else False end as EOA_Flag, 
CASE WHEN free_promo.dim_crm_account_id IS NOT NULL then True else False end as Free_Promo_Flag, 
CASE WHEN price_increase.dim_crm_account_id IS NOT NULL then True else False end as Price_Increase_Promo_Flag,
CASE WHEN ultimate.dim_parent_crm_account_id is not null then true else false end as ultimate_customer_flag
FROM PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT acct
------subquery that gets latest FO data
LEFT JOIN 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year, 
    last_value(sales_qualified_source_name) over (partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as SQS
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
        ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo
    ON fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
---------subquery that gets latest churn data 
LEFT JOIN 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
        ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
     and not(product_category like '%torage%' or PRODUCT_DETAILS like '%torage%')  
    ) churn 
    ON churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
--------subquery to get high tier case owner
LEFT JOIN 
    (SELECT
    account_id,
    owner_id, 
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.crm_user_role_type
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    and subject = 'FY25 High Value Account' -----subject placeholder - this could change
    ) high_value_case
    ON high_value_case.account_id = acct.dim_crm_account_id
--------------subquery to get start of FY25 values
LEFT JOIN 
    (SELECT
    *
    FROM PROD.RESTRICTED_SAFE_COMMON.DIM_CRM_ACCOUNT_DAILY_SNAPSHOT
    WHERE snapshot_date = '2024-02-05' -----placeholder date for start of year
    ) start_values
    ON start_values.dim_crm_account_id = acct.dim_crm_account_id
-----subquery to get FIRST high value case 
LEFT JOIN 
    (SELECT 
    *
    FROM (SELECT
    account_id,
    case_id,
    owner_id, 
    subject,
    dim_crm_user.user_name as case_owner_name,
    dim_crm_user.department as case_department,
    dim_crm_user.team,
    dim_crm_user.manager_name,
    dim_crm_user.user_role_name as case_user_role_name,
    dim_crm_user.crm_user_role_type,
    sfdc_case.created_date,
    MIN(sfdc_case.created_date) over(partition by ACCOUNT_ID) as first_high_value_date
    FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
    LEFT JOIN common.dim_crm_user 
        ON sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
    WHERE record_type_id in ('0128X000001pPRkQAM') 
    and lower(subject) like '%high value account%') -----subject placeholder - this could change
    WHERE created_date = first_high_value_date 
    ) first_high_value_case
    ON first_high_value_case.account_id = acct.dim_crm_account_id

-----EOA cohort accounts
    LEFT JOIN 
    (SELECT
mart_arr.ARR_MONTH
--,ping_created_at
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
--,MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
--,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
-- LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
--     ON mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
--,product_rate_plan_name
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
)) EOA 
    ON EOA.dim_crm_account_id = acct.dim_crm_account_id 
------free limit promo cohort accounts
LEFT JOIN 
    (select
  distinct
dim_crm_account_id
  from restricted_safe_common_mart_sales.mart_charge charge
  where 
  subscription_start_date >= '2023-02-01'
  and rate_plan_charge_description = 'fo-discount-70-percent') free_promo
    ON free_promo.dim_crm_account_id = acct.dim_crm_account_id 
------price increase promo cohort accounts
LEFT JOIN
    (select distinct dim_crm_account_id from
        (
         select
          charge.*,
          arr / quantity as actual_price,
          prod.annual_billing_list_price as list_price
          from restricted_safe_common_mart_sales.mart_charge charge
          inner join common.DIM_PRODUCT_DETAIL prod on charge.dim_product_detail_id = prod.dim_product_detail_id
          where 
          subscription_start_date >= '2023-04-01'
          and subscription_start_date <= '2023-07-01'
          and TYPE_OF_ARR_CHANGE = 'New'
          and quantity > 0
          and actual_price > 228
          and actual_price < 290
          and rate_plan_charge_name like '%Premium%'
        )) price_increase
        ON price_increase.dim_crm_account_id = acct.dim_crm_account_id 
LEFT JOIN
(
    select
distinct
dim_parent_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = date_trunc('month',current_date)
and product_tier_name like '%Ultimate%'
) ultimate on acct.dim_parent_crm_account_id = ultimate.dim_parent_crm_account_id


-------filtering to get current account data     
WHERE acct.snapshot_date = current_date-1),



---------------need to add fields to create timeframes for case pulls 
--------------this table pulls in all of the data for the accounts (from account CTE), opportunities, subscription, and billing account (PO required field only) to act as the base table for the query 
account_blended as (
SELECT 
$LAST_UPLOAD_DATE,
DATEADD('day', 30, $LAST_UPLOAD_DATE) as last_upload_30_days,
DATEADD('day', 90, $LAST_UPLOAD_DATE) as last_upload_90_days, 
DATEADD('day', 180, $LAST_UPLOAD_DATE) as last_upload_180_days, 
DATEADD('day', 270, $LAST_UPLOAD_DATE) as last_upload_270_days, 
DATEADD('day', 30, current_date) as current_date_30_days, 
DATEADD('day', 90, current_date) as current_date_90_days, 
DATEADD('day', 180, current_date) as current_date_180_days,
DATEADD('day', 270, current_date) as current_date_270_days,
DATEDIFF('day', $LAST_UPLOAD_DATE, current_date) as days_since_last_upload,
a.dim_crm_account_id as account_id, 
a.account_owner,
a.USER_ROLE_TYPE, 
a.CRM_ACCOUNT_OWNER_GEO,
a.PARENT_CRM_ACCOUNT_SALES_SEGMENT, 
a.PARENT_CRM_ACCOUNT_BUSINESS_UNIT, 
a.NEXT_RENEWAL_DATE, 
a.calculated_tier, 
a.team, 
a.high_value_case_owner,
a.high_value_case_owner_team,
a.high_value_case_owner_manager, 
a.count_active_subscriptions,
DATEDIFF('day', current_date, a.next_renewal_date) as days_till_next_renewal,
-- DATEDIFF('day', last_upload_date, a.next_renewal_date) as days_from_last_upload_to_next_renewal,
a.CARR_ACCOUNT_FAMILY as CARR_ACCOUNT_FAMILY,
a.CARR_THIS_ACCOUNT,
a.six_sense_account_buying_stage, 
a.SIX_SENSE_ACCOUNT_PROFILE_FIT, 
a.SIX_SENSE_ACCOUNT_INTENT_SCORE,
a.SIX_SENSE_ACCOUNT_UPDATE_DATE,
a.GS_HEALTH_USER_ENGAGEMENT,
a.GS_HEALTH_CD,
a.GS_HEALTH_DEVSECOPS,
a.GS_HEALTH_CI, 
a.GS_HEALTH_SCM, 
a.GS_FIRST_VALUE_DATE,
a.GS_LAST_CSM_ACTIVITY_DATE,
DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, current_date) as days_since_6sense_account_update,
-- DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, last_upload_date) as days_since_6sense_account_update_last_upload,
o.dim_crm_opportunity_id as opportunity_id,
o.owner_id as opportunity_owner_id,
CASE WHEN o.sales_type = 'Renewal' and o.close_date >= '2024-02-01' and o.close_date <= '2025-01-31' THEN True ELSE False end as FY25_Renewal,
o.is_closed, 
o.is_won,
o.sales_type, 
o.close_date, 
o.opportunity_term, 
o.opportunity_name,
o.QSR_status,
o.stage_name,
qsr.qsr_failed_last_75,
s.dim_subscription_id as sub_subscription_id, 
s.dim_subscription_id_original as sub_subscription_id_original,
s.subscription_status, 
s.term_start_date as current_subscription_start_date,
s.term_end_date as current_subscription_end_date,
DATEDIFF('day', s.term_start_date, current_date) as days_into_current_subscription,
-- DATEDIFF('day', s.term_start_date, last_upload_date) as days_into_current_subscription_last_upload,
s.turn_on_auto_renewal,
ptc.score_date as ptc_score_date, 
DATEDIFF('day', ptc.score_date, current_date) as days_since_last_ptc_score, 
-- DATEDIFF('day', s.term_start_date, last_upload_date) as days_into_current_subscription_last_upload,
ptc.score as ptc_score, 
ptc.score_group as ptc_score_group,
ptc.latest_score_date as ptc_latest_score_date,
ptc.insights as ptc_insights,
pte.score_date as pte_score_date, 
DATEDIFF('day', pte.score_date, current_date) as days_since_last_pte_score,
-- DATEDIFF('day', pte.score_date, last_upload_date) as days_since_last_pte_score_last_upload,
pte.score as pte_score, 
pte.score_group as pte_score_group,
pte.latest_score_date as pte_latest_score_date,
pte.insights as pte_insights,
lpte.most_recent_pte, 
lptc.most_recent_ptc,
cd.created_date as last_case_created_date, 
DATEDIFF('day', CAST(cd.created_date as date), current_date) as days_since_last_case_created,
cd.case_id as last_case_id, 
cd.subject as last_case_subject, 
cd.status as last_case_status, 
cd.owner_id as last_case_owner_id, 
DATEDIFF('day', CAST(lr.created_date as date), current_date) as days_since_last_renewal_case_created,
lr.case_id as last_renewal_case_id, 
lr.subject as last_renewal_case_subject, 
lr.status as last_renewal_case_status, 
lr.owner_id as last_renewal_case_owner_id,
oc.count_cases as current_open_cases,
oo.count_open_opps,
COUNT(distinct DIM_SUBSCRIPTION_ID) OVER (PARTITION BY s.dim_crm_account_id) AS count_subscriptions,
CASE WHEN HA_last_180 = True then TRUE else FALSE end as HA_LAST_180, 
CASE WHEN LA_last_180 = TRUE then TRUE else FALSE end as LA_last_180,
CASE WHEN High_Value_Last_90 = True then True Else False end as High_Value_Last_90, 
DATEDIFF('day', s.term_end_date, current_date) as days_since_current_sub_end_date,
-- DATEDIFF('day', s.term_end_date, last_upload_date) as days_since_current_sub_end_date_last_upload,
s.dim_subscription_id,
CASE WHEN sales_type = 'Renewal' then DATEDIFF('day', current_date, o.close_date) else null end as days_till_close,
-- CASE WHEN sales_type = 'Renewal' then DATEDIFF('day', current_date, last_upload_date) else null end as days_till_close_last_upload,
CASE WHEN o.opportunity_name LIKE '%QSR%' THEN TRUE ELSE FALSE END AS QSR_Flag,
CASE WHEN qsr_flag = false and sales_type != 'Renewal' then TRUE else FALSE END AS non_qsr_non_renewal_oppty_flag,
CASE WHEN o.sales_type = 'Renewal' THEN TRUE ELSE FALSE END AS Renewal_Flag,
a.EOA_flag,
b.PO_required,  
o.auto_renewal_status,
CASE WHEN (CONTAINS(opportunity_name, '#ultimateupgrade') or CONTAINS(opportunity_name, 'Ultimate Upgrade') or CONTAINS(opportunity_name, 'Upgrade to Ultimate') or CONTAINS(opportunity_name, 'ultimate upgrade')) THEN TRUE ELSE FALSE END AS Ultimate_Upgrade_Oppty_Flag
FROM account_base a
LEFT JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o
    ON a.dim_crm_account_id = o.dim_crm_account_id
    --AND o.is_closed = False
    AND DATEDIFF('day', current_date, o.close_date) <= 365
LEFT JOIN PROD.COMMON.DIM_SUBSCRIPTION s
    ON o.dim_crm_opportunity_id = s.dim_crm_opportunity_id_current_open_renewal
    and subscription_status = 'Active'
LEFT JOIN (SELECT distinct dim_crm_account_id, PO_required FROM PROD.common.dim_billing_account WHERE PO_REQUIRED = 'YES') b 
    ON b.dim_crm_account_id = a.dim_crm_account_id
-- LEFT JOIN PROD.common.dim_billing_account b
--     ON a.dim_crm_account_id = b.dim_crm_account_id
LEFT JOIN (SELECT *, max(score_date) over(partition by CRM_ACCOUNT_ID) as latest_score_date FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTC_SCORES"  QUALIFY score_date = latest_score_date) ptc 
    ON ptc.crm_account_id = a.dim_crm_account_id
LEFT JOIN (SELECT *, max(score_date) over(partition by CRM_ACCOUNT_ID) as latest_score_date FROM "PROD"."WORKSPACE_DATA_SCIENCE"."PTE_SCORES"  QUALIFY score_date = latest_score_date) pte 
    ON pte.crm_account_id = a.dim_crm_account_id
LEFT JOIN last_case_data cd 
    ON cd.account_id = a.dim_crm_account_id
LEFT JOIN last_renewal_case_data lr 
    ON lr.account_id = a.dim_crm_account_id
LEFT JOIN open_cases oc
    ON oc.account_id = a.dim_crm_account_id
LEFT JOIN open_renewal_opps oo
    ON oo.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN low_adoption_last_180 
    ON low_adoption_last_180.account_id = a.dim_crm_account_id
LEFT JOIN high_adoption_last_180
    ON high_adoption_last_180.account_id = a.dim_crm_account_id
LEFT JOIN qsr_failed_last_75 qsr
    ON qsr.account_id = a.dim_crm_account_id
    AND qsr.opportunity_id = o.dim_crm_opportunity_id
LEFT JOIN latest_ptc_score lptc
LEFT JOIN latest_pte_score lpte
LEFT JOIN high_value_case_last_90
    ON high_value_case_last_90.account_id = a.dim_crm_account_id
--JOIN last_created_date
---------------------currently a placeholder and will need to be substituted in for all SMB sements--------
-- WHERE a.PARENT_CRM_ACCOUNT_SALES_SEGMENT = 'SMB'
-- AND carr_account_family < 30000 and carr_account
WHERE (a.carr_account_family <= 30000 and a.carr_this_account > 0)
and (a.parent_crm_account_max_family_employee <= 100 or a.parent_crm_account_max_family_employee is null)
and a.ultimate_customer_flag = false
and a.parent_crm_account_sales_segment in ('SMB','Mid-Market','Large')
and a.parent_crm_account_upa_country <> 'JP'
and a.is_jihu_account = false),





utilization as (
SELECT 
mart_arr.ARR_MONTH
,monthly_mart.snapshot_month 
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
,monthly_mart.max_BILLABLE_USER_COUNT
,monthly_mart.LICENSE_USER_COUNT
,monthly_mart.subscription_start_date
,monthly_mart.subscription_end_date
,monthly_mart.term_end_date
,monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
,(mart_arr.ARR / mart_arr.QUANTITY)*(monthly_mart.max_BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as overage_amount
,max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
LEFT JOIN 
  (select
  DISTINCT
    snapshot_month,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    INSTANCE_TYPE,
    subscription_status,
    subscription_start_date,
    subscription_end_date,
    term_end_date, 
    LICENSE_USER_COUNT,
    MAX(BILLABLE_USER_COUNT) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as max_billable_user_count
    FROM COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'  
    and snapshot_month = DATE_TRUNC('month',CURRENT_DATE)
  ) monthly_mart
      ON mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL = monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL
WHERE ARR_MONTH = snapshot_month
and PRODUCT_TIER_NAME not like '%Storage%'
), 

----------------------------get the date on a subscription when a customer switches from auto-renew on to auto-renew off
autorenew_switch as (
select
dim_crm_account_id, 
dim_subscription_id,
active_autorenew_status,
prior_auto_renewal,
MAX(snapshot_date) as latest_switch_date
from
(
  select 
date_actual as snapshot_date,
PREP_BILLING_ACCOUNT_USER.user_name as update_user,
PREP_BILLING_ACCOUNT_USER.IS_INTEGRATION_USER,
TURN_ON_AUTO_RENEWAL as active_autorenew_status,
lag(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as prior_auto_renewal,
lead(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as future_auto_renewal,
sub.*
from common.dim_subscription_snapshot_bottom_up sub
inner join common.dim_date on sub.snapshot_id = dim_date.date_id
inner join restricted_safe_common_prep.PREP_BILLING_ACCOUNT_USER on sub.UPDATED_BY_ID = PREP_BILLING_ACCOUNT_USER.ZUORA_USER_ID
where snapshot_date >= '2023-02-01'
  and snapshot_date < CURRENT_DATE
  and subscription_status = 'Active'
 -- and update_user = 'svc_zuora_fulfillment_int@gitlab.com'
--and (TURN_ON_AUTO_RENEWAL = 'Yes' or TURN_ON_AUTO_RENEWAL is null)
order by snapshot_date asc
)
where
((active_autorenew_status = 'No' and update_user = 'svc_zuora_fulfillment_int@gitlab.com') and prior_auto_renewal = 'Yes')
GROUP BY 1,2,3,4
 ),


--Looks for current month ARR around $15 to account for currency conversion.
--Checks to make sure accounts previously had ARR in Bronze or Starter (to exclude accounts that just have discounts)
eoa_accounts_fy24 as
(
SELECT 
distinct dim_crm_account_id 
FROM
(
SELECT
mart_arr.ARR_MONTH
,mart_arr.SUBSCRIPTION_END_MONTH
,mart_arr.DIM_CRM_ACCOUNT_ID
,mart_arr.CRM_ACCOUNT_NAME
,mart_arr.DIM_SUBSCRIPTION_ID
,mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
,mart_arr.SUBSCRIPTION_NAME
,mart_arr.subscription_sales_type
,mart_arr.AUTO_PAY
,mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
,mart_arr.CONTRACT_AUTO_RENEWAL
,mart_arr.TURN_ON_AUTO_RENEWAL
,mart_arr.TURN_ON_CLOUD_LICENSING
,mart_arr.CONTRACT_SEAT_RECONCILIATION
,mart_arr.TURN_ON_SEAT_RECONCILIATION
,case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
,mart_arr.PRODUCT_TIER_NAME
,mart_arr.PRODUCT_DELIVERY_TYPE
,mart_arr.PRODUCT_RATE_PLAN_NAME
,mart_arr.ARR
,(mart_arr.ARR / mart_arr.QUANTITY)  as arr_per_user,
arr_per_user/12 as monthly_price_per_user,
mart_arr.mrr/mart_arr.quantity as mrr_check
FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
WHERE ARR_MONTH = '2023-01-01'
and PRODUCT_TIER_NAME like '%Premium%'
and ((monthly_price_per_user >= 14
and monthly_price_per_user <= 16) or (monthly_price_per_user >= 7.5 and monthly_price_per_user <= 9.5))
and dim_crm_account_id in
(
select
DIM_CRM_ACCOUNT_ID
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month >= '2020-02-01'
and arr_month <= '2022-02-01'
and product_rate_plan_name like any ('%Bronze%','%Starter%')
) order by dim_crm_account_id asc)),
 

 
prior_sub_25_eoa as
(
SELECT 
distinct dim_crm_account_id 
FROM
(
SELECT
mart_charge.*,
dim_subscription.dim_crm_opportunity_id,
dim_subscription.dim_crm_opportunity_id_current_open_renewal,
previous_mrr / previous_quantity as previous_price,
mrr / quantity as price_after_renewal
FROM PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
LEFT JOIN PROD.COMMON.DIM_SUBSCRIPTION
    ON mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
INNER JOIN eoa_accounts_fy24 on eoa_accounts_fy24.dim_crm_account_id = mart_charge.dim_crm_account_id
-- left join PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY on dim_subscription.dim_crm_opportunity_id_current_open_renewal = mart_crm_opportunity.dim_crm_opportunity_id
WHERE mart_charge.rate_plan_name like '%Premium%'
and type_of_arr_change <> 'New'
and mart_charge.term_start_date >= '2023-02-01'
and mart_charge.term_start_date < current_date
and price_after_renewal > previous_price
and previous_quantity <> 0
and quantity <> 0
and (lower(rate_plan_charge_description) like '%eoa%'
or ((previous_price >= 5 and previous_price <= 7)
or (previous_price >= 8 and previous_price <= 10)
or (previous_price >= 14 and previous_price <= 16)))
and quantity < 25)),




-----------brings together account blended date, utilization data, and autorenewal switch data 
all_data as(
SELECT a.*, 
u.overage_count,
u.overage_amount,
u.latest_overage_month,
u.qsr_enabled_flag,
u.max_billable_user_count,
u.LICENSE_USER_COUNT,
u.ARR,
s.latest_switch_date, 
DATEDIFF('day', latest_switch_date, current_date) as days_since_autorenewal_switch, 
CASE WHEN prior_sub_25_eoa.dim_crm_account_id IS NOT NULL then True else False end as prior_year_eoa_under_25_flag
FROM account_blended a
LEFT JOIN utilization u
    ON u.dim_crm_account_id = a.account_id
    --and term_end_date = current_subscription_end_date
    and u.dim_subscription_id_original = a.sub_subscription_id_original
LEFT JOIN autorenew_switch s
    ON s.dim_crm_account_id = a.account_id
    and s.dim_subscription_id = sub_subscription_id
LEFT JOIN prior_sub_25_eoa 
    ON prior_sub_25_eoa.dim_crm_account_id = a.account_id),





------------flags each account/opportunity with any applicable flags
case_flags as (
SELECT *,
CASE WHEN calculated_tier = 'Tier 1' and High_Value_Last_90 = False then True ELSE False end as check_in_case_needed_flag,
CASE WHEN sales_type = 'Renewal' AND PO_REQUIRED = 'YES' then TRUE ELSE FALSE END AS PO_REQUIRED_Flag, 
CASE WHEN opportunity_term > 12 OR (CONTAINS(opportunity_name, '2 year') or CONTAINS(opportunity_name, '3 year')) then TRUE else FALSE end as multiyear_renewal_flag,
CASE WHEN (auto_renewal_status like '%EoA%' or auto_renewal_status like 'EoA%') THEN TRUE ELSE FALSE end as EOA_Auto_Renewal_Will_Fail_Flag,
CASE WHEN ((auto_renewal_status != NULL or auto_renewal_status not in ('On', 'Off')) AND EOA_Auto_Renewal_Will_Fail_Flag = False) THEN TRUE ELSE FALSE END AS Auto_Renewal_Will_Fail_Flag, 
CASE WHEN eoa_flag = True and prior_year_eoa_under_25_flag = True and max_BILLABLE_USER_COUNT >= 25 THEN TRUE ELSE FALSE end as EOA_OVER_25_FLAG,
CASE WHEN QSR_status = 'Failed' and is_closed = False then TRUE ELSE FALSE END AS QSR_Failed_Flag, 
CASE WHEN sales_type = 'Renewal' and days_since_current_sub_end_date > 0 and is_closed = False then TRUE ELSE FALSE END as Overdue_Renewal_Flag,
--CASE WHEN days_since_autorenewal_switch = 2 then TRUE ELSE FALSE END AS Auto_Renew_Recently_Turned_Off_Flag, ----- commenting out to remove timeframe filter (add this in another query
CASE WHEN latest_switch_date IS NOT NULL THEN TRUE ELSE FALSE END AS Auto_Renew_Recently_Turned_Off_Flag,
-- CASE WHEN overage_count < 0 and overage_amount < 0 and latest_overage_month = DATE_TRUNC('month', current_date) and DATEDIFF('day', current_date, term_end_date) > 120 then TRUE ELSE FALSE END AS Underutiliation_Flag, 
CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') and overage_count < 0 and overage_amount < 0 and latest_overage_month = DATE_TRUNC('month', current_date) then TRUE ELSE FALSE END AS Underutilization_Flag, 
--CASE WHEN ptc_score > .5 = false and ptc_last_90 = False and days_since_last_ptc_score = 2 then TRUE ELSE FALSE END AS High_PTC_Score_Flag, ----placeholder, what timeframe after scoring do we want to create cases
CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') and ptc_score > .5 and ptc_latest_score_date = most_recent_ptc and sales_type = 'Renewal' then TRUE ELSE FALSE END AS High_PTC_Score_Flag,
--CASE WHEN pte_score > .5 = false and pte_last_90 = False and days_since_last_pte_score = 2 then TRUE ELSE FALSE END AS High_PTE_Score_Flag, ----placeholder, what timeframe after scoring do we want to create cases
CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') and pte_score > .5 and pte_latest_score_date = most_recent_pte and sales_type = 'Renewal' then TRUE ELSE FALSE END AS High_PTE_Score_Flag,
CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') and overage_count > 0 and overage_amount > 0 and latest_overage_month = DATE_TRUNC('month', current_date) and qsr_enabled_flag = false then TRUE ELSE FALSE END AS Overage_QSR_Off_Flag,
--CASE WHEN six_sense_account_buying_stage in ('Decision', 'Purchase') and six_sense_account_profile_fit = 'Strong' and SIX_SENSE_ACCOUNT_INTENT_SCORE > 74 and DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, current_date) <= 7 then TRUE ELSE FALSE END AS SixSense_Growth_Signal_Flag
CASE WHEN calculated_tier in ('Tier 2', 'Tier 3') and six_sense_account_buying_stage in ('Decision', 'Purchase') and six_sense_account_profile_fit = 'Strong' and SIX_SENSE_ACCOUNT_INTENT_SCORE > 74 then TRUE ELSE FALSE END AS SixSense_Growth_Signal_Flag
FROM all_data
WHERE is_closed = False
),




-----------targeted date CTE 
-----------needed for daily pull 
-- cases as (
-- SELECT *,
-- CASE WHEN check_in_case_needed_flag = True and (days_into_current_subscription <= 90 OR days_into_current_subscription <= 180 OR days_into_current_subscription <= 270) then 'High Value Account Check In' end as Check_In_Trigger,
-- CASE WHEN PO_REQUIRED_Flag = True and days_till_close = 30 then 'PO Required'
--      WHEN multiyear_renewal_flag = True and days_till_close = 30 then 'Multiyear Renewal'
--      WHEN EOA_Auto_Renewal_Will_Fail_Flag = True and days_till_close = 30 then 'EOA Renewal'
--      WHEN Auto_Renewal_Will_Fail_Flag = True and days_till_close = 30 then 'Auto-Renewal Will Fail'
--      WHEN Overdue_Renewal_Flag = True and days_since_current_sub_end_date = 2 then 'Overdue Renewal'
--      WHEN Auto_Renew_Recently_Turned_Off_Flag = TRUE and days_since_autorenewal_switch = 2 then 'Auto-Renew Recently Turned Off'
--      WHEN QSR_Failed_Flag = True and qsr_failed_last_75 != True then 'Failed QSR' ELSE NULL END AS Renewal_Trigger_Name, ----90 days is a placeholder (waiting for MIke to confirm timeframe)
-- CASE WHEN (Underutilization_Flag = True and (days_till_close < 270 and days_till_close > 180)) and LA_last_180 = False then 'Underutilization' 
--      WHEN (High_PTC_Score_Flag = True and (days_till_close < 180 and days_till_close > 30)) and LA_last_180 = False then 'High PTC Score' ELSE NULL END AS Low_Adoption_Case_Trigger_Name, 
-- CASE WHEN Overage_QSR_Off_Flag = True and (days_till_close < 270 and days_till_close > 30) and HA_last_180 = False then 'Overage and QSR Off'
--      WHEN High_PTE_Score_Flag = True and (days_till_close < 270 and days_till_close > 30) and HA_last_180 = False and low_adoption_case_trigger_Name IS NULL then 'High PTE Score'
--      WHEN SixSense_Growth_Signal_Flag = True and days_since_6sense_account_update = 2 and (days_till_close < 270 and days_till_close > 30) and low_adoption_case_trigger_name IS NULL and HA_last_180 = False then '6sense Growth Signal' ELSE NULL END AS High_Adoption_Case_Trigger_Name
-- FROM case_flags
-- WHERE calculated_tier != 'Tier 3'
-- )


----------adds timeframe to case flags to determine which cases should be created within this period of time
----------STEPS TO DO HERE:
----------1. For automated query: double check timelines and that tiers fall into the proper case types 
----------2. For manual pull query: change timelines and get a last pulled date -- logic from sisense query? 
cases as (
SELECT *,
CASE WHEN check_in_case_needed_flag = True and ((current_subscription_end_date > last_upload_90_days AND current_subscription_end_date <= current_date_90_days) OR (current_subscription_end_date > last_upload_180_days AND current_subscription_end_date <= current_date_180_days) OR (current_subscription_end_date > last_upload_270_days AND current_subscription_end_date <= current_date_270_days)) then 'High Value Account Check In' end as Check_In_Trigger_Name,
CASE WHEN PO_REQUIRED_Flag = True and (close_date > last_upload_30_days AND close_date <= current_date_30_days) then 'PO Required'
     WHEN multiyear_renewal_flag = True and (close_date > last_upload_30_days AND close_date <= current_date_30_days) then 'Multiyear Renewal'
     WHEN EOA_Auto_Renewal_Will_Fail_Flag = True and (close_date > last_upload_30_days AND close_date <= current_date_30_days) then 'Auto-Renewal Will Fail'
     WHEN Auto_Renewal_Will_Fail_Flag = True and (close_date > last_upload_30_days AND close_date <= current_date_30_days) then 'Auto-Renewal Will Fail'
     WHEN EOA_OVER_25_FLAG and (close_date > last_upload_30_days AND close_date <= current_date_30_days) then 'EOA Renewal'
     WHEN Overdue_Renewal_Flag = True and (current_subscription_end_date > DATEADD('day', -2, $LAST_UPLOAD_DATE) AND current_subscription_end_date <= DATEADD('day', -2, current_date)) then 'Overdue Renewal'
     WHEN Auto_Renew_Recently_Turned_Off_Flag = TRUE and (latest_switch_date > DATEADD('day', -2, $LAST_UPLOAD_DATE) AND latest_switch_date <= DATEADD('day', -2, current_date)) then 'Auto-Renew Recently Turned Off'
     WHEN QSR_Failed_Flag = True and qsr_failed_last_75 != True then 'Failed QSR' ELSE NULL END AS Renewal_Trigger_Name, 
----------------do these timeframes really need to change? if we want to create cases that are between 180 and 270 days out from close date?      
CASE WHEN (Underutilization_Flag = True and (days_till_close < 270 and days_till_close > 180)) and LA_last_180 = False then 'Underutilization' 
     WHEN (High_PTC_Score_Flag = True and (days_till_close < 180 and days_till_close > 30)) and LA_last_180 = False then 'High PTC Score' ELSE NULL END AS Low_Adoption_Case_Trigger_Name, 
 ----------------do these timeframes really need to change? if we want to create cases that are between 180 and 270 days out from close date?      
CASE WHEN Overage_QSR_Off_Flag = True and (days_till_close < 270 and days_till_close > 30) and HA_last_180 = False then 'Overage and QSR Off'
     WHEN High_PTE_Score_Flag = True and (days_till_close < 270 and days_till_close > 30) and HA_last_180 = False and low_adoption_case_trigger_Name IS NULL then 'High PTE Score'
     WHEN SixSense_Growth_Signal_Flag = True and (days_since_6sense_account_update >= 2 and days_since_6sense_account_update < 2+days_since_last_upload) and (days_till_close < 270 and days_till_close > 30) and low_adoption_case_trigger_name IS NULL and HA_last_180 = False then '6sense Growth Signal' ELSE NULL END AS High_Adoption_Case_Trigger_Name
FROM case_flags
WHERE calculated_tier != 'Tier 3'
),


--------------prioritizes renewals first and then low adoption over high adoption limiting at 20 per each type
final as (
SELECT * 
FROM (
SELECT *,
ROW_NUMBER() OVER(PARTITION BY CASE_TRIGGER order by overage_amount) as underutilization_top, 
ROW_NUMBER() OVER(PARTITION BY CASE_TRIGGER ORDER BY OVERAGE_AMOUNT desc) AS overage_top,
ROW_NUMBER() OVER(PARTITION BY CASE_TRIGGER ORDER BY CARR_THIS_ACCOUNT desc) as PTC_top,
ROW_NUMBER() OVER(PARTITION BY CASE_TRIGGER ORDER BY CARR_THIS_ACCOUNT desc) as PTE_top
FROM(
SELECT *, 
CASE WHEN Check_In_Trigger_name IS NOT NULL then check_in_trigger_name
     WHEN renewal_trigger_name IS NOT NULL then renewal_trigger_name
     WHEN (renewal_trigger_name IS NULL and low_adoption_case_trigger_name IS NOT NULL) then low_adoption_case_trigger_name
     WHEN (renewal_trigger_name IS NULL and low_adoption_case_trigger_name IS NULL and high_adoption_case_trigger_name IS NOT NULL) then high_adoption_case_trigger_name
     ELSE NULL end as case_trigger, 
CASE WHEN renewal_trigger_name IS NOT NULL then True else False end as renewal_case
FROM cases
WHERE case_trigger is not null))
WHERE (case_trigger not in ('High PTE Score', 'High PTC Score', 'Underutilization', 'Overage and QSR Off') OR (case_trigger = 'High PTE Score' and PTE_Top <= 20) OR (case_trigger = 'High PTC Score' and PTC_Top <= 20) OR (case_trigger = 'Underutilization' and underutilization_top <= 20) OR (case_trigger = 'Overage and QSR Off' and overage_top <= 20))
), 


FY26_high_value_account_cases as (
SELECT 
*, 
CASE WHEN calculated_tier = 'Tier 1' and FY25_Renewal = True and is_won = True then 'FY26 High Value Account' end as case_trigger 
--s.dim_crm_opportunity_id_current_open_renewal as next_renewal_opportunity_id
FROM all_data a
-- JOIN dim_subscription s
--     ON a.opportunity_id = S.dim_crm_opportunity_id
WHERE case_trigger IS NOT NULL
),


high_value_cases as (
SELECT 
f.case_trigger as case_trigger,
f.account_id,
f.calculated_tier as account_tier,
cd.case_trigger_id,  
cd.status, 
cd.case_origin, 
cd.type, 
case_subject, 
high_value_case_owner as owner_id,
cd.case_reason,
cd.record_type_id,
cd.priority,
----------NEED TO UPDATE THIS TO GET NEXT OPEN RENEWAL OPPORTUNITY ID
CASE WHEN cd.case_trigger_id in (10, 11, 12) then NULL else f.opportunity_id end as case_opportunity_id, 
cd.case_cta, 
CONCAT('Renewal Date: ', close_date, ';', ' PTC Score ', ' ', IFNULL(ptc_score, 0000), ';', ' PTE Score ', ' ', IFNULL(pte_score, 0000), ';', ' GS_HEALTH_USER_ENGAGEMENT ', ' ', IFNULL(GS_HEALTH_USER_ENGAGEMENT, 'NOT AVAILABLE'), ';', ' GS_HEALTH_CD ', ' ', IFNULL(GS_HEALTH_CD,'NOT AVAILABLE'), ';', ' GS_HEALTH_DEVSECOPS ', ' ', IFNULL(GS_HEALTH_DEVSECOPS, 'NOT AVAILABLE'), ';', ' GS_HEALTH_CI ', ' ', IFNULL(GS_HEALTH_CI, 'NOT AVAILABLE'), ';', ' GS_HEALTH_SCM ', ' ', IFNULL(GS_HEALTH_SCM, 'NOT AVAILABLE'), ';', ' GS_FIRST_VALUE_DATE ', ' ', IFNULL(CAST(GS_FIRST_VALUE_DATE AS DATE), '3000-02-01'), ';', ' Billable Users:', ' ', IFNULL(max_BILLABLE_USER_COUNT, 0000), ';', ' Licensed Users:', ' ', IFNULL(LICENSE_USER_COUNT, 0000), ';', ' Overage Amount:', ' ', IFNULL(overage_amount, 0000), ';', 'ARR:', ' ', IFNULL(ARR, 0000), ';' ) as context 
FROM FY26_high_value_account_cases f
LEFT JOIN boneyard.case_data cd
    ON f.case_trigger = cd.case_trigger
WHERE f.case_trigger IS NOT NULL
)


SELECT 
f.case_trigger as case_trigger,
f.account_id,
f.calculated_tier as account_tier,
cd.case_trigger_id,  
cd.status, 
cd.case_origin, 
cd.type, 
CASE WHEN renewal_case = true then CONCAT(cd.case_subject, ' ', f.close_date) ELSE cd.case_subject end as case_subject, 
CASE WHEN calculated_tier = 'Tier 1' then high_value_case_owner 
     WHEN current_open_cases > 0 then last_case_owner_id 
     ELSE cd.owner_id end as owner_id,
cd.case_reason,
cd.record_type_id,
cd.priority,
CASE WHEN cd.case_trigger_id in (10, 11, 12) then NULL else f.opportunity_id end as case_opportunity_id, 
cd.case_cta, 
CASE WHEN cd.case_trigger_id = 9 then CONCAT(cd.CASE_CONTEXT, ' ', ptc_insights)
     WHEN cd.case_trigger_id = 11 then CONCAT(cd.CASE_CONTEXT, ' ', pte_insights)
     WHEN cd.case_trigger_id = 6 then CONCAT(cd.CASE_CONTEXT, ' ', latest_switch_date)
     WHEN cd.case_trigger_id = 5 then CONCAT(cd.case_context, ' ', current_subscription_end_date)
     WHEN cd.case_trigger_id = 7 then CONCAT(cd.CASE_CONTEXT, ' ', opportunity_id)
     WHEN cd.case_trigger_id = 4 then CONCAT(cd.CASE_CONTEXT, ' ', auto_renewal_status)
     WHEN cd.case_trigger_id = 12 then CONCAT(cd.CASE_CONTEXT, ' ', SIX_SENSE_ACCOUNT_INTENT_SCORE) 
     ELSE cd.CASE_CONTEXT END AS context 
FROM FINAL f
LEFT JOIN boneyard.case_data cd
    ON f.case_trigger = cd.case_trigger

UNION 

SELECT * FROM high_value_cases


