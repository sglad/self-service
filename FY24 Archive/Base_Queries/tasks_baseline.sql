-- Returns all completed Outreach tasks
-- Intended to be used mainly for understanding calls, meetings, and emails by AEs and SDRs

select * from

(select 
    task_id,
task_status,
task.dim_crm_account_id,
task.dim_crm_user_id,
task.dim_crm_person_id,
task_subject,
case when lower(task_subject) like '%email%' then 'Email'
when lower(task_subject) like '%call%' then 'Call'
when lower(task_subject) like '%linkedin%' then 'LinkedIn'
when lower(task_subject) like '%inmail%' then 'LinkedIn'
when lower(task_subject) like '%sales navigator%' then 'LinkedIn'
when lower(task_subject) like '%drift%' then 'Chat'
when lower(task_subject) like '%chat%' then 'Chat'
else
task_type end as type,
case when task_subject like '%Outreach%' and task_subject not like '%Advanced Outreach%' then 'Outreach'
  when task_subject like '%Clari%' then 'Clari'
when task_subject like '%Conversica%' then 'Conversica'
  else 'Other' end as outreach_clari_flag,
TASK_CREATED_DATE,
case when outreach_clari_flag = 'Outreach' and (task_subject like '%[Out]%' or task_subject like '%utbound%') then 'Outbound'
when outreach_clari_flag = 'Outreach' and (task_subject like '%[In]%' or task_subject like '%nbound%') then 'Inbound'
else 'Other' end as inbound_outbound_flag,
case when inbound_outbound_flag = 'Outbound' and task_subject like '%Answered%' and task_subject not like '%Not Answer%'
and task_subject not like '%No Answer%' then true else false end as outbound_answered_flag,
task_date,
   row_number() over(partition by dim_crm_person_id order by task_date asc) as task_order,

      case when task.TASK_CREATED_BY_ID like '0054M000003Tqub%' then 'Outreach'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%GitLab Transactions%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Was Sent Email%' then 'SFDC Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Your GitLab License%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Advanced Outreach%' then 'Gainsight Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Filled Out Form%' then 'Marketo Form Fill'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Conversation in Drift%' then 'Drift'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Opened Email%' then 'Marketing Email Opened'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Sales Navigator%' then 'Sales Navigator'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Clari - Email%' then 'Clari Email'
      else
      'Other' end as task_type,

      user.user_name,
      case when user.department like '%arketin%' then 'Marketing' else user.department end as department,
      user.is_active,
      user.crm_user_sales_segment,
      user.user_role_name,
 case when count(distinct case when outreach_clari_flag in ('Clari','Outreach') then task_id else null end) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id) > 1  and
  count(distinct case when outreach_clari_flag in ('Clari','Outreach') then outreach_clari_flag else null end) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id) = 2
  then true else false end as likely_dupe
  --,
 -- case when likely_dupe and outreach_clari_flag = 'Outreach' and account_or_opportunity_id is null then
 -- first_value(account_or_opportunity_id) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id order by outreach_clari_flag asc) else account_or_opportunity_id end as account_or_opportunity_id
      from
  prod.common_mart_sales.mart_crm_task task
     inner join common.dim_crm_user user on task.dim_crm_user_id = user.DIM_CRM_USER_ID
   where --createdbyid like '0054M000003Tqub%' and
      task.dim_crm_user_id is not null
and
      is_deleted = false
and task_date >= '2021-02-01'
and task_status = 'Completed')

where outreach_clari_flag = 'Outreach'
