/*
NOTES
- Can filter to LT 2 ways - by using LT_segment at the Opportunity level or by filtering on Pooled or FO AE Touch
- Added logic to get FY23 LT activities by using Role and Manager Name
*/
-----all data gets all closed won (or closed lost churn & contraction) opportunity data
with all_data as (
[FY24 Opportunity Baseline with LT]
  ),
  ---------gets the snapshot date in usable format to tie to activity date
  snapshot_user as
(
SELECT
TO_DATE(CAST(snapshot_id as string), 'YYYYMMDD') as user_snapshot_date,
*
FROM common.dim_crm_user_daily_snapshot
),
----------gets all activity data from salesforce
all_activities as (
 select * from
(select
    task_id,
task_status,
account_id,
owner_id,
LEAD_OR_CONTACT_ID,
task_subject,
case when lower(task_subject) like '%email%' then 'Email'
when lower(task_subject) like '%call%' then 'Call'
when lower(task_subject) like '%linkedin%' then 'LinkedIn'
when lower(task_subject) like '%inmail%' then 'LinkedIn'
when lower(task_subject) like '%sales navigator%' then 'LinkedIn'
when lower(task_subject) like '%drift%' then 'Chat'
when lower(task_subject) like '%chat%' then 'Chat'
else
task_type end as touch_type,
case when task_subject like '%Outreach%' then 'Outreach'
  when task_subject like '%Clari%' then 'Clari'
when task_subject like '%Conversica%' then 'Conversica'
  else 'Other' end as outreach_clari_flag,
TASK_CREATED_DATE,
task_date,
   row_number() over(partition by LEAD_OR_CONTACT_ID order by task_date asc) as task_order,
      case when task.TASK_CREATED_BY_ID like '0054M000003Tqub%' then 'Outreach'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%GitLab Transactions%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Was Sent Email%' then 'SFDC Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Your GitLab License%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Advanced Outreach%' then 'Gainsight Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Filled Out Form%' then 'Marketo Form Fill'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Conversation in Drift%' then 'Drift'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Opened Email%' then 'Marketing Email Opened'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Sales Navigator%' then 'Sales Navigator'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Clari - Email%' then 'Clari Email'
      else
      'Other' end as task_type,
      user1.user_name as user_name_ss,
      case
        when user1.department like '%arketin%' then 'Marketing'
        when user1.department in ('SMB', 'MM', 'Sales', 'Sales Development','Filed Sales','Enterprise Sales','Commercial Sales','Channel Sales','Field Sales', 'Channel-Indirect', 'TAM', 'ENTR', 'Channel', 'Channels', 'Commercial', 'Partners') then 'Sales'
      else user1.department end as department_ss,
      user1.is_active as is_active_ss,
      user1.crm_user_sales_segment as sales_segment_ss,
      user1.user_role_name as role_name_ss,
      user1.user_snapshot_date as user_snapshot_date,
      user1.crm_user_role_type,
      user1.crm_user_area,
    user1.manager_name,
//      user2.user_name as user_name_c,
//      case when user2.department like '%arketin%' then 'Marketing' else user2.department end as department_c,
//      user2.is_active as is_active_c,
//      user2.crm_user_sales_segment as sales_segment_c,
//      user2.user_role_name as role_name_c,
 case when count(distinct case when outreach_clari_flag in ('Clari','Outreach') then task_id else null end) over(partition by lead_or_contact_id,department_ss,task_date,owner_id) > 1  and
  count(distinct case when outreach_clari_flag in ('Clari','Outreach') then outreach_clari_flag else null end) over(partition by lead_or_contact_id,department_ss,task_date,owner_id) = 2
  then true else false end as likely_dupe,
 case when likely_dupe and outreach_clari_flag = 'Outreach' and account_or_opportunity_id is null then
 first_value(account_or_opportunity_id) over(partition by lead_or_contact_id,department_ss,task_date,owner_id order by outreach_clari_flag asc) else account_or_opportunity_id end as account_or_opportunity_id
      from
  legacy.sfdc_task task
     inner join snapshot_user user1 on task.owner_id = user1.DIM_CRM_USER_ID
    AND task.task_date = user1.user_snapshot_date
//    inner join common.dim_crm_user user2 on task.owner_id = user2.dim_crm_user_id
   where --createdbyid like '0054M000003Tqub%' and
      owner_id is not null
and
      is_deleted = false
and task_date >= '2021-02-01'
and task_status = 'Completed')
where (likely_dupe = false or outreach_clari_flag = 'Outreach')
and department_ss in ('Marketing','Sales')),
------segments the salesforce activity data by users: SDR, FO, Pooled, Other
segmented as (
SELECT *,
CASE when role_name_ss  like 'Sales Development%' OR role_name_ss like '%SDR%' then 1 else 0 end as SDR_Touch,
CASE when (role_name_ss like '%FO%' or role_name_ss like 'ASM%') and (crm_user_area = 'LOWTOUCH' or manager_name = 'Kendra Pounds') then 1 else 0 end as FO_AE_Touch,
CASE when role_name_ss like any ('%Pool%', '%POOL%') and (crm_user_area = 'LOWTOUCH' or manager_name = 'Kendra Pounds') then 1 else 0 end as Pooled_AE_Touch,
CASE
--   when (role_name_ss not like 'Sales Development%' AND role_name_ss not like '%SDR%'
--         AND role_name_ss not like '%FO%' AND role_name_ss not like 'ASM%' AND role_name_ss not like '%Pool%' AND role_name_ss not like '%POOL%') 
  when SDR_Touch = 0 and FO_AE_Touch = 0 and Pooled_AE_Touch = 0
  then 1 else 0 end as other_touch
FROM all_activities),
------combines the activity data with the opportunity data (connects to the account_id and is based on a touch between close date and 90 days prior)
combined as (
select
all_data.DIM_CRM_ACCOUNT_ID,
all_data.dim_crm_opportunity_id,
all_data.trx_type,
all_data.is_web_portal_purchase,
all_data.crm_opp_owner_region_stamped,
all_data.CRM_OPP_OWNER_SALES_SEGMENT_STAMPED,
all_data.CRM_ACCOUNT_OWNER_STAMPED_NAME,
all_data.SALES_QUALIFIED_SOURCE_NAME,
all_data.is_won,
all_data.is_closed,
all_data.LT_Segment,
  all_data.fiscal_year,
  all_data.fiscal_quarter_name_fy,
order_type,
sales_type,
qsr_flag,
task_id,
task_date,
segmented.touch_type,
segmented.task_type,
segmented.task_subject,
all_data.CLOSE_DATE,
--all_data.oppty_order,
segmented.role_name_ss,
segmented.SDR_Touch,
segmented.FO_AE_Touch,
segmented.Pooled_AE_Touch,
segmented.other_touch,
all_data.net_arr
from all_data
left join segmented  on
(
  (segmented.account_ID = all_data.DIM_CRM_ACCOUNT_ID)
 and segmented.task_date <= all_data.CLOSE_DATE
and segmented.task_date > (all_data.CLOSE_DATE - 90))
where
  all_data.CLOSE_DATE >= '2022-02-01'
  and [LT_Segment=Low_Touch]
  )

  SELECT * FROM combined
