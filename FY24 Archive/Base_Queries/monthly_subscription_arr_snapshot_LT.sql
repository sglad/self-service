/*
Joins MART_ARR to MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY to get a monthly snapshot of billable and licensed user counts
Also gets PRICE as the ARR / QUANTITY
Both SaaS and SM
Only Active Subscriptions
Sets ARR_MONTH = SNAPSHOT_MONTH so that we have the most up to date ARR/Subscription info associated with the license usage
Compare to the Draft License Utilization tool in SFDC to spot check individual accounts
Determines if QSR is enabled using CONTRACT_SEAT_RECONCILIATION and TURN_ON_SEAT_RECONCILIATION
Set to pull the most recent month snapshot of data, can be modified for historical
Calculates upcoming QSR date if there is one
Determines if subscription is impacted by Matterhorn reduced renewal pricing from 4/3/23 to 4/3/24 - look for MATTERHORN_IMPACTED_RENEWAL_FLAG
Excludes Storage
14767 subs across 14366 accounts, 298,000,000 ARR
SFDC Comp: https://gitlab.my.salesforce.com/00O8X000008gqwD
23K subs
20,500 accounts
475M ARR
Not all accounts report monthly usage data, so the query output will be less than what is shown in SFDC (10-15%)
*/

SELECT 

  mart_arr.ARR_MONTH,
  monthly_mart.snapshot_month
         , mart_arr.SUBSCRIPTION_END_MONTH
         , mart_arr.DIM_CRM_ACCOUNT_ID
         , mart_arr.CRM_ACCOUNT_NAME
         , mart_arr.DIM_SUBSCRIPTION_ID
         , mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
         , mart_arr.SUBSCRIPTION_NAME
           , mart_arr.subscription_sales_type
         , mart_arr.AUTO_PAY
         , mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
         , mart_arr.CONTRACT_AUTO_RENEWAL
         , mart_arr.TURN_ON_AUTO_RENEWAL
         , mart_arr.TURN_ON_CLOUD_LICENSING
         , mart_arr.CONTRACT_SEAT_RECONCILIATION
         , mart_arr.TURN_ON_SEAT_RECONCILIATION
         , case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
         , mart_arr.PRODUCT_TIER_NAME
         , mart_arr.PRODUCT_DELIVERY_TYPE
         , mart_arr.PRODUCT_RATE_PLAN_NAME
         , mart_arr.ARR
         , mart_arr.ARR / mart_arr.QUANTITY as price
--     ,mart_arr.QUANTITY
         , monthly_mart.PING_CREATED_AT
         , monthly_mart.HOSTNAME
         , monthly_mart.DIM_NAMESPACE_ID
         , monthly_mart.BILLABLE_USER_COUNT
         , monthly_mart.LICENSE_USER_COUNT
         , monthly_mart.subscription_start_date
         , monthly_mart.subscription_end_date
         , case when monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT > 0 then monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT else 0 end AS overage_count
        , max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month
      ,  crm_account_owner
    , sum(overage_count * price) over(partition by mart_arr.DIM_CRM_ACCOUNT_ID,ARR_MONTH) as total_overage
  , 
  case when qsr_enabled_flag and total_overage > 0
  then
  case when
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH)=  3
or
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 6
OR 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 9
then --date_trunc('month',CURRENT_DATE)::date
  dateadd('day',7,
  dateadd('month', -DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH), subscription_end_date))::date
when 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 4
or
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 7
OR 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 10
then --dateadd('month',1,date_trunc('month',CURRENT_DATE))::date
   dateadd('day',7,
  dateadd('month', -DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) + 1, subscription_end_date))::date
when 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 5
or
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 8
OR 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 11
then --dateadd('month',2,date_trunc('month',CURRENT_DATE))::date
   dateadd('day',7,
dateadd('month', -DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) + 2, subscription_end_date))::date
when 
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 1
or
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 2
or date_trunc('month',CURRENT_DATE)=
SUBSCRIPTION_END_MONTH
then subscription_end_date::date
  else null end
  else null 
end as qsr_date
  , case when crm_account_owner like any ('%Erica Wilson%', '%Amanda Shim%' , '%Pooled%','%Matthew Wyman%') then 'Pooled' else False end as LT_Segment
  , case when DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 1
or
DATEDIFF('month',date_trunc('month',CURRENT_DATE),
SUBSCRIPTION_END_MONTH) = 2
or date_trunc('month',CURRENT_DATE)=
SUBSCRIPTION_END_MONTH then true else false end as renewal_flag,
case when PRODUCT_RATE_PLAN_NAME like any ('%Premium%','%Silver%') and subscription_end_date >= '2023-04-03' and subscription_end_date < '2024-04-03' then true else false end as 
matterhorn_impacted_renewal_flag

    FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
             LEFT JOIN COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY monthly_mart
                       ON mart_arr.DIM_SUBSCRIPTION_ID = monthly_mart.DIM_SUBSCRIPTION_ID
                           AND INSTANCE_TYPE = 'Production'
                          and monthly_mart.subscription_status = 'Active'
  left join  "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" on mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
    WHERE ARR_MONTH >= DATE_TRUNC('month',CURRENT_DATE)
      and mart_arr.subscription_status = 'Active'
  and ARR_MONTH = snapshot_month
and PRODUCT_RATE_PLAN_NAME not like '%Storage%'


