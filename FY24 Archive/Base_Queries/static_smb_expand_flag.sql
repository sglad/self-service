select
a.crm_account_owner,
a.dim_crm_account_id,
b.previous_account_owner__c,
b.previous_account_owner_email__c,
u.user_role_name as prior_owner_role
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT a
left join RAW.salesforce_stitch.account b on a.dim_crm_account_id = b.account_id_18__c
left join PROD.COMMON.DIM_CRM_USER u on u.user_email = b.previous_account_owner_email__c
where a.crm_account_owner like '%Pooled%'
and prior_owner_role like any ('%_EXP%','%_KEY%')
and a.carr_this_account > 0
