------sisense snippet (https://app.periscopedata.com/app/gitlab:safe-dashboard/snippet/Pooled-Acct-Flag/551efac86b4942868bc7e8a1e46ada74/edit) last updated 7/18/2023


SELECT 
dim_crm_account_id, 
--snapshot_date,
crm_account_owner,
CASE
    when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled'
    when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' 
     when user_role_type = 'KEY' and carr_this_account < 4000 then 'SMB Expand'
    when crm_account_owner = 'Julia Hill-Wright' and carr_this_account < 4000 then 'SMB Expand'
    when user_role_type = 'EXP' and carr_this_account < 2000 then 'SMB Expand'
    else 'False' end as Pooled_SMB_Expand_Flag,
-- CASE 
--     when user_role_type = 'KEY' and carr_this_account < 4000 then 'SMB Expand'
--     when crm_account_owner = 'Julia Hill-Wright' and carr_this_account < 4000 then 'SMB Expand'
--     when user_role_type = 'EXP' and carr_this_account < 2000 then 'SMB Expand'
--     else 'False' end as SMB_Expand_Flag, 
CASE 
    when Pooled_SMB_Expand_Flag = 'Pooled' and account_tier IS NULL then 'Rank 3'
    else account_tier end as account_tier_fy23h1
FROM  "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
where snapshot_date = '2023-07-31'
and cARR_this_account > 0 
and CRM_ACCOUNT_OWNER_GEO = 'AMER'
and CRM_ACCOUNT_OWNER_SALES_SEGMENT = 'SMB'
and PARENT_CRM_ACCOUNT_BUSINESS_UNIT = 'COMM' 

