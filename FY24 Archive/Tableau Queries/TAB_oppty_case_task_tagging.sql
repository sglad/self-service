select main_opps.*,
case when upgrades.dim_crm_opportunity_id is not null then true else false end as upgrade_flag,
upgrades.product_category as upgrade_product,
upgrades.previous_month_product_category,
case when herbie.dim_crm_account_id is not null
or
(TRX_TYPE = 'Churn' and NET_ARR > -30000 and 
(PARENT_CRM_ACCOUNT_BUSINESS_UNIT = 'ENTG' or PARENT_CRM_ACCOUNT_BUSINESS_UNIT = 'COMM')
and (CRM_ACCOUNT_EMPLOYEE_COUNT <= 100 or CRM_ACCOUNT_EMPLOYEE_COUNT is null)
and (CRM_ACCOUNT_USER_SALES_SEGMENT = 'SMB' or CRM_ACCOUNT_USER_SALES_SEGMENT = 'Mid-Market' or CRM_ACCOUNT_USER_SALES_SEGMENT = 'Large')
and main_opps.dim_crm_account_id not in
(select
distinct
dim_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = date_trunc('month',current_date)
and product_tier_name like '%Ultimate%')
)
 then true else false end as herbie_flag,
quantity.quantity,
ptp.score as ptpt_score,
ptpf.score as ptpf_score,
case_task.*

from

(
SELECT
mart_crm_opportunity.*,
user.crm_user_role_type,

--LT Segment Logic (drop for FY25)
CASE
    WHEN (dim_date.fiscal_year = 2024 and mart_crm_opportunity.crm_user_area in ('LOWTOUCHPOOL', 'EAST','WEST') and 
    user.crm_user_role_type like any ('%POOL%','%Pooled%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
    WHEN (dim_date.fiscal_year = 2024 and (mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and 
    order_type like '1%' and mart_crm_opportunity.crm_USER_region in ('AMER','East','West','EAST','WEST')) OR (opportunity_owner like '%Sales Admin%' and 
    mart_crm_opportunity.PARENT_CRM_ACCOUNT_UPA_COUNTRY in ('CA','US') and mart_crm_opportunity.CRM_USER_SALES_SEGMENT = 'SMB' and order_type like '1%')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 AND (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped in ('AMER','East','West','EAST','WEST') and (order_type = '1. New - First Order')) then 'FO'
    WHEN dim_date.fiscal_year = 2023 and ((crm_opp_owner_user_role_type_stamped not in ('Named','Expand','Territory') and CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'AMER') or (CRM_OPP_OWNER_SALES_SEGMENT_STAMPED = 'SMB' and crm_opp_owner_region_stamped = 'Sales Admin' and mart_crm_opportunity.PARENT_CRM_ACCOUNT_UPA_COUNTRY in ('CA','US') and order_type not like '1%')) and DIM_CRM_OPPORTUNITY_ID not in
( select
dim_crm_opportunity_id
from
RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
inner join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on MART_CRM_OPPORTUNITY.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
where
sales_type = 'Renewal'
and stage_name not in ('9-Unqualified','10-Duplicate','00-Pre Opportunity')
and not(MART_CRM_ACCOUNT.crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%'))
and close_date >= '2023-02-01'
and close_date < CURRENT_DATE
and not(opportunity_name like any ('%EDU Program%','%OSS Program%','%Startups Program%','%Refund%','%Debook%'))
and is_edu_oss = 0
and (opportunity_category not like '%Decom%' or opportunity_category is null)
and opportunity_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
and opportunity_owner_role not like '%EMEA%' ) then 'Pooled'
else False end as LT_segment,


dim_date.fiscal_year                     AS date_range_year,
dim_date.fiscal_quarter_name_fy          AS date_range_quarter,
DATE_TRUNC(month, dim_date.date_actual)  AS date_range_month,
dim_date.first_day_of_week               AS date_range_week,
dim_date.date_id                         AS date_range_id,
dim_date.fiscal_month_name_fy,
dim_date.fiscal_quarter_name_fy,
dim_date.fiscal_year,
dim_date.first_day_of_fiscal_quarter,
case
    when product_category like '%Self%' or PRODUCT_DETAILS like '%Self%' or product_category like '%Starter%' or PRODUCT_DETAILS like '%Starter%' then 'Self-Managed'
    when product_category like '%SaaS%' or PRODUCT_DETAILS like '%SaaS%' or product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' or product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' or product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'SaaS'
    when PRODUCT_DETAILS not like '%SaaS%' and (PRODUCT_DETAILS like '%Premium%' or PRODUCT_DETAILS like '%Ultimate%') then 'Self-Managed'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
else 'Other' end as delivery,
case
    when order_type like '3%' or order_type like '2%' then 'Growth'
    when order_type like '1%' then 'First Order'
    when order_type like '4%' or order_type like '5%' or order_type like '6%' then 'Churn / Contraction'
end as order_type_clean,
CASE when order_type like '5%' and net_arr = 0 then true else false end as partial_churn_0_narr_flag,
case
    when product_category like '%Premium%'  or PRODUCT_DETAILS like '%Premium%' then 'Premium'
    when product_category like '%Ultimate%'  or PRODUCT_DETAILS like '%Ultimate%' then 'Ultimate'
    when product_category like '%Bronze%'  or PRODUCT_DETAILS like '%Bronze%' then 'Bronze'
    when product_category like '%Starter%'  or PRODUCT_DETAILS like '%Starter%' then 'Starter'
    when product_category like '%Storage%' or PRODUCT_DETAILS like '%Storage%' then 'Storage'
    when product_category like '%Silver%'  or PRODUCT_DETAILS like '%Silver%' then 'Silver'
    when product_category like '%Gold%'  or PRODUCT_DETAILS like '%Gold%' then 'Gold'
    when product_category like 'CI%' or PRODUCT_DETAILS like 'CI%' then 'CI'
else product_category end as product_tier,
CASE
    when opportunity_name like '%QSR%' then true else false end as qsr_flag,
CASE
    when order_type like '7%' and qsr_flag = False then 'PS/CI/CD'
    when order_type like '1%' and net_arr >0  then 'First Order'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = False then 'Growth - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr =0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Flat'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr <0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type = 'Renewal' then 'Renewal - Uplift'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type <> 'Renewal' then 'Non-Renewal - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type <> 'Renewal' then 'Non-Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type = 'Renewal' then 'Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type = 'Renewal' then 'Renewal - Contraction'
    when order_type like any ('5.%','6.%') then 'Churn'
else 'Other' end as trx_type,
CASE  
    when opportunity_name like '%Startups Program%' then true else false end as startup_program_flag,

-- FO Fields:

fo_net_arr,
fo_close_date,
fo_fiscal_year,

--Churn Fields

Churn_net_arr,
Churn_close_date,
Churn_fiscal_year,

-- Acct Fields

current_customer_flag,  
account_tier,
account_tier_notes,
ptc_decile,
ptc_score,
ptc_score_group,
pte_decile,
pte_score,
pte_score_group,
curr_carr,
carr_this_account,
current_crm_account_employee_count



FROM restricted_safe_common_mart_sales.mart_crm_opportunity
LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
  left join common.dim_crm_user user on mart_crm_opportunity.dim_crm_user_id = user.dim_crm_user_id

--Links account data at the time of opportunity
  left join 
(

SELECT acct.*, 
dim_date.fiscal_year,
dim_date.fiscal_quarter_name_fy,
dim_date.last_day_of_month,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr,
CASE
    when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled'
    when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' end as LT_Segment,
case when curr_acct.dim_crm_account_id is not null then true else false end as current_pool_flag,
case when curr_acct.curr_carr > 0 then true else false end as current_customer_flag,       
curr_acct.curr_carr,
curr_acct.current_crm_account_employee_count,
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
left join
  (            
   select    
    distinct
   dim_crm_account_id,
    CARR_THIS_ACCOUNT as curr_carr,
    crm_account_employee_count as current_crm_account_employee_count
    from
              "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT"
 --  where            crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
   )        curr_acct
  on acct.dim_crm_account_id = curr_acct.dim_crm_account_id
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo on fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
       and not(product_category like '%torage%' or PRODUCT_DETAILS like '%torage%')
    ) churn on churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
WHERE dim_date.fiscal_year >= 2020

  ) acct_snap 
  on mart_crm_opportunity.DIM_CRM_ACCOUNT_ID = acct_snap.DIM_CRM_ACCOUNT_ID
and ((trx_type <> 'First Order' and mart_crm_opportunity.CLOSE_DATE - 30 = acct_snap.snapshot_date) 
 or (trx_type = 'First Order' and mart_crm_opportunity.CLOSE_DATE + 2 = acct_snap.snapshot_date)
   or (mart_crm_opportunity.CLOSE_DATE > CURRENT_DATE and acct_snap.snapshot_date = CURRENT_DATE - 1)
 )

WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
and partial_churn_0_narr_flag = false
--and fiscal_year >= 2023
--and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)

) main_opps
left join
(
SELECT
   arr_month,
  type_of_arr_change,
  arr.product_category,
  previous_month_product_category,
  opp.is_web_portal_purchase,
  arr.dim_crm_account_id, 
  opp.dim_crm_opportunity_id,
  SUM(beg_arr)                      AS beg_arr,
  SUM(end_arr)                      AS end_arr,
  SUM(end_arr) - SUM(beg_arr)       AS delta_arr,
  SUM(seat_change_arr)              AS seat_change_arr,
  SUM(price_change_arr)             AS price_change_arr,
  SUM(tier_change_arr)              AS tier_change_arr,
  SUM(beg_quantity)                 AS beg_quantity,
  SUM(end_quantity)                 AS end_quantity,
  SUM(seat_change_quantity)         AS delta_seat_change,
  COUNT(*)                          AS nbr_customers_upgrading
FROM restricted_safe_legacy.mart_delta_arr_subscription_month arr
left join 
(

SELECT
*
FROM restricted_safe_common_mart_sales.mart_crm_opportunity
WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
--and partial_churn_0_narr_flag = false
--and fiscal_year >= 2023
--and (is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = False)

)
 opp
  on (arr.DIM_CRM_ACCOUNT_ID = opp.DIM_CRM_ACCOUNT_ID
  and arr.arr_month = date_trunc('month',opp.subscription_start_date)
 -- and opp.order_type = '3. Growth'
  and opp.is_won)
WHERE 
 (ARRAY_CONTAINS('Self-Managed - Starter'::VARIANT, previous_month_product_category)
          OR ARRAY_CONTAINS('SaaS - Bronze'::VARIANT, previous_month_product_category)
       or ARRAY_CONTAINS('SaaS - Premium'::VARIANT, previous_month_product_category)
       or ARRAY_CONTAINS('Self-Managed - Premium'::VARIANT, previous_month_product_category)
       )
   AND tier_change_arr > 0
GROUP BY 1,2,3,4,5,6,7
) upgrades
on main_opps.dim_crm_opportunity_id = upgrades.dim_crm_opportunity_id

left join
  (            
   select    
    distinct
   MART_CRM_ACCOUNT.dim_crm_account_id
    from
              "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT"
left join
(
select
distinct
dim_crm_account_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR
where arr_month = date_trunc('month',current_date)
and product_tier_name like '%Ultimate%'
) ultimate
on ultimate.dim_crm_account_id = 
MART_CRM_ACCOUNT.dim_crm_account_id

   where      
(crm_account_employee_count <= 100 or crm_account_employee_count is null)
and
count_active_subscriptions > 0
and carr_this_account > 0
and carr_this_account < 30000
and parent_crm_account_business_unit in ('ENTG','COMM')
and crm_account_owner_user_segment in ('SMB','Mid-Market','Large')
and ultimate.dim_crm_account_id is null

   )       herbie
on herbie.dim_crm_account_id = main_opps.dim_crm_account_id

left join
(
select
mart_charge.dim_crm_account_id,
mart_charge.type_of_arr_change,
mart_charge.charge_amendment_type,
mart_charge.arr,
mart_charge.quantity
,
dim_subscription.dim_crm_opportunity_id
from
PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CHARGE
left join common.dim_subscription on mart_charge.dim_subscription_id = dim_subscription.dim_subscription_id
where
(previous_arr = 0 or previous_arr is null)
and mart_charge.type_of_arr_change = 'New'
and mart_charge.is_first_paid_order



) quantity
on quantity.dim_crm_account_id = main_opps.dim_crm_account_id and quantity.dim_crm_opportunity_id = main_opps.dim_crm_opportunity_id
left join
(
select * from
(
select
ptpt.*,
subs.dim_crm_account_id,
max(score_date) over(partition by dim_namespace_id) as last_score
from
PROD.WORKSPACE_DATA_SCIENCE.PTPT_SCORES ptpt
inner join PROD.COMMON.BDG_NAMESPACE_ORDER_SUBSCRIPTION subs
on subs.dim_namespace_id = ptpt.namespace_id
)
where score_date = last_score
) ptp
on ptp.dim_crm_account_id = main_opps.dim_crm_account_id
and ptp.score_date = dateadd('day',-2,main_opps.close_date)
left join
(
  select * from
(
select
ptpf.*,
subs.dim_crm_account_id,
max(score_date) over(partition by dim_namespace_id) as last_score
from
PROD.WORKSPACE_DATA_SCIENCE.PTPf_SCORES ptpf
inner join PROD.COMMON.BDG_NAMESPACE_ORDER_SUBSCRIPTION subs
on subs.dim_namespace_id = ptpf.namespace_id
)
where score_date = last_score
) ptpf
on ptpf.dim_crm_account_id = main_opps.dim_crm_account_id
and ptpf.score_date = dateadd('day',-2,main_opps.close_date)

left join
(
/*
- Pulls all Pooled Sales Case type 
- Categorizes cases by parsing the subject line
- Have to manually exclude spam cases using https://gitlab.my.salesforce.com/00O8X0000096DtA
- Trigger types last updated: 09/01/2023
*/

with 

task_data as (

-- Returns all completed Outreach tasks
-- Intended to be used mainly for understanding calls, meetings, and emails by AEs and SDRs

select * from

(select 
    task_id,
task_status,
task.dim_crm_account_id,
task.dim_crm_user_id,
task.dim_crm_person_id,
task_subject,
case when lower(task_subject) like '%email%' then 'Email'
when lower(task_subject) like '%call%' then 'Call'
when lower(task_subject) like '%linkedin%' then 'LinkedIn'
when lower(task_subject) like '%inmail%' then 'LinkedIn'
when lower(task_subject) like '%sales navigator%' then 'LinkedIn'
when lower(task_subject) like '%drift%' then 'Chat'
when lower(task_subject) like '%chat%' then 'Chat'
else
task_type end as type,
case when task_subject like '%Outreach%' and task_subject not like '%Advanced Outreach%' then 'Outreach'
  when task_subject like '%Clari%' then 'Clari'
when task_subject like '%Conversica%' then 'Conversica'
  else 'Other' end as outreach_clari_flag,
TASK_CREATED_DATE,
task_created_by_id,

--This is looking for either inbound emails (indicating they are from a customer) or completed phone calls

case when outreach_clari_flag = 'Outreach' and (task_subject like '%[Out]%' or task_subject like '%utbound%') then 'Outbound'
when outreach_clari_flag = 'Outreach' and (task_subject like '%[In]%' or task_subject like '%nbound%') then 'Inbound'
else 'Other' end as inbound_outbound_flag,
case when (inbound_outbound_flag = 'Outbound' and task_subject like '%Answered%' and task_subject not like '%Not Answer%'
and task_subject not like '%No Answer%') or (lower(task_subject) like '%call%' and task_subject not like '%Outreach%' and task_status = 'Completed' ) then true else false end as outbound_answered_flag,
task_date,
   row_number() over(partition by dim_crm_person_id order by task_date asc) as task_order,

      case when task.TASK_CREATED_BY_ID like '0054M000003Tqub%' then 'Outreach'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%GitLab Transactions%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Was Sent Email%' then 'SFDC Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Your GitLab License%' then 'Post-Purchase'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Advanced Outreach%' then 'Gainsight Marketing Email Send'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Filled Out Form%' then 'Marketo Form Fill'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Conversation in Drift%' then 'Drift'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Opened Email%' then 'Marketing Email Opened'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Sales Navigator%' then 'Sales Navigator'
      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Clari - Email%' then 'Clari Email'
      else
      'Other' end as task_type,

      user.user_name as case_user_name,
      case when user.department like '%arketin%' then 'Marketing' else user.department end as department,
      user.is_active,
      user.crm_user_sales_segment,
        user.crm_user_geo,
user.crm_user_region,
user.crm_user_area,
user.crm_user_business_unit,
      user.user_role_name--,
--  case when count(distinct case when outreach_clari_flag in ('Clari','Outreach') then task_id else null end) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id) > 1  and
--   count(distinct case when outreach_clari_flag in ('Clari','Outreach') then outreach_clari_flag else null end) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id) = 2
--   then true else false end as likely_dupe
  --,
 -- case when likely_dupe and outreach_clari_flag = 'Outreach' and account_or_opportunity_id is null then
 -- first_value(account_or_opportunity_id) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id order by outreach_clari_flag asc) else account_or_opportunity_id end as account_or_opportunity_id
      from
  prod.common_mart_sales.mart_crm_task task
     inner join common.dim_crm_user user on task.dim_crm_user_id = user.DIM_CRM_USER_ID
   where --createdbyid like '0054M000003Tqub%' and
      task.dim_crm_user_id is not null
and
      is_deleted = false
and task_date >= '2022-02-01'
and task_status = 'Completed')

where outreach_clari_flag = 'Outreach' or task_created_by_id = dim_crm_user_id
and user_role_name like any ( '%AE%','%SDR%','%BDR%')
  ),

--Baseline opportunity query

narr_data as
(
SELECT
dim_crm_opportunity_id,
dim_crm_account_id,
close_date,
CASE
    when opportunity_name like '%QSR%' then true else false end as qsr_flag,

CASE
    when order_type like '7%' and qsr_flag = False then 'PS/CI/CD'
    when order_type like '1%' and net_arr >0  then 'First Order'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = False then 'Growth - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr >0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Uplift'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr =0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Flat'
    when order_type like any ('2.%','3.%','4.%','7%') and net_arr <0 and sales_type <> 'Renewal' and qsr_flag = True then 'QSR - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr >0 and sales_type = 'Renewal' then 'Renewal - Uplift'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type <> 'Renewal' then 'Non-Renewal - Contraction'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type <> 'Renewal' then 'Non-Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr =0 and sales_type = 'Renewal' then 'Renewal - Flat'
    when order_type like any ('2.%','3.%','4.%') and net_arr <0 and sales_type = 'Renewal' then 'Renewal - Contraction'
    when order_type like any ('5.%','6.%') then 'Churn'
else 'Other' end as trx_type
from PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_OPPORTUNITY
WHERE 
((mart_crm_opportunity.is_edu_oss = 1 and net_arr > 0) or mart_crm_opportunity.is_edu_oss = 0)
AND 
mart_crm_opportunity.is_jihu_account = False
AND stage_name not like '%Duplicate%'
and (opportunity_category is null or opportunity_category not like 'Decom%')
--and partial_churn_0_narr_flag = false
--and
--(is_won or (stage_name = '8-Closed Lost' and sales_type = 'Renewal') or is_closed = false)
 ),

-- We limit to only Pooled accounts (including EMEA pool to limit query size)

/*
We pull all the Pooled cases, using the record type ID. 

We have to manually parse the Subject field to get the Trigger Type, hopefully this will go away in future iterations.

Early query iterations manually listed Spam cases to exclude given that the SFDC_CASE table is lacking the spam checkbox from SFDC.
However this is not scalable, instead we filter afterwards in Tableau based on case Status.
*/


case_data as

(select 
case 
when subject like '%Q3%' then 'Q3 AE Play'
when subject like '%Q4 Atlassian%' then 'Q4 AE Play'
when subject like '%Renewal Due to Fail%' or subject like '%Will Fail%' then 'Auto Renewal Will Fail'
when subject like '%Failed QSR%' or subject like '%Overdue QSR%' then 'Failed QSRs - 14 days overdue'
when subject like '%Overage%' and subject like '%QSR%' then 'Overage and QSR Off'
when subject like '%EoA%' then 'EoA Renewals'
when subject like '%Multi Year%' then 'Multi Year Renewals'
when subject like '%PO%' then 'PO Purchases'
when subject like '%Non Auto Renewal%' then 'Auto Renewal Turned Off'
when subject like '%Auto Renew Recently Turned Off%' then 'Auto Renewal Turned Off'
when subject like '%6sense%' then '6Sense Growth Signal'
when subject like '%Past Due Renewal%' or subject like '%Overdue Renewal%' then 'Overdue Renewals'
when subject like '%PTE%' then 'High PTE Account'
when subject like '%Likely to Downtier%' then 'Accounts likely to downtier (Paid to Free)'
when subject like '%likely to downtier%' then 'Accounts likely to downtier (Paid to Free)'
when subject like '%Upgrade%' then 'Accounts likely to upgrade to Ultimate'
when subject like '%PRO: New Overage Detected%' then 'Accounts with Overages'
when subject like '%PRO: New overage detected%' then 'Accounts with Overages'
when subject like '%Contact Sales Request%' or subject like '%Contact Us Request%' then 'Contact Sales Request'
when subject like '%NPS/CSAT%' then 'NPS/ CSAT Survey Response'
when subject like '%PTC%' then 'High PTC Account'
when subject like '%SDR%' then 'SDR Created'
when subject like '%PQL%' then 'Hand Raise PQL'
when subject like '%Support Ticket%' then 'Support Ticket'
when subject like '%Test%' then 'Internal Test'
when subject like '%New Case: %' then 'New Case:'
 when subject like '%New case: %' then 'New Case:'
 when subject like '%Upcoming SM Renewal not on 14%' then 'SM Renewal not on GL 14'
 when subject like '%Cancelled%' then 'Auto Renewal Turned Off'
 when subject like '%Under%' then 'Underutilization'
 else
 (case when subject like '%Internal transfer%' then 'Internal Transfer'
 else
 'Other' end) end as trigger_type,

sfdc_case.*
 ,
 dim_crm_user.user_name,
 dim_crm_user.department as case_department,
 dim_crm_user.team,
 dim_crm_user.manager_name,
 dim_crm_user.user_role_name as case_user_role_name,
 dim_crm_user.crm_user_role_type
from workspace_sales.sfdc_case
left join common.dim_crm_user on sfdc_case.owner_id = dim_crm_user.dim_crm_user_id
where
sfdc_case.RECORD_TYPE_ID = '0128X000001pPRkQAM'
and sfdc_case.created_date >= '2023-02-01'
--and sfdc_case.is_closed
-- and (sfdc_case.reason not like '%Spam%' or reason is null)
 and sfdc_case.CASE_NUMBER not in ('00431774','00431781','00431783','00432055','00434891','00434927','00446212','00446427','00449130','00449137','00449142','00449145','00449149','00449159','00449174','00449193','00462545')
)

select
narr_data.dim_crm_opportunity_id as oppty_id,
listagg(distinct case_data.trigger_type, ', ') within group (order by case_data.trigger_type)
--over(partition by narr_data.dim_crm_opportunity_id) 
as oppty_trigger_list,
listagg(distinct case_data.case_id, ', ')
--over(partition by narr_data.dim_crm_opportunity_id) 
as oppty_case_id_list,
listagg(distinct all_acct_cases.trigger_type, ', ') within group (order by all_acct_cases.trigger_type)
--over(partition by narr_data.dim_crm_account_id) 
as acct_trigger_list,
listagg(distinct all_acct_cases.case_id, ', ')
--over(partition by narr_data.dim_crm_account_id) 
as acct_case_id_list,
count(distinct case_data.case_id) as oppty_case_count,
count(distinct case when case_data.status = 'Closed - Resolved' or case_data.status = 'Closed' then case_data.case_id else null end) as oppty_resolved_case_count,
case when oppty_case_count > 0 then true else false end as oppty_closed_case_flag,
case when oppty_resolved_case_count > 0 then true else false end as oppty_resolved_case_flag,

count(distinct all_acct_cases.case_id) as acct_case_count,
count(distinct case when all_acct_cases.status = 'Closed - Resolved' or all_acct_cases.status = 'Closed' then all_acct_cases.case_id else null end) as acct_resolved_case_count,
case when acct_case_count > 0 then true else false end as acct_closed_case_flag,
case when acct_resolved_case_count > 0 then true else false end as acct_case_flag,

--Get stats for any task within 90 days of oppty close date
count(distinct case when acct_task.inbound_outbound_flag = 'Inbound' then acct_task.task_id else null end) as oppty_inbound_email_count,
count(distinct case when acct_task.outbound_answered_flag then acct_task.task_id else null end) as oppty_completed_call_count,
count(distinct case when acct_task.inbound_outbound_flag = 'Outbound' then acct_task.task_id else null end) as oppty_outbound_email_count,
case when oppty_inbound_email_count > 0 then true else false end as oppty_inbound_flag,
case when oppty_completed_call_count > 0 then true else false end as oppty_completed_call_flag,
case when oppty_outbound_email_count > 0 then true else false end as oppty_outbound_flag,
count(distinct acct_task.task_id) as oppty_completed_task_count,
case when oppty_completed_task_count > 0 then true else false end as oppty_task_influence_flag,

--Get stats for any task during the duration any cases were open
count(distinct case when case_task.inbound_outbound_flag = 'Inbound' then case_task.task_id else null end) as case_inbound_email_count,
count(distinct case when case_task.outbound_answered_flag then case_task.task_id else null end) as case_completed_call_count,
count(distinct case when case_task.inbound_outbound_flag = 'Outbound' then case_task.task_id else null end) as case_outbound_email_count,
case when case_inbound_email_count > 0 then true else false end as case_inbound_flag,
case when case_completed_call_count > 0 then true else false end as case_completed_call_flag,
case when case_outbound_email_count > 0 then true else false end as case_outbound_flag,
count(distinct case_task.task_id) as case_completed_task_count,
case when case_completed_task_count > 0 then true else false end as case_task_influence_flag


from
narr_data
left join case_data
on narr_data.dim_crm_account_id = case_data.account_id
and (narr_data.close_date::date >= dateadd('day',<Parameters.Oppty Close Date - Minimum Days from Case Creation>,case_data.created_date::date)
or (narr_data.trx_type like any ('%Churn%','%QSR%') and narr_data.close_date::date >= dateadd('day',<Parameters.Oppty Close Date - Minimum Days from Case Creation>-30,case_data.created_date::date)))
and narr_data.close_date::date <= dateadd('day',<Parameters.Oppty Close Date - Maximum Days from Case Creation>,case_data.closed_date::date)
and case_data.is_closed = true
left join case_data all_acct_cases
on narr_data.dim_crm_account_id = all_acct_cases.account_id
and all_acct_cases.is_closed = true
left join task_data acct_task
on acct_task.dim_crm_account_id = narr_data.dim_crm_account_id
and acct_task.task_date::date >= dateadd('day',-<Parameters.Oppty Close Date - Tasks X Days Before>,narr_data.close_date::date)
and acct_task.task_date::date <= narr_data.close_date::date
left join task_data case_task
on case_task.dim_crm_account_id = case_data.account_id
and case_task.task_date::date >= case_data.created_date::date
and case_task.task_date::date <= case_data.closed_date::date
group by 1

) case_task
on case_task.oppty_id = main_opps.dim_crm_opportunity_id
