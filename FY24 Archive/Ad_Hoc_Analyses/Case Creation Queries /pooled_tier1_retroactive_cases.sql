
------export CSV for Mike and determine timeline 


with pooled_tier1 as (
SELECT *
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
WHERE acct.snapshot_date = '2023-07-31'
and cARR_this_account > 0 
and CRM_ACCOUNT_OWNER_GEO = 'AMER'
and CRM_ACCOUNT_OWNER_SALES_SEGMENT = 'SMB'
and PARENT_CRM_ACCOUNT_BUSINESS_UNIT = 'COMM'
AND account_owner like any ('Pooled%', 'Amanda Shim', 'Erica Wilson', 'Matthew Wyman%') 
AND account_tier in ('Rank 1', 'Rank 1.5')
),



exclude_purchase as (
SELECT 
acct.dim_crm_account_id, 
  o.close_date, 
  o.stage_name, 
  o.net_arr 
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o
  ON o.dim_crm_account_id = acct.dim_crm_account_id
WHERE acct.snapshot_date = '2023-07-31'
and acct.cARR_this_account > 0 
and acct.CRM_ACCOUNT_OWNER_GEO = 'AMER'
and acct.CRM_ACCOUNT_OWNER_SALES_SEGMENT = 'SMB'
and acct.PARENT_CRM_ACCOUNT_BUSINESS_UNIT = 'COMM'
AND account_owner like any ('Pooled%', 'Amanda Shim', 'Erica Wilson', 'Matthew Wyman%') 
AND account_tier in ('Rank 1', 'Rank 1.5')
AND ((o.IS_CLOSED_WON = TRUE and o.close_date between '2023-06-10' and '2023-08-09'))
), 

//exclude_touch as (
//select t.*
//  FROM
//
//(select 
//    task_id,
//task_status,
//task.dim_crm_account_id,
//task.dim_crm_user_id,
//task.dim_crm_person_id,
//task_subject,
//case when lower(task_subject) like '%email%' then 'Email'
//when lower(task_subject) like '%call%' then 'Call'
//when lower(task_subject) like '%linkedin%' then 'LinkedIn'
//when lower(task_subject) like '%inmail%' then 'LinkedIn'
//when lower(task_subject) like '%sales navigator%' then 'LinkedIn'
//when lower(task_subject) like '%drift%' then 'Chat'
//when lower(task_subject) like '%chat%' then 'Chat'
//else
//task_type end as type,
//case when task_subject like '%Outreach%' and task_subject not like '%Advanced Outreach%' then 'Outreach'
//  when task_subject like '%Clari%' then 'Clari'
//when task_subject like '%Conversica%' then 'Conversica'
//  else 'Other' end as outreach_clari_flag,
//TASK_CREATED_DATE,
//case when outreach_clari_flag = 'Outreach' and (task_subject like '%[Out]%' or task_subject like '%utbound%') then 'Outbound'
//when outreach_clari_flag = 'Outreach' and (task_subject like '%[In]%' or task_subject like '%nbound%') then 'Inbound'
//else 'Other' end as inbound_outbound_flag,
//case when inbound_outbound_flag = 'Outbound' and task_subject like '%Answered%' and task_subject not like '%Not Answer%'
//and task_subject not like '%No Answer%' then true else false end as outbound_answered_flag,
//task_date,
//   row_number() over(partition by dim_crm_person_id order by task_date asc) as task_order,
//
//      case when task.TASK_CREATED_BY_ID like '0054M000003Tqub%' then 'Outreach'
//      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%GitLab Transactions%' then 'Post-Purchase'
//      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Was Sent Email%' then 'SFDC Marketing Email Send'
//      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Your GitLab License%' then 'Post-Purchase'
//      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Advanced Outreach%' then 'Gainsight Marketing Email Send'
//      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Filled Out Form%' then 'Marketo Form Fill'
//      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Conversation in Drift%' then 'Drift'
//      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Opened Email%' then 'Marketing Email Opened'
//      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Sales Navigator%' then 'Sales Navigator'
//      when task.TASK_CREATED_BY_ID not like '0054M000003Tqub%' and task_subject like '%Clari - Email%' then 'Clari Email'
//      else
//      'Other' end as task_type,
//
//      user.user_name,
//      case when user.department like '%arketin%' then 'Marketing' else user.department end as department,
//      user.is_active,
//      user.crm_user_sales_segment,
//      user.user_role_name,
// case when count(distinct case when outreach_clari_flag in ('Clari','Outreach') then task_id else null end) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id) > 1  and
//  count(distinct case when outreach_clari_flag in ('Clari','Outreach') then outreach_clari_flag else null end) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id) = 2
//  then true else false end as likely_dupe
//  --,
// -- case when likely_dupe and outreach_clari_flag = 'Outreach' and account_or_opportunity_id is null then
// -- first_value(account_or_opportunity_id) over(partition by dim_crm_person_id,department,task_date,task.dim_crm_user_id order by outreach_clari_flag asc) else account_or_opportunity_id end as account_or_opportunity_id
//      from
//  prod.common_mart_sales.mart_crm_task task
//     inner join common.dim_crm_user user on task.dim_crm_user_id = user.DIM_CRM_USER_ID
//   where --createdbyid like '0054M000003Tqub%' and
//      task.dim_crm_user_id is not null
//and
//      is_deleted = false
//and task_date >= '2021-02-01'
//and task_status = 'Completed') t
//LEFT JOIN expand_accounts e
//  ON t.dim_crm_account_id = e.dim_crm_account_id
//where outreach_clari_flag = 'Outreach'
//and e.dim_crm_account_id IS NOT NULL
//and datediff('day', task_date, current_date) <= 90
//and ((OUTBOUND_ANSWERED_FLAG = True and contains(USER_ROLE_NAME,'AE')) OR (INBOUND_OUTBOUND_FLAG = 'Inbound' and Type = 'Email' and contains(USER_ROLE_NAME,'AE')))
//--AND (INBOUND_OUTBOUND_FLAG = 'Inbound' and Type = 'Email' and contains(USER_ROLE_NAME,'AE'))
//), 
eoa_renewals 
as 
(SELECT 
 '00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN,
--'Urgent Renewal & TRX Support' as TYPE,
'SMB Pool' as TYPE, 
CONCAT('EoA Renewal ', subscription_end_month) as CASE_SUBJECT, 
'Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY 
 FROM (
SELECT 
        mart_arr.ARR_MONTH
         , mart_arr.SUBSCRIPTION_END_MONTH
         , mart_arr.DIM_CRM_ACCOUNT_ID
         , mart_arr.CRM_ACCOUNT_NAME
         , mart_arr.DIM_SUBSCRIPTION_ID
         , mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
         , mart_arr.SUBSCRIPTION_NAME
           , mart_arr.subscription_sales_type
         , mart_arr.AUTO_PAY
         , mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
         , mart_arr.CONTRACT_AUTO_RENEWAL
         , mart_arr.TURN_ON_AUTO_RENEWAL
         , mart_arr.TURN_ON_CLOUD_LICENSING
         , mart_arr.CONTRACT_SEAT_RECONCILIATION
         , mart_arr.TURN_ON_SEAT_RECONCILIATION
         , case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
         , mart_arr.PRODUCT_TIER_NAME
         , mart_arr.PRODUCT_DELIVERY_TYPE
         , mart_arr.PRODUCT_RATE_PLAN_NAME
         , mart_arr.ARR
         , CASE when (mart_arr.ARR / mart_arr.QUANTITY > 107 and mart_arr.ARR / mart_arr.QUANTITY < 110) then 108 else (mart_arr.ARR / mart_arr.QUANTITY) end as price    
  FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
  JOIN pooled_tier1 e
    ON mart_arr.dim_crm_account_id = e.dim_crm_account_id 
  WHERE ARR_MONTH >= DATE_TRUNC('month',date('2022-02-01'))
  and mart_arr.subscription_status = 'Active'
  and PRODUCT_TIER_NAME not like '%Storage%'
  and price in (108,180)
  and arr_month = '2023-08-01'
 and mart_arr.SUBSCRIPTION_END_MONTH <= '2023-10-01'
  )),


//eoa_renewals as (
//SELECT 
//'00G8X000006WmU3' as OWNER_ID, 
//'Open' as STATUS,
//a.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
//'SMB Pool Internal Created' as CASE_ORIGIN,
//--'Urgent Renewal & TRX Support' as TYPE,
//'SMB Pool' as TYPE, 
//CONCAT('EoA Renewal ', close_date) as CASE_SUBJECT, 
//'Renewal' as CASE_REASON, 
//'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
//'Medium' as PRIORITY 
//FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
//JOIN expand_accounts a
//    ON o.dim_crm_account_id = a.dim_crm_account_id
//WHERE sales_type = 'Renewal'
//AND (opportunity_name like any ('%Starter%', '%Bronze%') OR products_purchased like any ('%Starter%', '%Bronze%'))
//AND next_steps like any ('EOA%', '%EOA%', '%eoa%', 'EOA%')
//AND is_closed = False
//AND o.close_date >= '2023-08-10'
//AND o.close_date <= '2023-10-08'),


----qsr oppty that has a qsr status of failed and oppty still open
failed_qsr as (SELECT
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Churn & Contraction Mitigation' as TYPE,
'SMB Pool' as TYPE, 
CONCAT('Overdue QSR - ', close_date) as CASE_SUBJECT, 
'Adding Licenses/Purchasing Help' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Low' as PRIORITY
FROM(
SELECT o.*, 
CASE WHEN opportunity_name like '%QSR%' then true else false end as qsr_flag, 
datediff('day', created_date, current_date)+1 as age
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN pooled_tier1 a
ON  o.dim_crm_account_id = a.dim_crm_account_id
WHERE qsr_flag = true
and qsr_status = 'Failed'
and is_closed = False)),



multiyear_renewals as (SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Urget Renewal & TRX Support' as TYPE,
'SMB Pool' as TYPE, 
CONCAT('Multiyear Renewal - ', close_date) as CASE_SUBJECT,  
'Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN pooled_tier1 a
ON o.dim_crm_account_id = a.dim_crm_account_id
WHERE opportunity_term > 12
and (opportunity_name like '%2 year%' OR opportunity_name like '%3 year%')
and is_closed = False
AND o.close_date >= '2023-08-10'
AND o.close_date <= '2023-10-08'),




po_renewals as (SELECT
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Urgent Renewal & TRX Support' as TYPE,
'SMB Pool' as TYPE, 
CONCAT('PO Required Renewal - ', close_date) as CASE_SUBJECT, 
'Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN pooled_tier1 a
ON o.dim_crm_account_id = a.dim_crm_account_id
JOIN PROD.common.dim_billing_account b
ON a.dim_crm_account_id = b.dim_crm_account_id
WHERE is_closed = False
and PO_required = 'YES'
AND o.close_date >= '2023-08-10'
AND o.close_date <= '2023-10-08'),






overdue_renewals as (SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Urgent Renewal & TRX Support' as TYPE,
'SMB Pool' as TYPE, 
CONCAT('Overdue Renewal - ', close_date) as CASE_SUBJECT,  
'Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY 
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN pooled_tier1 a
ON o.dim_crm_account_id = a.dim_crm_account_id
WHERE close_date >= '2023-06-10' 
AND close_date <= '2023-08-08'
AND is_closed = False
and sales_type = 'Renewal'
AND NOT(opportunity_name LIKE ANY ('%storage%', '%Storage%'))
AND NOT (opportunity_name LIKE ANY ('%Refund%', '%refund%', '%debook%', '%Debook%'))),

 
 
 
failed_autorenewals as (SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
'Urgent Renewal & TRX Support' as TYPE,
CONCAT('Auto-Renewal Will Fail - ', close_date) as CASE_SUBJECT,  
'Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN pooled_tier1 a
ON o.dim_crm_account_id = a.dim_crm_account_id
where is_closed = False
AND (auto_renewal_status != ''AND auto_renewal_status not in ('On', 'Off'))
AND o.close_date >= '2023-08-10'
AND o.close_date <= '2023-10-08'
 ), 


monthly_overages as (
select
    DISTINCT
    snapshot_month,
    PING_CREATED_AT::date as ping_created_at,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    DIM_CRM_ACCOUNT_ID,
    INSTANCE_TYPE,
   delivery_type,
    subscription_status,
    BILLABLE_USER_COUNT,
    LICENSE_USER_COUNT,
    subscription_start_date,
    subscription_end_date, 
    BILLABLE_USER_COUNT - LICENSE_USER_COUNT AS overage_count
//  max(snapshot_month) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month, 
//  min(snapshot_month) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as earliest_overage_month
  from PROD.pumps.pump_gainsight_metrics_monthly_paid
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'
    --and date_trunc('month', PING_CREATED_AT::date) = date_trunc('month', current_date) 
    and PING_CREATED_AT::date >= '2023-07-01'
    AND PING_CREATED_AT::date <=  current_date
   and snapshot_month >= '2023-07-01'
    and overage_count > 0
),
   
   
overage_amount as (
SELECT o.*,
    mart_arr.ARR / mart_arr.QUANTITY as price,
  case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag,
  max(snapshot_month) over(partition by o.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month, 
  overage_count * (mart_arr.ARR / mart_arr.QUANTITY) as overage_amount
FROM monthly_overages o
  LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
ON o.DIM_SUBSCRIPTION_ID_ORIGINAL = mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
AND o.snapshot_month = mart_arr.arr_month
WHERE ((delivery_type = 'SaaS' and overage_amount > 0) OR (delivery_type = 'Self-Managed' and overage_amount > 1000))
and qsr_enabled_flag = False
qualify snapshot_month = latest_overage_month
),

    

final_overage as 
(SELECT distinct
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
'Expansion Opportunity' as TYPE,
('Overage and QSR Off') as CASE_SUBJECT, 
'Adding Licenses/Purchasing Help' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Low' as PRIORITY
FROM overage_amount o
JOIN pooled_tier1 a
ON o.dim_crm_account_id = a.dim_crm_account_id),
//JOIN common.dim_subscription sub
//ON o.dim_crm_account_id = sub.dim_crm_account_id 
//WHERE sub.turn_on_seat_reconciliation = 'No'),
 


renew_data as
(select
*
from
(
  select 
date_actual as snapshot_date,
PREP_BILLING_ACCOUNT_USER.user_name as update_user,
PREP_BILLING_ACCOUNT_USER.IS_INTEGRATION_USER,
TURN_ON_AUTO_RENEWAL as active_autorenew_status,
lag(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as prior_auto_renewal,
lead(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as future_auto_renewal,
sub.*,
  acct.crm_account_owner,
  acct.carr_this_account,
  acct.account_tier_calculated as account_tier
from common.dim_subscription_snapshot_bottom_up sub
inner join common.dim_date on sub.snapshot_id = dim_date.date_id
inner join restricted_safe_common_prep.PREP_BILLING_ACCOUNT_USER on sub.UPDATED_BY_ID = PREP_BILLING_ACCOUNT_USER.ZUORA_USER_ID
 inner join 
  (
    select
    snapshot_date as acct_snapshot_date,
    crm_account_owner,
    dim_crm_account_id,
    carr_this_account,
    parent_crm_account_area,
    account_tier_calculated
    from
  (

with acct_base as (SELECT acct.*, 
dim_date.fiscal_year,
dim_date.fiscal_quarter_name_fy,
dim_date.last_day_of_month,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr,
CASE
    when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled'
    when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' end as LT_Segment,
case when curr_acct.dim_crm_account_id is not null then true else false end as current_pool_flag,
case when curr_acct.curr_carr > 0 then true else false end as current_customer_flag,          
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
left join
  (            
   select    
    distinct
   dim_crm_account_id,
    CARR_THIS_ACCOUNT as curr_carr
    from
              pooled_tier1
   )        curr_acct
  on acct.dim_crm_account_id = curr_acct.dim_crm_account_id
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo on fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
    and not(product_category like '%torage%' or PRODUCT_DETAILS like '%torage%')
    ) churn on churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
WHERE dim_date.fiscal_year >= 2023
              ),

calc as (SELECT b.*, 
case when b.account_tier in ('Rank 1','Rank 1.5') and (b.churn_close_date is null or b.churn_close_date > b.SNAPSHOT_date) then 1
when b.account_tier_notes not like '%Ops%' and b.snapshot_date >= '2023-03-01' then 
        case when b.account_tier = 'Rank 2' then 2
         when b.account_tier = 'Rank 3' then 3
         else 1 end
else
calc.account_tier_calculated end as account_tier_calculated
FROM acct_base b
LEFT JOIN (SELECT
      dim_crm_account_id,
           snapshot_date as calc_snapshot_date,
        2964 as carr_tier_1,
        2400 as carr_tier_2,
        1916 as carr_tier_3,
        134985000 as funding_tier_1,
        44909650 as funding_tier_2,
        20624700 as funding_tier_3,
        103 as dev_count_tier_1,
        44 as dev_count_tier_2,
        29 as dev_count_tier_3,
        CASE
        WHEN (CARR_THIS_ACCOUNT >= CARR_TIER_1 OR LAM_DEV_COUNT >= DEV_COUNT_TIER_1 OR TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_1) then 1 else 0 end as top_tier_1,
        CASE
        WHEN
        (((CARR_THIS_ACCOUNT >= CARR_TIER_2 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_2)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
        OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))) then 1 else 0 end as top_tier_2,
        CASE
        WHEN
        (((CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR  (LAM_DEV_COUNT >= DEV_COUNT_TIER_3  AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))
        AND (top_tier_2 = 0)) then 1 else 0 end as top_tier_3 ,
        case when top_tier_1 > 0 or top_tier_2 >0 or top_tier_3 > 0 then 1
        else
        case when carr_this_account < 1500 and lam_dev_count < 25 then 3
        else 2 end
        end as account_tier_calculated 
     from acct_base
      where --lt_segment = 'Pooled'
     -- and
           fiscal_year = 2024) calc
      on calc.dim_crm_account_id = b.dim_crm_account_id
      and calc.calc_snapshot_date = b.snapshot_date) 
 SELECT calc.*, 
 first_value(account_tier_calculated) over(partition by dim_crm_account_id, date_trunc('month', snapshot_date) order by snapshot_date asc) as initial_tier,
LAST_VALUE(account_tier_calculated) over(partition by dim_crm_account_id, date_trunc('month', snapshot_date) order by snapshot_date asc) as final_tier,
case when initial_tier = 1 and final_tier > 1 then 1 else 0 end as downtier_flag,
case when initial_tier > 1 and final_tier = 1 then 1 else 0 end as uptier_flag  
 FROM calc
    )
    where snapshot_date = CURRENT_DATE - 1
    and LT_segment = 'Pooled'
    ) acct 
  on sub.dim_crm_account_id = acct.dim_crm_account_id
where snapshot_date >= '2023-02-01'
  and snapshot_date < CURRENT_DATE
  and subscription_status = 'Active'
  and update_user = 'svc_zuora_fulfillment_int@gitlab.com'
--and (TURN_ON_AUTO_RENEWAL = 'Yes' or TURN_ON_AUTO_RENEWAL is null)
order by snapshot_date asc
)
where
(active_autorenew_status = 'No' and prior_auto_renewal = 'Yes')
 ),
 
 
 current_sub as
(
 select
  subscription_name,
  DIM_SUBSCRIPTION_ID,
  subscription_version,
  subscription_status,
  TURN_ON_AUTO_RENEWAL,
  max(subscription_version) over(partition by subscription_name) as latest_version
  from
  common.dim_subscription
  where subscription_status = 'Active'
  and term_start_date >= '2022-02-01'
  qualify subscription_version = latest_version
  order by subscription_name
), 


autorenew_summary as
(
select
current_sub.TURN_ON_AUTO_RENEWAL as current_autorenew_status,
renew_data.*,
oppty.close_date
from
renew_data
inner join current_sub on renew_data.DIM_SUBSCRIPTION_ID = current_sub.DIM_SUBSCRIPTION_ID
left join "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" oppty on renew_data.dim_crm_account_id = oppty.dim_crm_account_id
  and renew_data.snapshot_date < oppty.close_date
  and oppty.sales_type = 'Renewal'
  and oppty.is_closed = False
where current_autorenew_status <> 'Yes'  
  ), 
  
  
  ------get this as soon as it flips from yes to no ----need to do initial retroactive run
  autorenew_off as (
  select distinct
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Churn & Contraction Mitigation' as TYPE,
'SMB Pool' as TYPE,
'Customer Recently Cancelled' as CASE_SUBJECT, 
'Licensing/Fulfillment' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY 
FROM autorenew_summary
where 
snapshot_date >= '2023-06-10'
and snapshot_date <= '2023-08-09'
  ),
  
//  underutilization_cases as (
//SELECT 
//account_id
//FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
//WHERE record_type_id in ('0128X000001pPRkQAM')
//AND subject like ('Underutilization%')
//AND datediff('day', created_date, current_date) <= 90
//), 

  arr_data 
as 
(
SELECT 
  mart_arr.ARR_MONTH,
  monthly_mart.snapshot_month 
          , ping_created_at 
         , mart_arr.SUBSCRIPTION_END_MONTH
         , mart_arr.DIM_CRM_ACCOUNT_ID
         , mart_arr.CRM_ACCOUNT_NAME
        , MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
         , mart_arr.DIM_SUBSCRIPTION_ID
         , mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
         , mart_arr.SUBSCRIPTION_NAME
           , mart_arr.subscription_sales_type
         , mart_arr.AUTO_PAY
         , mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
         , mart_arr.CONTRACT_AUTO_RENEWAL
         , mart_arr.TURN_ON_AUTO_RENEWAL
         , mart_arr.TURN_ON_CLOUD_LICENSING
         , mart_arr.CONTRACT_SEAT_RECONCILIATION
         , mart_arr.TURN_ON_SEAT_RECONCILIATION
         , case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
         , mart_arr.PRODUCT_TIER_NAME
         , mart_arr.PRODUCT_DELIVERY_TYPE
         , mart_arr.PRODUCT_RATE_PLAN_NAME
         , mart_arr.ARR
         , mart_arr.ARR / mart_arr.QUANTITY as price
--     ,mart_arr.QUANTITY
--          , monthly_mart.PING_CREATED_AT
--          , monthly_mart.HOSTNAME
--          , monthly_mart.DIM_NAMESPACE_ID
         , monthly_mart.BILLABLE_USER_COUNT
         , monthly_mart.LICENSE_USER_COUNT
         , monthly_mart.subscription_start_date
         , monthly_mart.subscription_end_date
  , monthly_mart.term_end_date
         , monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
          , (mart_arr.ARR / mart_arr.QUANTITY)*(monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as underutilization_amount
        , max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_underutilization_month
    FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
  left join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
             LEFT JOIN 
  (
    select
    DISTINCT
    snapshot_month,
    ping_created_at,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    INSTANCE_TYPE,
    subscription_status,
    BILLABLE_USER_COUNT,
    LICENSE_USER_COUNT,
    subscription_start_date,
    subscription_end_date,
    term_end_date
    from
  COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'
    and PING_CREATED_AT::date >= '2023-07-01'
    and PING_CREATED_AT::date >= '2023-07-31'
  ) monthly_mart
  ON mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL = monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL
    WHERE ARR_MONTH >= DATE_TRUNC('month',date('2023-02-01'))
   --   and lower(user_role_name) like 'pooled-smb-system%'
 --     and mart_arr.subscription_status = 'Active'
  and ARR_MONTH = snapshot_month
  and ARR_MONTH = '2023-07-01'
  and PRODUCT_TIER_NAME not like '%Storage%'
      and overage_count < 0
  and monthly_mart.billable_user_count >= 1
--  and qsr_enabled_flag
  ),
  
  underutilization_top as 
(SELECT arr_data.*, 
 o.close_date, 
 o.sales_type, 
 datediff('day', current_date, o.close_date)
 from arr_data 
JOIN pooled_tier1 e
ON arr_data.dim_crm_account_id = e.dim_crm_account_id
LEFT JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
ON arr_data.dim_crm_account_id = o.dim_crm_account_id
 AND sales_type = 'Renewal' 
 AND is_closed = False
 --and o.close_date >= current_date 
 and o.close_date < '2024-07-01'
WHERE datediff('day', '2023-07-01', o.close_date) >= 120
ORDER BY underutilization_amount 
LIMIT 30),

   
----pulls once a month and get the top 30 accounts underutilizing by underutilization amount
underutilization as (
select
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Churn & Contraction Mitigation' as TYPE,
'SMB Pool' as TYPE,
CONCAT('Underutilization') as CASE_SUBJECT, 
'Licensing/ Fulfillment' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY 
from underutilization_top), 




high_ptc_score as (SELECT distinct
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Churn & Contraction Mitigation' as TYPE,
'SMB Pool' as TYPE, 
'High PTC Score' as CASE_SUBJECT, 
'Sales Evaluation (Non-Renewal)' as CASE_REASON,
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Low' as PRIORITY
FROM (
  SELECT distinct dim_crm_account_id, 
  carr_this_account 
  FROM pooled_tier1 a
JOIN "PROD"."WORKSPACE_DATA_SCIENCE"."PTC_SCORES" ptc
ON ptc.crm_account_id = a.dim_crm_account_id
where carr_this_account >0 
and score > .5
AND score_date >= '2023-06-02'
AND score_date <= '2023-08-01'
AND dim_crm_account_id not in (SELECT dim_crm_account_id FROM exclude_purchase) 
AND dim_crm_account_id not in ('0014M00001imFXr',  '0016100001b9FTj', '0014M00001wUqRo', '0016100001W0DmI')
AND dim_crm_account_id not in (SELECT account_id from autorenew_off)
AND dim_crm_account_id not in (SELECT account_id from failed_autorenewals)
AND dim_crm_account_id not in (SELECT account_id from underutilization)
ORDER BY a.carr_this_account desc
LIMIT 30)),


high_pte_score as (SELECT distinct
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Expansion' as TYPE,
'SMB Pool' as TYPE, 
'High PTE Score' as CASE_SUBJECT, 
'New High Engagement Account - 5* PTE' as CASE_REASON,
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Low' as PRIORITY
FROM pooled_tier1 a
JOIN "PROD"."WORKSPACE_DATA_SCIENCE"."PTE_SCORES"  pte 
    ON a.dim_crm_account_id = pte.crm_account_id 
WHERE carr_this_account > 0 
AND pte.score_group in (4,5) 
AND score_date >= '2023-07-01'
AND score_date <= '2023-08-09'
AND dim_crm_account_id not in (SELECT account_id from autorenew_off)
AND dim_crm_account_id not in (SELECT account_id from failed_autorenewals)
AND dim_crm_account_id not in (SELECT account_id from underutilization)
AND dim_crm_account_id not in (SELECT account_id from final_overage)
AND dim_crm_account_id not in (SELECT account_id from failed_qsr)
AND dim_crm_account_id not in (SELECT account_id from high_ptc_score)
AND dim_crm_account_id not in (SELECT account_id from overdue_renewals)
  ),



all_cases as (                
SELECT * FROM high_pte_score
UNION ALL 
SELECT * FROM high_ptc_score
UNION ALL 
SELECT * FROM eoa_renewals
UNION ALL 
SELECT * FROM failed_qsr
UNION ALL 
SELECT * FROM multiyear_renewals
UNION ALL 
SELECT * FROM PO_renewals
UNION ALL 
SELECT * FROM overdue_renewals
UNION ALL 
SELECT * FROM failed_autorenewals
UNION ALL 
SELECT * FROM final_overage
UNION ALL 
SELECT * FROM autorenew_off
UNION ALL 
SELECT * FROM underutilization
)

 
SELECT 
*
FROM all_cases
WHERE account_id not in (SELECT dim_crm_account_id from exclude_purchase)
and account_id not in ('0014M00001imFXr',  '0016100001b9FTj', '0014M00001wUqRo', '0016100001W0DmI', '0014M00001ikvBp')




