--------------WIP----------------
-----need to add 'next steps' field to snowflake and update query
SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
a.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, --hardcoded this case type to align with other internal created cases (this can be changed/edited in the code)
'Urgent Renewal & TRX Support' as TYPE,
CONCAT('EoA Renewal ', close_date) as CASE_SUBJECT, 
'EoA Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, --hardcoding 'Pooled Sales Case' as record type
'H/M/L' as PRIORITY 
----'Xyz' as CASE_REASON_SUBTYPE 
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" a
    ON o.dim_crm_account_id = a.dim_crm_account_id
WHERE sales_type = 'Renewal'
AND (opportunity_name like any ('%Starter%', '%Bronze%') OR products_purchased like any ('%Starter%', '%Bronze%'))
----need to add in "next steps" ---- possible but not available 
AND CRM_ACCOUNT_OWNER_AREA = 'LOWTOUCH'
AND USER_ROLE_TYPE = 'POOL' 
AND account_tier not in ('Rank 1', 'Rank 1.5')
AND o.close_date >= '2023-02-01'
AND is_closed = False
---and 'next steps' contain 'EOA'