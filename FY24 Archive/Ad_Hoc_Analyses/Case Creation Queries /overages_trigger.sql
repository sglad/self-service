----create temporary table to get the window between last case creation date and the day we are running the csv
-- drop table if exists last_create_date;
-- create temporary table last_create_date as (
-- SELECT 
-- MAX(created_date) as last_case_creation_date
-- FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
-- WHERE record_type_id in ('0128X000001pPRkQAM')
-- AND case_type != 'Inbound Request'
-- ORDER BY created_date desc);

-----gets dim_crm_account_id for all overage cases that already exist 
drop table if exists overage_cases; 
create temporary table overage_cases as (
SELECT 
account_id
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE record_type_id in ('0128X000001pPRkQAM')
AND subject like ('Overage%')
);

-----pulls all overages for the current month 
DROP TABLE IF EXISTS monthly_overages; 
create temporary table monthly_overages as (
select
    DISTINCT
    snapshot_month,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    DIM_CRM_ACCOUNT_ID,
    INSTANCE_TYPE,
    subscription_status,
    BILLABLE_USER_COUNT,
    LICENSE_USER_COUNT,
    subscription_start_date,
    subscription_end_date, 
    BILLABLE_USER_COUNT - LICENSE_USER_COUNT AS overage_count
//  max(snapshot_month) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month, 
//  min(snapshot_month) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as earliest_overage_month
   from PROD.pumps.pump_gainsight_metrics_monthly_paid
    --FROM COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'
    and date_trunc('month', PING_CREATED_AT::date) = date_trunc('month', current_date) 
    and overage_count > 0);


-----gets pooled accounts
drop table if exists pooled_accts;
create temporary table pooled_accts as
(
SELECT *, 
'Pooled' as LT_segment
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT"
  WHERE owner_role = 'SystemUser_COMM_AMER_SMB_AMER_LOWTOUCH_POOL'
  and account_tier not in ('Rank 1', 'Rank 1.5')
);



------joins monthly overages to pooled accounts and also to the subscription (to get QSR disabled filter) and also removes all accounts that have an overage case that exists 
SELECT * 
FROM monthly_overages o
JOIN pooled_accts p
ON o.dim_crm_account_id = p.dim_crm_account_id
JOIN common.dim_subscription sub
ON o.dim_crm_account_id = sub.dim_crm_account_id 
WHERE o.dim_crm_account_id not in (SELECT * from overage_cases)
AND sub.turn_on_seat_reconciliation = 'No'
-----add dim_subscription table to get the seat reconcilation flag

