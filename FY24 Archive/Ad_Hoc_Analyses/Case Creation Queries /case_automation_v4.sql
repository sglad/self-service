SELECT 
a.dim_crm_account_id as account_id, 
a.account_owner,
a.USER_ROLE_TYPE, 
a.CRM_ACCOUNT_OWNER_GEO,
a.PARENT_CRM_ACCOUNT_SALES_SEGMENT, 
a.PARENT_CRM_ACCOUNT_BUSINESS_UNIT, 
a.NEXT_RENEWAL_DATE, 
o.dim_crm_opportunity_id as opportunity_id,
o.is_closed, 
o.sales_type, 
o.close_date, 
o.opportunity_term, 
o.opportunity_name,
o.QSR_status,
s.subscription_status, 
s.term_end_date as current_subscription_end_date,
DATEDIFF('day', s.term_end_date, current_date) as days_since_current_sub_end_date,
s.dim_subscription_id,
DATEDIFF('day', current_date, o.close_date) as days_till_close,
CASE WHEN o.opportunity_name LIKE '%QSR%' THEN TRUE ELSE FALSE END AS QSR_Flag,
CASE WHEN o.sales_type = 'Renewal' THEN TRUE ELSE FALSE END AS Renewal_Flag,
b.PO_required,
CASE WHEN PO_REQUIRED = 'YES' then TRUE ELSE FALSE END AS PO_REQUIRED_Flag, 
o.auto_renewal_status,
CASE WHEN (auto_renewal_status like '%EoA%' or auto_renewal_status like 'EoA%') THEN 'EoA - Autorenewal will Fail'
     WHEN (auto_renewal_status != NULL or auto_renewal_status not in ('On', 'Off')) THEN 'Autorenewal Will Fail'
     ELSE FALSE END AS Auto_Renewal_Will_Fail_Flag
FROM PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT a
LEFT JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o
    ON a.dim_crm_account_id = o.dim_crm_account_id
    AND o.is_closed = False
    AND DATEDIFF('day', current_date, o.close_date) <= 365
LEFT JOIN PROD.COMMON.DIM_SUBSCRIPTION s
    ON o.dim_crm_opportunity_id = s.dim_crm_opportunity_id_current_open_renewal
LEFT JOIN (SELECT distinct dim_crm_account_id, PO_required FROM PROD.common.dim_billing_account WHERE PO_REQUIRED = 'YES') b 
    ON b.dim_crm_account_id = a.dim_crm_account_id
-- LEFT JOIN PROD.common.dim_billing_account b
--     ON a.dim_crm_account_id = b.dim_crm_account_id
WHERE user_role_type = 'POOL' 
AND crm_account_owner_geo in ('AMER', 'EMEA') 
AND a.PARENT_CRM_ACCOUNT_SALES_SEGMENT = 'SMB'








