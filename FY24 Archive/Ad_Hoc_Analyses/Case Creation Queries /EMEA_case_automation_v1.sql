with last_create_date as (
SELECT 
MAX(created_date) as last_case_creation_date 
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE record_type_id in ('0128X000001pPRkQAM')
AND case_type != 'Inbound Request'
AND subject not in  ('HR: Contact Sales Request', 'Contact Sales Request', 'HR: Hand Raise PQL', 'Hand Raise PQL', 'NPS/CSAT Survey Response', 'HR: NPS/CSAT Survey Response', 'Support Ticket')
and account_owner in ('François Xavier Debroux', 'Nga Nguyen', 'Ope Obakin', 'Raphael Werner')
ORDER BY created_date desc
LIMIT 1),

EMEA_accounts as (
SELECT * 
FROM PROD.RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT
where USER_ROLE_TYPE = 'POOL'
and CRM_ACCOUNT_OWNER_GEO = 'EMEA'
and PARENT_CRM_ACCOUNT_SALES_SEGMENT = 'SMB'
and PARENT_CRM_ACCOUNT_BUSINESS_UNIT = 'COMM'
), 

exclude_purchase as (
SELECT 
  a.dim_crm_account_id as account_id
  FROM EMEA_accounts a
  JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o
  ON a.dim_crm_account_id = o.dim_crm_account_id
WHERE ((o.is_closed_won = True AND datediff('day', o.close_date, current_date) <= 60) OR (o.stage_name = '8-Closed Lost' and sales_type = 'Renewal' and datediff('day', o.close_date, current_date) <=90))
), 

exclude_renewal_case as (
SELECT 
account_id
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE record_type_id in ('0128X000001pPRkQAM')
and contains(subject, 'Renewal')
AND datediff('day', created_date, current_date) <= 90
),

exclude as (
SELECT account_id from exclude_renewal_case
UNION ALL 
SELECT account_id from exclude_purchase
), 

-----------------all above is EMEA specific--------------------------



-------pulling EOA renewals based on price of $108 or $180 and creating a case 60 days out from their renewal opportunity close date 
eoa_renewals as (
SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN,
--'Urgent Renewal & TRX Support' as TYPE,
'Urgent Renewal & TRX Support' as TYPE, 
CONCAT('EoA Renewal ', close_date) as CASE_SUBJECT, 
'Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY, 
CONCAT('https://gitlab.my.salesforce.com/', opp_id) as opportunity_link
FROM (
SELECT 
        mart_arr.ARR_MONTH
         , mart_arr.SUBSCRIPTION_END_MONTH
         , mart_arr.DIM_CRM_ACCOUNT_ID
         , mart_arr.CRM_ACCOUNT_NAME
         , mart_arr.DIM_SUBSCRIPTION_ID
         , mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
         , mart_arr.SUBSCRIPTION_NAME
           , mart_arr.subscription_sales_type
         , mart_arr.AUTO_PAY
         , mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
         , mart_arr.CONTRACT_AUTO_RENEWAL
         , mart_arr.TURN_ON_AUTO_RENEWAL
         , mart_arr.TURN_ON_CLOUD_LICENSING
         , mart_arr.CONTRACT_SEAT_RECONCILIATION
         , mart_arr.TURN_ON_SEAT_RECONCILIATION
         , case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
         , mart_arr.PRODUCT_TIER_NAME
         , mart_arr.PRODUCT_DELIVERY_TYPE
         , mart_arr.PRODUCT_RATE_PLAN_NAME
         , mart_arr.ARR
         , mart_arr.quantity
         , CASE when (mart_arr.ARR / mart_arr.QUANTITY > 107 and mart_arr.ARR / mart_arr.QUANTITY < 110) then 108 else (mart_arr.ARR / mart_arr.QUANTITY) end as price
         ,o.close_date
         ,o.is_closed
         ,o.sales_type
         ,o.stage_name
         ,o.dim_crm_opportunity_id as opp_id
  FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
  JOIN EMEA_accounts a
    ON mart_arr.dim_crm_account_id = a.DIM_CRM_ACCOUNT_ID
  JOIN common.dim_subscription 
    ON mart_arr.DIM_SUBSCRIPTION_ID = DIM_SUBSCRIPTION.DIM_SUBSCRIPTION_ID
  JOIN PROD.RESTRICTED_SAFE_WORKSPACE_SALES.MART_CRM_OPPORTUNITY o 
    ON dim_subscription.DIM_CRM_OPPORTUNITY_ID_CURRENT_OPEN_RENEWAL = o.dim_crm_opportunity_id  
    and o.dim_crm_account_id= mart_arr.dim_crm_account_id
  JOIN last_create_date
  WHERE ARR_MONTH >= DATE_TRUNC('month',date('2022-02-01'))
  and mart_arr.subscription_status = 'Active'
  and PRODUCT_TIER_NAME not like '%Storage%'
  and ((price >= 107 and price <= 110) OR price::int = 180)
  and arr_month = date_trunc('month', current_date)
 and is_closed = False
 AND o.close_date >= dateadd('day', 30, last_case_creation_date)
 AND o.close_date < dateadd('day', 30, current_date))), 
 


 
failed_qsr as (SELECT
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Churn & Contraction Mitigation' as TYPE,
'Churn & Contraction Mitigation' as TYPE, 
CONCAT('Failed QSR - ', close_date) as CASE_SUBJECT, ----do we want to use the close date or another date in the subject line? 
'Adding Licenses/Purchasing Help' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Low' as PRIORITY, 
CONCAT('https://gitlab.my.salesforce.com/', dim_crm_opportunity_id) as opportunity_link
FROM(
SELECT o.*, 
CASE WHEN opportunity_name like '%QSR%' then true else false end as qsr_flag, 
datediff('day', created_date, current_date)+1 as age
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN EMEA_accounts a
ON  o.dim_crm_account_id = a.dim_crm_account_id
JOIN last_create_date
WHERE qsr_flag = true
and qsr_status = 'Failed'
and is_closed = False
and a.dim_crm_account_id not in (SELECT account_id FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE record_type_id in ('0128X000001pPRkQAM') and contains(subject, 'Failed QSR')))),





------pulling mutli-year renewals ~60 days out from their renewal date
multiyear_renewals as (SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Urget Renewal & TRX Support' as TYPE,
'Urgent Renewal & TRX Support' as TYPE, 
CONCAT('Multiyear Renewal - ', close_date) as CASE_SUBJECT,  
'Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY, 
CONCAT('https://gitlab.my.salesforce.com/', o.dim_crm_opportunity_id) as opportunity_link
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN EMEA_accounts a
ON o.dim_crm_account_id = a.dim_crm_account_id
JOIN last_create_date
WHERE opportunity_term > 12
and (opportunity_name like '%2 year%' OR opportunity_name like '%3 year%')
and is_closed = False
AND o.close_date >= dateadd('day', 30, last_case_creation_date)
AND o.close_date < dateadd('day', 30, current_date)),




------pulling PO required renewals ~60 days out of their renewal date 
po_renewals as (SELECT
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Urgent Renewal & TRX Support' as TYPE,
'Urgent Renewal & TRX Support' as TYPE, 
CONCAT('PO Required Renewal - ', close_date) as CASE_SUBJECT, 
'Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY, 
CONCAT('https://gitlab.my.salesforce.com/', o.dim_crm_opportunity_id) as opportunity_link
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN EMEA_accounts a
ON o.dim_crm_account_id = a.dim_crm_account_id
JOIN PROD.common.dim_billing_account b
ON a.dim_crm_account_id = b.dim_crm_account_id
JOIN last_create_date
WHERE is_closed = False
and PO_required = 'YES'
AND o.close_date >= dateadd('day', 30, last_case_creation_date)
AND o.close_date < dateadd('day', 30, current_date)),




-------pulling overdue renewals based on an open renewal opportunity after close date -- will change the criteria for this in the next iteration
overdue_renewals as (SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Urgent Renewal & TRX Support' as TYPE,
'Urgent Renewal & TRX Support' as TYPE, 
CONCAT('Overdue Renewal - ', close_date) as CASE_SUBJECT,  
'Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY, 
CONCAT('https://gitlab.my.salesforce.com/', o.dim_crm_opportunity_id) as opportunity_link
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN EMEA_accounts a
ON o.dim_crm_account_id = a.dim_crm_account_id
JOIN last_create_date
WHERE close_date <= current_date-1 
AND close_date >= last_case_creation_date ----test this
AND is_closed = False
and sales_type = 'Renewal'
AND NOT(opportunity_name LIKE ANY ('%storage%', '%Storage%'))
AND NOT (opportunity_name LIKE ANY ('%Refund%', '%refund%', '%debook%', '%Debook%'))),

 
 
 ------pulling auto-renewals that have a failure reason ~60 days out from renewal date
failing_autorenewals as (SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Urgent Renewal & TRX Support' as TYPE,
'Urgent Renewal & TRX Support' as TYPE,                        
CONCAT('Auto-Renewal Will Fail - ', close_date) as CASE_SUBJECT, ----do we want to use the close date or another date in the subject line? 
'Renewal' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY, 
CONCAT('https://gitlab.my.salesforce.com/', o.dim_crm_opportunity_id) as opportunity_link
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
JOIN EMEA_accounts a
ON o.dim_crm_account_id = a.dim_crm_account_id
JOIN last_create_date
where is_closed = False
AND auto_renewal_status != ''
AND auto_renewal_status not in ('on', 'off', 'On', 'Off')
and auto_renewal_status not like ('%EoA')
and auto_renewal_status not like ('%EoA%')
AND o.close_date >= dateadd('day', 30, last_case_creation_date)
AND o.close_date < dateadd('day', 30, current_date)
 ), 
 
 
 ----gets all overage cases within the last 90 days 
overage_cases as (
SELECT 
account_id
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE record_type_id in ('0128X000001pPRkQAM')
AND subject like ('Overage%')
AND datediff('day', created_date, current_date) <= 90
), 

-------pulling overs SaaS > 0 and SM > 1000 by month
-------only creating every 90 days if there is an overage
 monthly_overages as (
select
    DISTINCT
    snapshot_month,
    PING_CREATED_AT::date as ping_created_at,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    DIM_CRM_ACCOUNT_ID,
    INSTANCE_TYPE,
    DELIVERY_TYPE,
    subscription_status,
    BILLABLE_USER_COUNT,
    LICENSE_USER_COUNT,
    subscription_start_date,
    subscription_end_date, 
    BILLABLE_USER_COUNT - LICENSE_USER_COUNT AS overage_count
//  max(snapshot_month) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month, 
//  min(snapshot_month) over(partition by DIM_SUBSCRIPTION_ID_ORIGINAL) as earliest_overage_month
  from PROD.pumps.pump_gainsight_metrics_monthly_paid
    --FROM COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
  JOIN last_create_date
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'
    --and date_trunc('month', PING_CREATED_AT::date) = date_trunc('month', current_date) 
    and PING_CREATED_AT::date >= last_case_creation_date
    and overage_count > 0
), 

    overage_amount as (
SELECT o.*,
    mart_arr.ARR / mart_arr.QUANTITY as price,
  case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag,
  max(snapshot_month) over(partition by o.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_overage_month, 
  overage_count * (mart_arr.ARR / mart_arr.QUANTITY) as overage_amount
FROM monthly_overages o
  LEFT JOIN RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
ON o.DIM_SUBSCRIPTION_ID_ORIGINAL = mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
AND o.snapshot_month = mart_arr.arr_month
WHERE ((delivery_type = 'SaaS' and overage_amount > 0) OR (delivery_type = 'Self-Managed' and overage_amount > 1000))
and qsr_enabled_flag = False
qualify snapshot_month = latest_overage_month
),
    

final_overage as 
(SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
o.DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
'Expansion Opportunity' as TYPE,
 --------------------what date do we use for this since there is no close date? not necessarily an opportunity associated with this right? 
'Overage and QSR Off' as CASE_SUBJECT, 
'Adding Licenses/Purchasing Help' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Low' as PRIORITY, 
NULL as opportunity_id
FROM overage_amount o
JOIN EMEA_accounts a
ON o.dim_crm_account_id = a.dim_crm_account_id
-- JOIN common.dim_subscription sub
-- ON o.dim_crm_account_id = sub.dim_crm_account_id 
WHERE o.dim_crm_account_id not in (SELECT * from overage_cases)
-- AND sub.turn_on_seat_reconciliation = 'No'
),

 


renew_data as
(select
*
from
(
  select 
date_actual as snapshot_date,
PREP_BILLING_ACCOUNT_USER.user_name as update_user,
PREP_BILLING_ACCOUNT_USER.IS_INTEGRATION_USER,
TURN_ON_AUTO_RENEWAL as active_autorenew_status,
lag(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as prior_auto_renewal,
lead(TURN_ON_AUTO_RENEWAL,1) over(partition by subscription_name order by snapshot_date asc) as future_auto_renewal,
sub.*,
  acct.crm_account_owner,
  acct.carr_this_account,
  acct.account_tier as account_tier
from common.dim_subscription_snapshot_bottom_up sub
inner join common.dim_date on sub.snapshot_id = dim_date.date_id
inner join restricted_safe_common_prep.PREP_BILLING_ACCOUNT_USER on sub.UPDATED_BY_ID = PREP_BILLING_ACCOUNT_USER.ZUORA_USER_ID
 inner join 
 (SELECT *, 
'Pooled' as LT_segment
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT"
  WHERE account_owner = 'Pooled Sales User [ DO NOT CHATTER ]') acct 
  on sub.dim_crm_account_id = acct.dim_crm_account_id
where snapshot_date >= '2023-02-01'
  and snapshot_date < CURRENT_DATE
  and subscription_status = 'Active'
 -- and update_user = 'svc_zuora_fulfillment_int@gitlab.com'
--and (TURN_ON_AUTO_RENEWAL = 'Yes' or TURN_ON_AUTO_RENEWAL is null)
order by snapshot_date asc
)
where
((active_autorenew_status = 'No' and update_user = 'svc_zuora_fulfillment_int@gitlab.com') and prior_auto_renewal = 'Yes')
 ),
 
 
 current_sub as
(
 select
  subscription_name,
  DIM_SUBSCRIPTION_ID,
  subscription_version,
  subscription_status,
  TURN_ON_AUTO_RENEWAL,
  max(subscription_version) over(partition by subscription_name) as latest_version
  from
  common.dim_subscription
  where subscription_status = 'Active'
  and term_start_date >= '2022-02-01'
  qualify subscription_version = latest_version
  order by subscription_name
), 


autorenew_summary as
(
select
current_sub.TURN_ON_AUTO_RENEWAL as current_autorenew_status,
renew_data.*,
oppty.close_date, 
oppty.dim_crm_opportunity_id as oppty_id
from
renew_data
inner join current_sub on renew_data.DIM_SUBSCRIPTION_ID = current_sub.DIM_SUBSCRIPTION_ID
left join "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" oppty on renew_data.dim_crm_account_id = oppty.dim_crm_account_id
  and renew_data.snapshot_date < oppty.close_date
  and oppty.sales_type = 'Renewal'
  and oppty.is_closed = False
where current_autorenew_status <> 'Yes'  
  ),

  
  
  ------get this as soon as it flips from yes to no ----need to do initial retroactive run
  autorenew_off as (
  select distinct
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Churn & Contraction Mitigation' as TYPE,
'Urgent Renewal & TRX Support' as TYPE,
'Auto Renew Recently Turned Off' as CASE_SUBJECT, 
'Licensing/Fulfillment' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY, 
CONCAT('https://gitlab.my.salesforce.com/', oppty_id) as opportunity_link
FROM autorenew_summary
JOIN last_create_date
where 
snapshot_date >= last_case_creation_date
  ),



  
underutilization_cases as (
SELECT 
account_id
FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" 
WHERE record_type_id in ('0128X000001pPRkQAM')
AND subject like ('Underutilization%')
AND datediff('day', created_date, current_date) <= 90
), 

  arr_data 
as 
(
SELECT 
  mart_arr.ARR_MONTH,
  monthly_mart.snapshot_month 
          , ping_created_at 
         , mart_arr.SUBSCRIPTION_END_MONTH
         , mart_arr.DIM_CRM_ACCOUNT_ID
         , mart_arr.CRM_ACCOUNT_NAME
        , MART_CRM_ACCOUNT.CRM_ACCOUNT_OWNER
         , mart_arr.DIM_SUBSCRIPTION_ID
         , mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL
         , mart_arr.SUBSCRIPTION_NAME
           , mart_arr.subscription_sales_type
         , mart_arr.AUTO_PAY
         , mart_arr.DEFAULT_PAYMENT_METHOD_TYPE
         , mart_arr.CONTRACT_AUTO_RENEWAL
         , mart_arr.TURN_ON_AUTO_RENEWAL
         , mart_arr.TURN_ON_CLOUD_LICENSING
         , mart_arr.CONTRACT_SEAT_RECONCILIATION
         , mart_arr.TURN_ON_SEAT_RECONCILIATION
         , case when mart_arr.CONTRACT_SEAT_RECONCILIATION = 'Yes' and mart_arr.TURN_ON_SEAT_RECONCILIATION = 'Yes' then true else false end as qsr_enabled_flag
         , mart_arr.PRODUCT_TIER_NAME
         , mart_arr.PRODUCT_DELIVERY_TYPE
         , mart_arr.PRODUCT_RATE_PLAN_NAME
         , mart_arr.ARR
         , mart_arr.ARR / mart_arr.QUANTITY as price
--     ,mart_arr.QUANTITY
--          , monthly_mart.PING_CREATED_AT
--          , monthly_mart.HOSTNAME
--          , monthly_mart.DIM_NAMESPACE_ID
         , monthly_mart.BILLABLE_USER_COUNT
         , monthly_mart.LICENSE_USER_COUNT
         , monthly_mart.subscription_start_date
         , monthly_mart.subscription_end_date
  , monthly_mart.term_end_date
         , monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT AS overage_count
        , (mart_arr.ARR / mart_arr.QUANTITY)*(monthly_mart.BILLABLE_USER_COUNT - monthly_mart.LICENSE_USER_COUNT) as underutilization_amount
        , max(snapshot_month) over(partition by monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL) as latest_underutilization_month
    FROM RESTRICTED_SAFE_COMMON_MART_SALES.MART_ARR mart_arr
  left join RESTRICTED_SAFE_COMMON_MART_SALES.MART_CRM_ACCOUNT on mart_arr.DIM_CRM_ACCOUNT_ID = MART_CRM_ACCOUNT.DIM_CRM_ACCOUNT_ID
             LEFT JOIN 
  (
    select
    DISTINCT
    snapshot_month,
    ping_created_at,
    DIM_SUBSCRIPTION_ID_ORIGINAL,
    INSTANCE_TYPE,
    subscription_status,
    BILLABLE_USER_COUNT,
    LICENSE_USER_COUNT,
    subscription_start_date,
    subscription_end_date,
    term_end_date
    from
  COMMON_MART_PRODUCT.MART_PRODUCT_USAGE_PAID_USER_METRICS_MONTHLY 
    JOIN last_create_date
  where INSTANCE_TYPE = 'Production'
    and subscription_status = 'Active'
//    and PING_CREATED_AT::date >= '2022-02-01'
//    and PING_CREATED_AT::date >= last_case_creation_date ------------NEED TO UPDATE THIS TO GRAB EOMONTH DATE FOR UNDERUTILIZATION PING AND SET THE TIMEFRAME TO GET ONLY UNDERUTILIZATION WHEN ITS THE LAST DAY PING
  and date_trunc('month', current_date) >= last_case_creation_date
  and date_trunc('month', current_date) <= current_date
  
  ) monthly_mart
  ON mart_arr.DIM_SUBSCRIPTION_ID_ORIGINAL = monthly_mart.DIM_SUBSCRIPTION_ID_ORIGINAL
    WHERE ARR_MONTH >= DATE_TRUNC('month',date('2022-01-01'))
   --   and lower(user_role_name) like 'pooled-smb-system%'
 --     and mart_arr.subscription_status = 'Active'
  and ARR_MONTH = snapshot_month
  and ARR_MONTH = DATE_TRUNC('month',CURRENT_DATE)
  and PRODUCT_TIER_NAME not like '%Storage%'
  and  crm_account_owner like any ('%Erica Wilson%', '%Amanda Shim%' ,'%Matthew Wyman%', '%Pooled%')
  and monthly_mart.billable_user_count >= 1
  and overage_count < 0
--  and qsr_enabled_flag
  ), 
  
 underutilization_top_x as 
(SELECT arr_data.*, 
 o.close_date, 
 o.sales_type, 
o.dim_crm_opportunity_id as oppty_id, 
 datediff('day', current_date, o.close_date)
 from arr_data 
JOIN EMEA_accounts a
ON arr_data.dim_crm_account_id = a.dim_crm_account_id
LEFT JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_OPPORTUNITY" o 
ON arr_data.dim_crm_account_id = o.dim_crm_account_id
 AND sales_type = 'Renewal' 
 AND is_closed = False
 and o.close_date >= current_date and o.close_date < DATEADD('day', 270, current_date)
WHERE datediff('day', current_date, o.close_date) >= 120
AND arr_data.dim_crm_account_id not in (SELECT * FROM underutilization_cases) 
ORDER BY underutilization_amount 
LIMIT 30
 ), 
  
  
  
underutilization as (
select
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Churn & Contraction Mitigation' as TYPE,
'Churn & Contraction Mitigation' as TYPE,
CONCAT('Underutilization') as CASE_SUBJECT, 
'Licensing/Fulfillment' as CASE_REASON, 
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Medium' as PRIORITY,
CONCAT('https://gitlab.my.salesforce.com/', oppty_id) as opportunity_link
FROM underutilization_top_x), 



-----pulling high PTC based on probability of churn (>50%) and getting the top 30 cases by carr (after current exlcusions)
high_ptc_score as (SELECT 
*
FROM EMEA_accounts a
JOIN "PROD"."WORKSPACE_DATA_SCIENCE"."PTC_SCORES" ptc
ON ptc.crm_account_id = a.dim_crm_account_id
JOIN last_create_date
where carr_this_account >0 
and score > .5
and score_date > last_case_creation_date
and score_date <= current_date
AND dim_crm_account_id not in (SELECT account_id FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" where record_type_id in ('0128X000001pPRkQAM') AND subject in ('PRO: New 1/2* PTC Account', 'High PTC Score') and datediff('day', created_date, current_date) <= 90)
AND dim_crm_account_id not in (SELECT account_id from exclude)
ORDER BY carr_this_account desc
LIMIT 30), 


----getting only those from the top 30 carr accounts
high_ptc_final as (
SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Churn & Contraction Mitigation' as TYPE,
'Churn & Contraction Mitigation' as TYPE, 
'High PTC Score' as CASE_SUBJECT, 
'Sales Evaluation (Non-Renewal)' as CASE_REASON,
'0128X000001pPRkQAM' as RECORD_TYPE_ID, 
'Low' as PRIORITY, 
NULL as opportunity_id
FROM high_ptc_score),

 high_pte_score as (SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Expansion' as TYPE,
'Expansion Opportunity' as TYPE, 
'High PTE Score' as CASE_SUBJECT, 
'New High Engagement Account - 5* PTE' as CASE_REASON,
'0128X000001pPRkQAM' as RECORD_TYPE_ID,
'Low' as PRIORITY,
NULL as opportunity_id
FROM EMEA_accounts a
JOIN "PROD"."WORKSPACE_DATA_SCIENCE"."PTE_SCORES"  pte 
    ON a.dim_crm_account_id = pte.crm_account_id 
JOIN last_create_date 
WHERE carr_this_account > 0 
AND pte.score_group in (4,5) 
AND dim_crm_account_id not in (SELECT account_id FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" where record_type_id in ('0128X000001pPRkQAM') AND subject in ('PRO: New 4/5* PTE Account', 'PRO: New 5* PTE Account', 'High PTE Score', 'High PTC Score') and datediff('day', created_date, current_date) <= 90)
AND dim_crm_account_id not in (SELECT account_id from high_ptc_final)
AND score_date > last_case_creation_date
and score_date <= current_date
  ),

  six_sense as (
SELECT 
'00G8X000006WmU3' as OWNER_ID, 
'Open' as STATUS,
DIM_CRM_ACCOUNT_ID as ACCOUNT_ID,
'SMB Pool Internal Created' as CASE_ORIGIN, 
--'Expansion' as TYPE,
'Expansion Opportunity' as TYPE, 
'6sense Growth Signal' as CASE_SUBJECT, 
'Sales Evaluation (Non-Renewal)' as CASE_REASON,
'0128X000001pPRkQAM' as RECORD_TYPE_ID,
'Low' as PRIORITY, 
NULL as opportunity_id
FROM EMEA_accounts 
WHERE six_sense_account_buying_stage in ('Decision','Purchase')
and SIX_SENSE_ACCOUNT_PROFILE_FIT = 'Strong'
and SIX_SENSE_ACCOUNT_INTENT_SCORE > 74
and DATEDIFF('day', SIX_SENSE_ACCOUNT_UPDATE_DATE, current_date) <= 7
and dim_crm_account_id not in (SELECT account_id FROM "PROD"."WORKSPACE_SALES"."SFDC_CASE" where record_type_id in ('0128X000001pPRkQAM') AND subject in  ('6sense Growth Signal') and datediff('day', created_date, current_date) <= 90 )),
  



all_renewals as (
SELECT * FROM eoa_renewals
UNION ALL  
SELECT * FROM multiyear_renewals
UNION ALL 
SELECT * FROM PO_renewals
UNION ALL 
SELECT * FROM overdue_renewals
UNION ALL
SELECT * FROM failing_autorenewals
), 



all_nonrenewals as (   
SELECT * FROM high_pte_score
UNION ALL 
SELECT * FROM high_ptc_final
UNION ALL 
SELECT * FROM failed_qsr
UNION ALL 
SELECT * FROM final_overage
UNION ALL 
SELECT * FROM autorenew_off
UNION ALL 
SELECT * FROM underutilization
UNION ALL 
SELECT * FROM six_sense
), 


all_cases as (
SELECT * FROM all_nonrenewals
WHERE account_id not in (SELECT account_id from all_renewals)
UNION 
SELECT * from all_renewals) 

SELECT * FROM all_cases
WHERE account_id not in (SELECT * FROM exclude)