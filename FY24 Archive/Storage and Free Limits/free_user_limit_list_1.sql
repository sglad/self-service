/*
Based on Charan's snippet: https://gitlab.com/gitlab-com/business-analytics/-/snippets/2546593

Updated dates for 12m active flag, current Storage snapshot
Updated to exclude Premium Trials
*/

with 																									
 -- step 1: pull all the notified namespaces 																									
all_banner_view_methods AS ( --ultimate namespaces that have triggered notification																									
																									
    SELECT 																									
      DISTINCT 																									
      dim_namespace.ultimate_parent_namespace_id as namespace_id,																									
      MIN(COALESCE(dashboard_notification_at, dashboard_enforcement_at))::DATE AS min_event_date -- ADDED LINE																									
     -- MIN(dashboard_notification_at)::DATE AS min_event_date  -- REMOVED LINE																									
    FROM workspace_product.wk_gitlab_dotcom_namespace_details_snapshots nd																									
    JOIN common.dim_namespace																									
      ON dim_namespace.dim_namespace_id = nd.namespace_id																									
    WHERE dashboard_notification_at IS NOT NULL or dashboard_enforcement_at is not null 																									
    GROUP BY 1																									
																									
UNION ALL																									
																									
  SELECT 																									
    DISTINCT 																									
    e.ultimate_parent_namespace_id as namespace_id,																									
    MIN(behavior_at)::DATE AS min_event_date																									
  FROM common_mart.mart_behavior_structured_event e 																									
  WHERE e.event_action = 'render'																									
    AND e.event_label = 'user_limit_banner'																									
    AND e.behavior_at::DATE >= '2022-12-28' -- Date that banner was originally released																									
  GROUP BY 1																									
 																									
), 																									
																									
-- notified namespace unique 																									
all_banner_view_methods2 as (select namespace_id , min(min_event_date) as first_event_Date																									
from all_banner_view_methods																									
group by 1 ) , 																									
																									
-- step 2: List of all namespaces 																									
-- Step 2: list of namespaces to focus 																									
ns as (select 																									
       dn.created_at::date as created_at, dn.creator_id, dn.dim_namespace_id as namespace_id,dn.is_setup_for_company,GITLAB_PLAN_TITLE, namespace_type, visibility_level																									
from prod.common.dim_namespace dn 																									
where dn.NAMESPACE_IS_ULTIMATE_PARENT = TRUE																									
and dn.namespace_type in ( 'Group') 																									
       and visibility_level in ('private') 																									
       and GITLAB_PLAN_TITLE like any('Free','%Trial%') 	
       and GITLAB_PLAN_TITLE <> 'Premium Trial'																								
and dn.namespace_creator_is_blocked = FALSE -- same as blocked.user_id IS NULL 																									
and dn.namespace_is_internal = FALSE																									
and dn.created_at < '2022-12-28'																									
), 	

																									
-- Step 3: list of active namespaces 																									
ns_activity_yr as (select ns.namespace_id, max(1) as active_12m, max(e.event_Date) as last_event_Date																									
																									
from PROD.common_mart.mart_event_namespace_daily e																									
inner join  ns on ns.namespace_id = e.DIM_ULTIMATE_PARENT_NAMESPACE_Id																									
where e.event_Date between '2022-06-11' and '2023-06-11'																									
group by 1), 																									
																									
-- Step 4: user count at specific time																									
ns_users as (select ns.namespace_id,memberships.user_id,																									
           MIN(members.invite_created_at) AS first_invite_created_at, --first invite to parent namespace (or sub-group, project, etc)																									
            MIN(members.invite_accepted_at) AS first_invite_accepted_at,																									
            IFNULL(first_invite_accepted_at, first_invite_created_at) AS membership_start_at																									
          FROM legacy.gitlab_dotcom_memberships memberships --starting with memberships since we only care about succcessful invites and can group by ultimate_parent_id																									
          inner  join legacy.gitlab_dotcom_members members --join to get timestamp -- xxxxx inner join reduces the row count from 15M to 9M, but we decided to do this for now, as the impact to group namespace is <2% 																									
            ON memberships.user_id = members.user_id																									
        and memberships.membership_source_id = members.source_id 																									
        inner join ns--limit to just the top-level namespaces we care about																									
            ON memberships.ultimate_parent_id = ns.namespace_id 																									
             where memberships.is_billable = TRUE																									
            group by 1,2)																									
																									
            																									
,ns_users_jan as ( 																									
select ns.namespace_id																									
  ,  count(distinct case when nu.membership_start_at <= '2023-01-01'  then user_id else null end ) as num_users -- members who join and leave will not be captured. So beware. 																									
from ns_users nu 																									
inner join ns on ns.namespace_id = nu.namespace_id																									
group by 1), 																									
																									
-- step 5: No of current users 																									
																									
ns_users_current as (select ns.namespace_id,count(distinct memberships.user_id)  as num_users																									
																									
          FROM legacy.gitlab_dotcom_memberships memberships --starting with memberships since we only care about succcessful invites and can group by ultimate_parent_id																									
       																									
        inner join ns--limit to just the top-level namespaces we care about																									
            ON memberships.ultimate_parent_id = ns.namespace_id 																									
             where memberships.is_billable = TRUE																									
            group by 1), 																									
            																									
-- step 6: storage																									
 storage2 as 																									
(select  ns.namespace_id																									
  ,storage_gib as total_size_GB 																									
  from ns 																									
inner join PROD.common.fct_usage_storage s 																									
  on ns.namespace_id = s.ULTIMATE_PARENT_NAMESPACE_ID and s.snapshot_month = '2023-06-01'																									
 ), 																									
 																									
 																									
 --- step 7: active user list in last 3 months 																									
 user_active_cnt3m as 																									
 (select dim_ultimate_parent_namespace_id as namespace_id , count(distinct dim_user_id) as num_users 																									
  from PROD.common.fct_event_user_daily 																									
  where event_Date between dateadd(month,-3,getdate()::date) and getdate()::date 																									
  group by 1), 																									
  																									
  																									
 -- List of eligible namespace 																									
final as (select ns.namespace_id, namespace_type, visibility_level	
          , ay.active_12m as is_active_within_12m
          ,coalesce(s2.total_size_GB ,0) as storage_size_GB																									
          , coalesce(j.num_users ,0) as jan_users																									
          , coalesce(c.num_users ,0) as current_users																									
          , coalesce(m3.num_users ,0) as active_user_count_3m 																									
--          , coalesce(ay.active_12m, 0 ) as acive_12m																									
          , iff(bv.namespace_id is not null,1,0) as notified_namespace																									
 from ns -- about 1% of namespaces that converted from private to public will be missing here. 																									
 left join storage2 s2 on s2.namespace_id = ns.namespace_id 																									
 left join all_banner_view_methods2 bv on bv.namespace_id = ns.namespace_id 	
 left join ns_activity_yr ay on ay.namespace_id = ns.namespace_id 
 left join ns_users_jan  j on j.namespace_id = ns.namespace_id  																									
 left join ns_users_current c on c.namespace_id = ns.namespace_id 																									
 left join  user_active_cnt3m m3 on m3.namespace_id = ns.namespace_id
 where namespace_type in ( 'Group') and visibility_level = 'private' --redundant in this code, as ns already filters for this. 
																									
), 																									
																									
final2 as (select  namespace_id, namespace_type,visibility_level, is_active_within_12m	,																								
case when storage_size_GB <= 5 and current_users > 5 																									
           and namespace_type in ( 'Group') and visibility_level = 'private' then '1.Impacted by User limit only'																									
           																									
when storage_size_GB <= 5 and (notified_namespace = 1 or jan_users > 5)  																									
           and namespace_type in ( 'Group') and visibility_level = 'private' then '2.Received user limit notification in past'																									
           																									
when storage_size_GB > 5 and current_users > 5 																									
           and namespace_type in ( 'Group') and visibility_level = 'private' then '3.Impacted by user limit and storage limit'																									
           																									
when storage_size_GB > 5 and (notified_namespace = 1 or jan_users > 5) 																									
           and namespace_type in ( 'Group') and visibility_level = 'private' then '4.Impacted by user limit and storage limit'																										
           																									
																							
           																									
else 'others' end as namespace_type_detailed																									
, case when storage_size_GB < 5 then '1. Less than 5GB'																									
       when storage_size_GB < 10 then '2. >5GB but <10GB'																									
       when storage_size_GB < 20 then '3. >10GB but <20GB'   																									
       when storage_size_GB < 50 then '4. >20GB but <50GB'  																									
       when storage_size_GB < 250 then '5. >50GB but <250GB' 																									
       else '6. > 250GB' end as storage_size_bucket																									
, case when current_users < 6 then '1. < 6 users'																									
            when current_users < 10 then '2. 6 to 10 users'																									
           when current_users < 25 then '3. 11 to 25 users'																									
           when current_users < 50 then '4. 26 to 50 users'																									
           when current_users < 100 then '5. 51 to 100 users'																									
           when current_users < 200 then '6. 101 to 200 users'																									
           else '7. >= 200 users' end as user_bucket																																																		
from final 																									
																								
 )  																									
																									
select is_active_within_12m, namespace_type_detailed, namespace_id from final2 	where namespace_type_detailed	!= 	'others'																					
	
