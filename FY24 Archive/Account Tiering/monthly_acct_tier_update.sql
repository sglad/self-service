--------query to get acct snapshot table------
drop table if exists acct_base; 
create temporary table acct_base as (SELECT acct.*, 
dim_date.fiscal_year,
dim_date.fiscal_quarter_name_fy,
dim_date.last_day_of_month,
fo.fiscal_year as fo_fiscal_year,
fo.close_date as fo_close_date,
fo.net_arr as fo_net_arr,
churn.fiscal_year as churn_fiscal_year, 
churn.close_date as churn_close_date, 
churn.net_arr as churn_net_arr,
CASE
    when dim_date.fiscal_year = 2024 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled'
    when dim_date.fiscal_year = 2023 and crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' end as LT_Segment,
case when curr_acct.dim_crm_account_id is not null then true else false end as current_pool_flag,
case when curr_acct.curr_carr > 0 then true else false end as current_customer_flag,          
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT
FROM "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT_DAILY_SNAPSHOT" acct
LEFT JOIN common.dim_date 
  ON acct.snapshot_date = dim_date.date_actual
left join
  (            
   select    
    distinct
   dim_crm_account_id,
    CARR_THIS_ACCOUNT as curr_carr
    from
              "PROD"."RESTRICTED_SAFE_COMMON"."DIM_CRM_ACCOUNT"
   where            crm_account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%')
   )        curr_acct
  on acct.dim_crm_account_id = curr_acct.dim_crm_account_id
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_won
    and order_type = '1. New - First Order'
    ) fo on fo.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
left join 
  (
 select
    distinct
    DIM_parent_CRM_ACCOUNT_ID,
    last_value(net_arr) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as net_arr,
    last_value(close_date) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as close_date,
    last_value(fiscal_year) over(partition by DIM_parent_CRM_ACCOUNT_ID order by close_date asc) as fiscal_year
    from
    restricted_safe_common_mart_sales.mart_crm_opportunity
    LEFT JOIN common.dim_date
  ON mart_crm_opportunity.close_date = dim_date.date_actual
    where
    is_closed
    and order_type like any ('%5%','%6%')
      
    ) churn on churn.DIM_parent_CRM_ACCOUNT_ID = acct.DIM_parent_CRM_ACCOUNT_ID
WHERE dim_date.fiscal_year >= 2023
              );
              
--------getting calculated account tiers for each day, exlcluding tiers 1/1.5 unless they have churned and any 2/3 that has been manually updated 
drop table if exists calc; 
create temporary table calc as (SELECT b.*, 
case when b.account_tier in ('Rank 1','Rank 1.5') and (b.churn_close_date is null or b.churn_close_date > b.SNAPSHOT_date)  then 1 ----case when to say that if an account is rank 1 or 2 and NOT churned then keep it as tier 1, and if a tier 2/3 account was manually updated then keep it as 2/3 otherwise give it the calculated tier
    when b.account_tier_notes not like '%Ops%' and b.snapshot_date >= '2023-03-01' then 
        case when b.account_tier = 'Rank 2' then 2
            when b.account_tier = 'Rank 3' then 3
        else 1 end
else calc.account_tier_calculated end as account_tier_calculated
FROM acct_base b
LEFT JOIN (SELECT
      dim_crm_account_id,
           snapshot_date as calc_snapshot_date,
        2964 as carr_tier_1,
        2400 as carr_tier_2,
        1916 as carr_tier_3,
        134985000 as funding_tier_1,
        44909650 as funding_tier_2,
        20624700 as funding_tier_3,
        103 as dev_count_tier_1,
        44 as dev_count_tier_2,
        29 as dev_count_tier_3,
        CASE
        WHEN (CARR_THIS_ACCOUNT >= CARR_TIER_1 OR LAM_DEV_COUNT >= DEV_COUNT_TIER_1 OR TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_1) then 1 else 0 end as top_tier_1,
        CASE
        WHEN
        (((CARR_THIS_ACCOUNT >= CARR_TIER_2 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_2)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2)
        OR (LAM_DEV_COUNT >= DEV_COUNT_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_2 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))) then 1 else 0 end as top_tier_2,
        CASE
        WHEN
        (((CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3)
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND LAM_DEV_COUNT >= DEV_COUNT_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR (CARR_THIS_ACCOUNT >= CARR_TIER_3 AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services'))
        OR  (LAM_DEV_COUNT >= DEV_COUNT_TIER_3  AND TOTAL_FUNDING_AMOUNT >= FUNDING_TIER_3 AND FINAL_INDUSTRY IN ('FinTech', 'Healthcare', 'Internet Software & Services')))
        AND (top_tier_2 = 0)) then 1 else 0 end as top_tier_3 ,
        case when top_tier_1 > 0 or top_tier_2 >0 or top_tier_3 > 0 then 1
        else
        case when carr_this_account < 1500 and lam_dev_count < 25 then 3
        else 2 end
        end as account_tier_calculated 
     from acct_base
      where --lt_segment = 'Pooled'
     -- and
           fiscal_year = 2024) calc
      on calc.dim_crm_account_id = b.dim_crm_account_id
      and calc.calc_snapshot_date = b.snapshot_date); 
 

--Logic is in the last_month_pooled_customers table already just waiting on date to be passed
drop table if exists last_tier_update;
create temporary table last_tier_update as (
select
concat('20', max(trim(right(account_tier_notes,8)))) as most_recent_retier
FROM calc
where
crm_account_owner in ('Amanda Shim', 'Erica Wilson', 'Matthew Wyman Jr',  'Pooled Sales User [ DO NOT CHATTER ]')
and snapshot_date = CURRENT_DATE - 2
and account_tier_notes like '%Self Service%'
limit 1
);


-------getting current pooled customers (snapshot as of yesterday) by account owner -- getting calculated account tier from the calc query above as this will be the "new account tier" 
drop table if exists current_pooled_customers;
create temporary table current_pooled_customers as (
select
dim_crm_account_id,
next_renewal_date,
snapshot_date,
crm_account_owner,
carr_this_account,
account_tier, 
account_tier_calculated, 
churn_close_date, 
fo_close_date,
most_recent_retier
FROM calc
JOIN last_tier_update
WHERE
crm_account_owner in ('Amanda Shim', 'Erica Wilson', 'Matthew Wyman Jr', 'Pooled Sales User [ DO NOT CHATTER ]')
--  and carr_this_account > 0
and snapshot_date = current_date - 1
);


---getting last month pooled customers by account owner and pulling account tier. Snapshot date is the day after the last retier date
drop table if exists last_month_pooled_customers;
create temporary table last_month_pooled_customers as (
select
most_recent_retier,
dim_crm_account_id,
snapshot_date,
churn_close_date, 
fo_close_date,
crm_account_owner,
next_renewal_date,
carr_this_account,
PARENT_CRM_ACCOUNT_LAM_DEV_COUNT as LAM_dev_count,
crm_account_industry as final_industry,
TRY_CAST(crm_account_zoom_info_total_funding as float) as TOTAL_FUNDING_AMOUNT, 
substr(account_tier,6,3) as account_tier, 
crm_account_owner_user_segment,   
CASE WHEN crm_account_owner in ('Amanda Shim', 'Erica Wilson', 'Matthew Wyman Jr', 'Pooled Sales User [ DO NOT CHATTER ]') then 1 else 0 end as last_month_pooled_account
FROM calc a
JOIN last_tier_update
WHERE
//(crm_account_owner = 'Pooled Sales User [ DO NOT CHATTER ]' or
//(proposed_crm_account_owner = 'Pooled Sales User [ DO NOT CHATTER ]' and u.user_role_type = 'FO' and CRM_USER_REGION = 'AMER' and crm_account_owner_user_segment = 'SMB'))
--  and carr_this_account > 0
 snapshot_date = most_recent_retier
);


-------using this table to null out account tiers from any accounts that are not previously pooled accounts 
drop table if exists last_month_final; 
create temporary table last_month_final as (
SELECT 
  lm.*, 
  CASE WHEN last_month_pooled_account = 1 then account_tier else NULL end as lm_account_tier
  FROM last_month_pooled_customers lm
);



--------this table compares the current pool to the previous pool and flag new to pool, new via FO, renewals and churns to update tiers in the next query 
drop table if exists compare;
create temporary table compare as (
select
curr.*,
past.crm_account_owner as previous_owner,
try_cast(past.lm_account_tier as float) as prior_tier,
last_month_pooled_account,
case when (curr.FO_close_date >= curr.most_recent_retier and curr.FO_close_date < current_date()) and prior_tier IS NULL then 1 else 0 end as new_via_FO,
--case when curr.crm_account_owner = 'Pooled Sales User [ DO NOT CHATTER ]' and (past.crm_user_role_type = 'FO' or past.crm_account_owner = 'Sales Admin [DO NOT CHATTER THIS USER]') then 1 else 0 end as new_previous_FO,
case when curr.carr_this_account > 0 and past.carr_this_account > 0 and last_month_pooled_account = 0 then 1 else 0 end as new_to_pool,
case when (curr.churn_close_date >= past.most_recent_retier and curr.churn_close_date < current_date()) and prior_tier IS NOT NULL then 1 else 0 end as churn_flag,
  case when curr.carr_this_account > 0 and curr.next_renewal_date <> past.next_renewal_date and churn_flag = 0 then 1 else 0 end as renewal_flag
from
current_pooled_customers curr
left join last_month_final past
on curr.dim_crm_account_id = past.dim_crm_account_id
);



select
dim_crm_account_id,
case
when new_via_fo = 1 then account_tier_calculated
when new_to_pool = 1 then account_tier_calculated
when renewal_flag = 1 and account_tier_calculated <> prior_tier and (prior_tier in (2,3) OR (prior_tier = 1 AND carr_this_account < 0)) then account_tier_calculated
--when renewal_flag = 1 and account_tier_calculated < prior_tier then account_tier_calculated
when renewal_flag = 0 and account_tier_calculated < prior_tier then account_tier_calculated
when churn_flag = 1 then account_tier_calculated
--when account_left_pool = 1 then null 
end as new_account_tier,
case
when new_via_fo = 1 then 'New Account via FO'
when new_to_pool = 1 then 'New Account to Pool'
when renewal_flag = 1 and account_tier_calculated > prior_tier then 'Renewal Downtier'
when renewal_flag = 1 and account_tier_calculated < prior_tier then 'Renewal Uptier'
when renewal_flag = 0 and account_tier_calculated < prior_tier then 'Nonrenewal Uptier'
when churn_flag = 1 then 'Churned - Updated Tier'
--when account_left_pool = 1 then 'No Longer in Pool'
end as new_account_tier_notes,
prior_tier
from compare
where (new_via_FO = 1 or new_to_pool = 1 or (renewal_flag = 1 and new_account_tier <> prior_tier) or (renewal_flag = 0 and new_account_tier < prior_tier and carr_this_account > 0 ) or churn_flag = 1) 
ORDER by new_account_tier_notes, prior_tier
