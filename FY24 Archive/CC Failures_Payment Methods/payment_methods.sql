----pulling all current pooled accounts with carr breakdown
DROP TABLE IF EXISTS pooled;
CREATE TEMPORARY TABLE pooled as (
SELECT *, 
  CASE WHEN carr_this_account < 2500 then '<2.5k'
     WHEN carr_this_account >= 2500 and carr_this_account < 5000 then '2.5-5k'
     WHEN carr_this_account >= 5000 and carr_this_account < 10000 then '5-10k'
     WHEN carr_this_account >= 10000 and carr_this_account < 15000 then '10-15k'
     WHEN carr_this_account >= 15000 then '15k+' end as arr_bucket,
CASE WHEN account_owner like any ('%Amanda Shim%', '%Erica Wilson%', '%Matthew Wyman%',  '%Pooled Sales User%','%Meg Murray%','%Daniel Listrom%','%Chelsey Maki%') then 'Pooled' else 'Other' end as LT_segment 
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT"
WHERE LT_segment = 'Pooled'
and carr_this_account > 0 );


----pulling al current smb accounts with carr breakdown 
DROP TABLE IF EXISTS smb; 
CREATE TEMPORARY TABLE smb as (
SELECT *, 
CASE WHEN carr_this_account < 2500 then '<2.5k'
     WHEN carr_this_account >= 2500 and carr_this_account < 5000 then '2.5-5k'
     WHEN carr_this_account >= 5000 and carr_this_account < 10000 then '5-10k'
     WHEN carr_this_account >= 10000 and carr_this_account < 15000 then '10-15k'
     WHEN carr_this_account >= 15000 then '15k+' end as arr_bucket
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_CRM_ACCOUNT" 
where carr_this_account > 0 
and CRM_ACCOUNT_OWNER_USER_SEGMENT = 'SMB'
);


-----mart_payment temp table will get just the mart_arr records from the latest month (not used in this query but could be used in the future for repeat analysis)
DROP TABLE IF EXISTS mart_payment;
CREATE TEMPORARY TABLE mart_payment as (
SELECT distinct
dim_crm_account_id, 
auto_pay, 
PO_REQUIRED, 
default_payment_method_type
FROM "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_ARR"
where arr_month = '2023-05-01');


------takes all the accounts from pooled or smb (can interchange temp tables) and joins to the billing account which provides information on batch # and payment method type 
-------joins to mart_arr table to get only billing accounts from the active month so that older billing accounts are not included 
------produced some duplicated for crm account because some accounts have multiple active billing accounts -- I am including this in the analysis because each billing account should be accounted for especialy if there are different ways of paying within one accounts (this is seen more in SMB then low touch)
SELECT
CASE WHEN payment_method = 'Auto CC' then 'Auto CC (Web Direct)'
     WHEN payment_method = 'CreditCard' then 'Invoice + CC'
     WHEN payment_method in ('ACH', 'Check', 'WireTransfer') then 'Invoice + WireTransfer/ACH/Check'
     Else 'Other' end as payment_type, 
COUNT(*) as total
FROM(
SELECT
p.*,
b.billing_account_name,
b.auto_pay, 
b.batch,
b.default_payment_method_type, 
CASE WHEN BATCH = 'Batch1' then 'Auto CC'
     WHEN BATCH = 'Batch2' then b.default_payment_method_type 
     end as payment_method
FROM pooled p
lEFT JOIN PROD.common.dim_billing_account b
ON p.DIM_CRM_ACCOUNT_ID = b.DIM_CRM_ACCOUNT_ID
   JOIN "PROD"."RESTRICTED_SAFE_COMMON_MART_SALES"."MART_ARR" a
ON a.billing_account_number = b.billing_account_number
AND a.arr_month = '2023-05-01'
where batch in ('Batch1','Batch2'))
GROUP BY 1
ORDER BY 1